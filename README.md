# Spline EOS

Using splines as equations of state (EOS).

# Example

This is currently in progress.

# Installation and Requirements

This was developed with Python 3.7.
The packages used are documented in the `requirements.txt`.

## How to Install

To install the code in a development mode, activate 
your Python 3.8 environment and then:

    $ cd /path/to/seos
    $ pip install -e ./

Or if using Anaconda,

    $ conda activate <environment>
    $ cd /path/to/seos
    $ pip install -e ./ --user

# Developers

Currently, I am the only developer, and I will not be accepting merge requests.
This is code used in my master's thesis.

# Copyright

Copyright (C) 2021 Reid Ginoza
All rights reserved.
