# Copyright (C) 2021 Reid Ginoza
# All rights reserved.
from pathlib import Path
from textwrap import dedent
from typing import Callable, Sequence

from matplotlib import pyplot as plt
import numpy as np
import pandas as pd
import pytest

from seos.other_models.mg import MGLinear


def get_jackson():
    data = pd.read_csv(Path(__file__).parent / "files" / "v_jackson_lx17.csv")
    truth_p = data["P(GPA)"].to_numpy()
    v_rel = data["V(--)"].to_numpy()
    c0, s1 = 2.33, 2.32
    rho_0 = 1.9
    x = np.array((c0, s1))
    jackson = MGLinear(x, rho_0=rho_0)
    return v_rel, truth_p, jackson


@pytest.mark.parametrize(
    "v_rel,truth_p,hugoniot", [get_jackson(),],
)
def test_mg(v_rel, truth_p, hugoniot: Callable):
    # Act
    results_pressure = hugoniot(v_rel)

    # Assert
    assert results_pressure == pytest.approx(truth_p, abs=0.5)


def plot_jackson_diff():
    # Arrange
    data = pd.read_csv(Path(__file__).parent / "files" / "v_jackson_lx17.csv")
    truth_p = data["P(GPA)"].to_numpy()
    v_rel = data["V(--)"].to_numpy()

    c0, s1 = 2.33, 2.32
    x = np.array((c0, s1))
    jackson = MGLinear(x, rho_0=1.9)

    # Act
    results_pressure = jackson(v_rel)

    # Plot
    fig: plt.Figure
    axs: Sequence[plt.Axes]
    fig, axs = plt.subplots(nrows=2, sharex=True)

    axs[0].plot(v_rel, truth_p, label="Truth", alpha=0.8, lw=3)
    axs[0].plot(v_rel, results_pressure, label="MG module", alpha=0.8)
    axs[0].set_title("Hugoniot")
    axs[0].legend()

    residual = truth_p - results_pressure
    axs[1].scatter(v_rel, truth_p - results_pressure)
    axs[1].set_title("Difference")
    print(
        dedent(
            f"""\
    Max positive diff: {residual.max()}
    Max negative diff: {residual.min()}
    """
        )
    )
    return fig, axs


def test_dallman_mg():
    # Arrange
    data = pd.read_csv(
        Path(__file__).parent / "files" / "v_rel_dallman_lx17.csv"
    )
    truth_p = data["P(GPA)"].to_numpy()

    c0, s1 = 1.05, 3.65
    rho_0 = 1.913
    x = np.array((c0, s1))
    dallman = MGLinear(x, rho_0=rho_0)

    # Act
    results_pressure = dallman(data["V(--)"].to_numpy())

    # Assert
    assert results_pressure == pytest.approx(truth_p, abs=0.5)
