# Copyright (C) 2021 Reid Ginoza
# All rights reserved.
import pytest

from seos.utilities import conversions


def test_import():
    assert True


def test_pressure_GPa_to_atm():
    # 1 GPa in atm
    truth = 9869.23  # atm
    assert truth == conversions.GPa_to_atm


def test_pressure_atm_to_GPa():
    # 1 atm in GPa
    truth = pytest.approx(0.000101325)  # GPa
    assert truth == 1 / conversions.GPa_to_atm


def test_energy_J_to_KJ():
    # 1 kJ in J
    assert 1e-3 == conversions.J_to_kJ


def test_energy_kJ_to_cal():
    # write this test
    # 1 kJ in cal
    truth = pytest.approx(239.005736)
    assert truth == conversions.kJ_to_cal
