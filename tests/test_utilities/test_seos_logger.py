# Copyright (C) 2021 Reid Ginoza
# All rights reserved.
from logging import getLoggerClass
from seos.utilities.seos_logger import make_logger


def test_import():
    assert True


def test_make_logger():
    # Act
    logger = make_logger(__name__)

    # Assert
    assert logger.__class__ == getLoggerClass()
