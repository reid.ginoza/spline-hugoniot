# Copyright (C) 2021 Reid Ginoza
# All rights reserved.
from pathlib import Path

import numpy as np
import pandas as pd
import pytest
from scipy.interpolate import InterpolatedUnivariateSpline

from seos.splines import spline_eos
from seos.splines import default_spline

TARVER_RHO_0 = 1.905


def test_constraint_sound_speed_non_imaginary():
    # Arrange
    new_spline = default_spline.few_nodes_spline()

    # Act
    constraint_sound_speed = new_spline.constraint_sound_speed_non_imaginary()

    # Assert
    assert constraint_sound_speed <= 0


def load_hugo_with_cs():
    """Used for verifying Cs calculations."""
    csv_file = Path(__file__).parent / "files" / "down_sampled_hugo.csv"
    df = pd.read_csv(csv_file)
    df["CS_SQ"] = df["CS(KM/S)"] ** 2
    return df


def make_spline_from_truth_df(df):
    return spline_eos.SplineEOS(
        x=df["V(--)"],
        y=df["P(GPA)"],
        rho_0=TARVER_RHO_0,
        p0=3.3027e-04,
        e0=6.7890e-02,
        t0=298.15,
        cv=1.3052623990693266,
        gamma=8.94000000e-01,  # assume gamma_kind = "constant"
    )


# verified using the table from BCAT.
@pytest.mark.parametrize(
    "v_rel,truth_cs_sq",
    [
        (0.6026277, 197.711721),
        (0.6081522, 188.567824),
        (0.61392435, 179.479609),
        (0.6199632, 170.433025),
        (0.6262878, 161.442436),
        (0.6329553, 152.497801),
        (0.63994665, 143.592289),
        (0.64733805, 134.768881),
        (0.65514855, 126.000625),
        (0.663454349999999, 117.2889),
        (0.67229355, 108.680625),
        (0.681742349999999, 100.140049),
        (0.69187695, 91.699776),
        (0.70284975, 83.36420416),
        (0.71473695, 75.14462596),
        (0.727748099999999, 67.05644544),
        (0.74209275, 59.11764544),
        (0.75805665, 51.34868964),
        (0.776039849999999, 43.77410244),
        (0.7966329, 36.42846736),
        (0.820654949999999, 29.354724),
        (0.8494395, 22.60812304),
        (0.8852535, 16.27154244),
        (0.932307, 10.46846025),
        (0.99812475, 5.50887841),
    ],
)
def test_sound_speed_squared_at_v_rel(v_rel, truth_cs_sq):
    # Arrange
    df = load_hugo_with_cs()
    spline = make_spline_from_truth_df(df)

    # Act
    c_sq = spline.sound_speed_squared_at_v_rel(v_rel, "constant")

    # Assert
    assert c_sq == pytest.approx(truth_cs_sq, rel=0.1, abs=1)
