# Copyright (C) 2021 Reid Ginoza
# All rights reserved.
import numpy as np
import pandas as pd
import pytest
from scipy.interpolate import InterpolatedUnivariateSpline

from seos.splines import spline_eos
from seos.splines import default_spline


def test_import():
    assert True


def test_init():
    v = [0.5, 0.6, 0.7, 0.8, 0.9, 1]
    p = [10, 8, 6, 4, 2, 0]

    assert spline_eos.SplineEOS(v, p, rho_0=1.9, p0=0, e0=0)


def test_new_spline_from_coeffs_not_enough():
    # Arrange
    v = [0.5, 0.6, 0.7, 0.8, 0.9, 1]
    p = [10, 8, 6, 4, 2, 0]
    spline = spline_eos.SplineEOS(v, p)

    new_coeffs = np.array([2, 3, 4, 5, 6])

    # Act
    with pytest.raises(ValueError):
        spline.new_spline_from_coeffs(new_coeffs)


def test_new_spline_from_coeffs_no_zeros():
    # Arrange
    v = [0.5, 0.6, 0.7, 0.8, 0.9, 1]
    p = [10, 8, 6, 4, 2, 0]
    spline = spline_eos.SplineEOS(v, p)

    new_coeffs = np.array([2, 3, 4, 5, 6, 7])

    # Act
    new_spline = spline.new_spline_from_coeffs(new_coeffs)

    # Assert
    # check it initialized, then check coeffs
    assert new_spline
    assert new_spline._data[9] == pytest.approx(
        np.array([2, 3, 4, 5, 6, 7, 0, 0, 0, 0])
    )


def test_new_spline_from_coeffs_with_zeros():
    # Arrange
    v = [0.5, 0.6, 0.7, 0.8, 0.9, 1]
    p = [10, 8, 6, 4, 2, 0]
    spline = spline_eos.SplineEOS(v, p)

    new_coeffs = np.array([2, 3, 4, 5, 6, 7, 0, 0, 0, 0])

    # Act
    new_spline = spline.new_spline_from_coeffs(new_coeffs)

    # Assert
    # check it initialized, then check coeffs
    assert new_spline
    assert new_spline._data[9] == pytest.approx(
        np.array([2, 3, 4, 5, 6, 7, 0, 0, 0, 0])
    )


def test_default_spline_classes_few_nodes():
    # Act
    new_spline = default_spline.few_nodes_spline()

    # Assert
    assert new_spline.__class__ == spline_eos.SplineEOS


def test_default_spline_classes_many_nodes():
    # Act
    new_spline = default_spline.many_nodes_spline()

    # Assert
    assert new_spline.__class__ == spline_eos.SplineEOS


# from lx17_jwl_hugo.csv
# v is relative volume
# energy in MJ/kg which the same as kJ/g
@pytest.mark.parametrize(
    "v,truth_e",
    [
        (0.609327021, 10),
        (0.616345283, 8.982),
        (0.623772102, 8.0191),
        (0.6316313, 7.1112),
        (0.639991937, 6.2583),
        (0.648886164, 5.4604),
        (0.658440481, 4.7175),
        (0.668726086, 4.0297),
        (0.679822996, 3.3969),
        (0.691922127, 2.8192),
        (0.705163798, 2.2964),
        (0.719818628, 1.8287),
        (0.736203432, 1.416),
        (0.754754358, 1.0584),
        (0.776062248, 0.75571),
        (0.801160737, 0.50809),
        (0.831514622, 0.31551),
        (0.869902735, 0.17794),
        (0.921670134, 0.095403),
        (0.999527782, 0.067891),
        (np.array([0.921670134, 0.999527782]), np.array([0.095403, 0.067891])),
    ],
)
def test__energy_hugo(v, truth_e):
    # Arrange
    hugo = default_spline.tarver_jwl_spline()

    # Act
    results_e = hugo.energy_hugo(v, units="kJ/g")

    assert results_e == pytest.approx(truth_e, rel=1e-2)


def test__reference_state_missing():
    t = default_spline.tarver_jwl_spline()
    assert t._reference_state_missing


@pytest.mark.parametrize(
    "rho_0,p0,e0",
    [
        (None, 1.0, 1.0),
        (1.0, None, 1.0),
        (1.0, 1.0, None),
        (None, None, 1.0),
        (None, 1.0, None),
        (1, None, None),
        (None, None, None),
    ],
)
def test__reference_state_missing_actually_missing(rho_0, p0, e0):
    t = default_spline.tarver_jwl_spline()
    t.rho_0 = rho_0
    t.p0 = p0
    t.e0 = e0

    assert t._reference_state_missing()


@pytest.mark.parametrize(
    "rho_0,p0,e0",
    [
        (None, 1.0, 1.0),
        (1.0, None, 1.0),
        (1.0, 1.0, None),
        (None, None, 1.0),
        (None, 1.0, None),
        (1, None, None),
        (None, None, None),
    ],
)
def test__energy_hugo_at_one_point_reference_missing(rho_0, p0, e0):
    t = default_spline.tarver_jwl_spline()
    t.rho_0 = rho_0
    t.p0 = p0
    t.e0 = e0
    with pytest.raises(spline_eos.ReferenceStateMissing):
        t.energy_hugo(0.8, "kJ/g")


# # from lx17_jwl_hugo.csv
# # v is relative volume
# # Us in km/s = mm/us
@pytest.mark.parametrize(
    "v,truth_Us",
    [
        (0.60932702149437, 11.408),
        (0.616345282774686, 11.0059999999999),
        (0.6237721021611, 10.599),
        (0.631631299734748, 10.189),
        (0.639991937109453, 9.7735),
        (0.648886163907623, 9.3536),
        (0.658440481128162, 8.9283),
        (0.668726085582897, 8.497),
        (0.679822996217257, 8.0593),
        (0.691922126979514, 7.6142),
        (0.705163797890061, 7.1607),
        (0.719818628377101, 6.698),
        (0.736203431751429, 6.2246),
        (0.754754358161648, 5.7386),
        (0.776062247932537, 5.2377),
        (0.801160736815543, 4.7187),
        (0.831514622435617, 4.1772),
        (0.869902735284716, 3.606),
        (0.921670134017127, 2.9948),
        (0.999527782150165, 2.3283),
    ],
)
def test_Us_from_v_hugoniot(v, truth_Us):
    t = default_spline.tarver_jwl_spline()

    assert t.Us_from_v_hugoniot(v) == pytest.approx(truth_Us, rel=1e-2)


def make_dUs_dmu_case():
    df = pd.DataFrame(
        [
            (0.60932702149437, 11.408),
            (0.616345282774686, 11.0059999999999),
            (0.6237721021611, 10.599),
            (0.631631299734748, 10.189),
            (0.639991937109453, 9.7735),
            (0.648886163907623, 9.3536),
            (0.658440481128162, 8.9283),
            (0.668726085582897, 8.497),
            (0.679822996217257, 8.0593),
            (0.691922126979514, 7.6142),
            (0.705163797890061, 7.1607),
            (0.719818628377101, 6.698),
            (0.736203431751429, 6.2246),
            (0.754754358161648, 5.7386),
            (0.776062247932537, 5.2377),
            (0.801160736815543, 4.7187),
            (0.831514622435617, 4.1772),
            (0.869902735284716, 3.606),
            (0.921670134017127, 2.9948),
            (0.999527782150165, 2.3283),
        ],
        columns=["v", "Us"],
    )
    df["mu"] = 1 - df["v"]
    spline = InterpolatedUnivariateSpline(
        x=df.mu.iloc[::-1], y=df.Us.iloc[::-1]
    )
    deriv_spline = spline.derivative(1)
    df["dUs_dmu"] = deriv_spline(df.mu)
    mu = df.mu.iloc[::-1].to_numpy()
    dUs_dmu = df.dUs_dmu.iloc[::-1].to_numpy()
    return mu, dUs_dmu


@pytest.mark.parametrize(
    "mu,truth_dUs_dmu",
    [
        # doesn't seem to do well below mu = 0.1
        # (0.00047221784983497717, 7.121487301948184),
        # (0.07832986598287295, 10.30628381462375),
        (0.13009726471528404, 13.442658024695842),
        (0.16848537756438298, 16.4397544826357),
        (0.198839263184457, 19.308133174057623),
        (0.22393775206746303, 22.148174468528786),
        (0.24524564183835196, 24.89489496522627),
        (0.263796568248571, 27.57293203083318),
        (0.280181371622899, 30.254592552583766),
        (0.294836202109939, 32.95446329891806),
        (0.30807787302048595, 35.53024156692307),
        (0.32017700378274305, 38.14137824135803),
        (0.33127391441710297, 40.71786110352531),
        (0.341559518871838, 43.20574491221423),
        (0.351113836092377, 45.89277565137034),
        (0.360008062890547, 48.49622381908446),
        (0.36836870026525204, 50.920263816991834),
        (0.3762278978389, 53.49343728184413),
        (0.38365471722531397, 56.091839285581635),
        (0.39067297850563, 58.45082467695526),
    ],
)
def test_dUs_dmu(mu, truth_dUs_dmu):
    # Arrange
    t = default_spline.tarver_jwl_spline()

    # Act
    results_dUs_dmu = t.dUs_dmu(mu)

    # Assert
    assert results_dUs_dmu == pytest.approx(truth_dUs_dmu, rel=1e-2)


# from lx17_jwl_hugo.csv
# v is relative volume
# temperature in K
@pytest.mark.parametrize(
    "v,truth_temp",
    [
        (0.609327021, 5430.6),
        (0.616345283, 4859.7),
        (0.623772102, 4324.9),
        (0.6316313, 3826.0),
        (0.639991937, 3362.7),
        (0.648886164, 2934.6),
        (0.658440481, 2541.5),
        (0.668726086, 2182.8),
        (0.679822996, 1858.3),
        (0.691922127, 1567.5),
        (0.705163798, 1309.7),
        (0.719818628, 1084.5),
        (0.736203432, 890.97),
        (0.754754358, 728.39),
        (0.776062248, 595.63),
        (0.801160737, 491.31),
        (0.831514622, 413.48),
        (0.869902735, 359.3),
        (0.921670134, 324.0),
        (0.999527782, 298.28),
    ],
)
def test_temperature_hugo(v, truth_temp):
    # Arrange
    t = default_spline.tarver_jwl_spline()

    # Act
    results_temp = t.temperature_hugo(v)

    # Assert
    assert results_temp == pytest.approx(truth_temp, rel=1e-1, abs=1.0)
