# Copyright (C) 2021 Reid Ginoza
# All rights reserved.
from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

from seos.splines import spline_eos
from seos.splines import default_spline

v_spec = np.linspace(0.5, 0.999)

s = default_spline.tarver_jwl_spline()
reid = np.array([s.tau_specific_volume(v) for v in v_spec])

gamma = 0.894
v0 = 1 / 1.905


def clay_tau(v_spec):
    return (v_spec / v0) ** (-gamma)


def reid_analytical(v_spec):
    return np.exp(gamma * (1 - v_spec / v0))


clay = clay_tau(v_spec)
reid2 = reid_analytical(v_spec)

fig: plt.Figure
ax: plt.Axes
fig, ax = plt.subplots()
ax.plot(v_spec, reid, label="Reid Numerical", alpha=0.5)
ax.plot(v_spec, reid2, label="Reid Analytical", alpha=0.5)
ax.plot(v_spec, clay, label="Matthew Clay", alpha=0.5)
ax.set_xlabel("Specific Volume (cc/g)")
ax.set_ylabel(r"Tau $\tau\left(v\right)$")
ax.legend()
ax.set_title(r"$\tau\left(v\right)$ comparison")
fig.savefig(Path(__file__).parent / "tau_check_1.png")
