# Copyright (C) 2021 Reid Ginoza
# All rights reserved.
from pathlib import Path
from typing import Iterable, Tuple

import numpy as np
import pandas as pd
import pytest
from scipy.interpolate import InterpolatedUnivariateSpline

from seos.splines import spline_eos
from seos.splines import default_spline
from seos.utilities.conversions import kg_to_g


def test_import():
    assert True


def get_orig_eos() -> Tuple[spline_eos.SplineEOS, Iterable, float]:
    f = Path(__file__).parent / "files" / "distended_hugo.csv"
    data = pd.read_csv(f)

    rho_0 = 1.75
    rho_dist = 1.70
    gamma = 1

    data["V(--)"] = data["v_specific"] * rho_0
    data["P(GPA)"] = data["P_Hugo"]

    target_p = data["P_Dist"]
    target_density = rho_dist

    return (
        spline_eos.SplineEOS(
            x=data["V(--)"], y=data["P(GPA)"], rho_0=rho_0, gamma=gamma
        ),
        target_p,
        target_density,
    )


def get_dist_as_orig_eos() -> Tuple[spline_eos.SplineEOS, Iterable, float]:
    f = Path(__file__).parent / "files" / "distended_hugo.csv"
    data = pd.read_csv(f)

    rho_0 = 1.75
    rho_dist = 1.70
    gamma = 1

    data["V(--)"] = data["v_specific"] * rho_dist
    data["P(GPA)"] = data["P_Dist"]

    target_p = data["P_Hugo"]
    target_density = rho_0

    return (
        spline_eos.SplineEOS(
            x=data["V(--)"], y=data["P(GPA)"], rho_0=rho_dist, gamma=gamma
        ),
        target_p,
        target_density,
    )


@pytest.mark.parametrize(
    "v_spec,p_hugo,p_dist_truth",
    [
        (0.209508571455773, 40150.7565107181, 41033.8552757952),
        (0.228548571469373, 12582.6772303205, 12852.6996436284),
        (0.247588571482974, 5208.29670681463, 5317.41321319157),
        (0.266628571496574, 2406.24074360271, 2455.48397064332),
        (0.285668571510175, 1177.89891140535, 1201.45817125491),
        (0.304708571523776, 597.191782940771, 608.871616973651),
        (0.323748571537377, 309.870116306307, 315.79916513829),
        (0.342788571550977, 163.404111534071, 166.464347438083),
        (0.361828571564578, 87.1777809690923, 88.7765284375156),
        (0.380868571578178, 46.905973028784, 47.7486707881991),
        (0.399908571591779, 25.3883865680589, 25.835408018066),
        (0.418948571605379, 13.7911362596895, 14.0292123123717),
        (0.43798857161898, 7.49792764329946, 7.62488136860709),
        (0.457028571632581, 4.06451221560919, 4.13203665715391),
        (0.476068571646181, 2.1834259306922, 2.21902947701754),
        (0.495108571659782, 1.14960927155733, 1.16801510590527),
        (0.514148571673383, 0.580363534606141, 0.589489918344577),
        (0.533188571686983, 0.26684556145767, 0.270968338062873),
        (0.552228571700584, 0.0945726626291986, 0.0960086800760226),
        (0.571268571714185, 0.000545240391834799, 0.000553379464605982),
    ],
)
def test_hugo_to_dist(v_spec, p_hugo, p_dist_truth):
    # Arrange
    spline, target_p, rho_dist = get_orig_eos()

    # Act
    result_p = spline.pressure_distended(v_spec, rho_dist)

    # Assert
    assert result_p == pytest.approx(p_dist_truth, rel=1e-6, abs=1e-6)


@pytest.mark.parametrize(
    "v_spec,p_hugo_truth,p_dist",
    [
        (0.209508571455773, 40150.7565107181, 41033.8552757952),
        (0.228548571469373, 12582.6772303205, 12852.6996436284),
        (0.247588571482974, 5208.29670681463, 5317.41321319157),
        (0.266628571496574, 2406.24074360271, 2455.48397064332),
        (0.285668571510175, 1177.89891140535, 1201.45817125491),
        (0.304708571523776, 597.191782940771, 608.871616973651),
        (0.323748571537377, 309.870116306307, 315.79916513829),
        (0.342788571550977, 163.404111534071, 166.464347438083),
        (0.361828571564578, 87.1777809690923, 88.7765284375156),
        (0.380868571578178, 46.905973028784, 47.7486707881991),
        (0.399908571591779, 25.3883865680589, 25.835408018066),
        (0.418948571605379, 13.7911362596895, 14.0292123123717),
        (0.43798857161898, 7.49792764329946, 7.62488136860709),
        (0.457028571632581, 4.06451221560919, 4.13203665715391),
        (0.476068571646181, 2.1834259306922, 2.21902947701754),
        (0.495108571659782, 1.14960927155733, 1.16801510590527),
        (0.514148571673383, 0.580363534606141, 0.589489918344577),
        (0.533188571686983, 0.26684556145767, 0.270968338062873),
        (0.552228571700584, 0.0945726626291986, 0.0960086800760226),
        (0.571268571714185, 0.000545240391834799, 0.000553379464605982),
    ],
)
def test_dist_to_hugo(v_spec, p_hugo_truth, p_dist):
    # Arrange
    spline, target_p, rho_dist = get_dist_as_orig_eos()

    # Act
    result_p = spline.pressure_distended(v_spec, rho_dist)

    # Assert
    assert result_p == pytest.approx(p_hugo_truth, rel=1e-6, abs=1e-6)
