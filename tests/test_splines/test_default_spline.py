# Copyright (C) 2021 Reid Ginoza
# All rights reserved.
import numpy as np
import pytest

from seos.splines import default_spline


def test_import():
    assert True


def test_few_nodes_spline():
    assert default_spline.few_nodes_spline()


def test_few_nodes_spline_with_args():
    assert default_spline.few_nodes_spline(1.4)
    assert default_spline.few_nodes_spline(1.4, 4)
    assert default_spline.few_nodes_spline(1.4, 4, 3)


def test_many_nodes_spline():
    assert default_spline.many_nodes_spline()


def test_many_nodes_spline_with_args():
    assert default_spline.many_nodes_spline(1.1)
    assert default_spline.many_nodes_spline(1.1, 2)
    assert default_spline.many_nodes_spline(1.1, 2, 3)


def test_tarver_jwl_spline():
    rho_0 = 1.905  # g/cc
    p0 = 3.3027e-04  # GPa
    e0 = 6.7890e-02  # MJ/kg or kJ/g
    tarver = default_spline.tarver_jwl_spline()
    assert tarver
    assert tarver.rho_0 == rho_0
    assert tarver.p0 == p0
    assert tarver.e0 == e0
