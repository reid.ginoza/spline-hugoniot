# Copyright (C) 2021 Reid Ginoza
# All rights reserved.
import pandas as pd

TARVER_RHO_0 = 1.905


def down_sample():
    df = pd.read_csv("hugo3.csv")
    return df[::60].sort_values(by="VOL(CC/G)", ignore_index=True)


def calc_v_rel(df: pd.DataFrame):
    df["V(--)"] = df["VOL(CC/G)"] * TARVER_RHO_0
    return df


def save_file(df):
    df.to_csv("down_sampled_hugo.csv", index=False)


if __name__ == "__main__":
    df = down_sample()
    df = calc_v_rel(df)
    save_file(df)
