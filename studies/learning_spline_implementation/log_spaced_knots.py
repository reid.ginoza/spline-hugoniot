# Copyright (C) 2021 Reid Ginoza
# All rights reserved.
"""Learning with log-spaced knots."""
import matplotlib.pyplot as plt
import numpy as np
from scipy.interpolate import InterpolatedUnivariateSpline


def get_t(spline: InterpolatedUnivariateSpline):
    return spline._data[8].copy()


def get_c(spline: InterpolatedUnivariateSpline):
    return spline._data[9].copy()


def get_k(spline: InterpolatedUnivariateSpline):
    return spline._data[5]


def f(x):
    return np.sin(5.0 * np.pi * (x - 0.6))


def make_spline(x, y, k: int):
    return InterpolatedUnivariateSpline(x=x, y=y, k=k)


def show_examples(x, y, k: int, n_rand: int = 20):
    spline = make_spline(x, y, k)
    c_orig = get_c(spline)
    print(f"Current coeffs ({len(c_orig)}): {c_orig}")

    num_right = k + 1
    fixed_loc = -num_right - 1

    num_new = len(c_orig) - num_right - 1

    for i in range(n_rand):

        rand_coeff = np.sort(np.random.random(num_new) * 10)[::-1]
        fixed_coeff = [-1.0]
        right_coeffs = np.zeros(num_right)
        print(f"rand_coeff: {rand_coeff}")
        print(
            f"fixed_loc: {fixed_loc}, coeff at fixed_loc: {get_c(spline)[fixed_loc]} "
            f"new fixed_coeff: {fixed_coeff}"
        )
        print(f"right_coeffs: {right_coeffs}")
        new_coeff = np.concatenate([rand_coeff, fixed_coeff, right_coeffs])
        print(f"new_coeff ({len(new_coeff)}): {new_coeff}")
        assert len(new_coeff) == len(
            get_c(spline)
        ), f"Number of coeffs {len(new_coeff)} does not match original {len(get_c(spline))}"

        t = get_t(spline)
        k = get_k(spline)
        rand_spline = spline._from_tck((t, new_coeff, k))

        fig: plt.Figure
        ax: plt.Axes
        fig, ax = plt.subplots()
        ax.plot(x, rand_spline(x))
        ax.scatter(x, rand_spline(x))
        ax.set_title(f"Random {i}")


X = np.logspace(np.log10(0.6), 0, num=21)
Y = f(X)

fig: plt.Figure
ax: plt.Axes
fig, ax = plt.subplots()
ax.plot(X, Y)
ax.scatter(X, Y)
ax.set_title("Data")

show_examples(X, Y, 3, 20)
