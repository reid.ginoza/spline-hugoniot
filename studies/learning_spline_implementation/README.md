# README
The goal of this study is to establish how the
`scipy.interpolate.InterpolatedUnivariateSpline` coefficients
are determined.

# Model Function
Let's use y=sin(2 pi x) on x in [0, 1].

Since the boundaries are both at y=0, we'll also look at
y=sin(2 pi x) + 5 on x in [0, 1].

# Lessons

## Extrapolation

When creating the spline, one has the option to determine the type of
extrapolation. This has no impact on how the knots or coefficients
are determined. They appear to set the default for `__call__`, which
also has a keyword argument that may change the extrapolation method.
In other words,
the `t`, `c`, and `k`, values that are passed to make a new spline are 
unaffected by the extrapolation mode.

## Knots
The docstring for `UnivariateSpline.get_knots()` informs us that:
> Internally, the knot vector contains ``2*k`` additional boundary knots.

Also, of note, the `UnivariateSpline.get_knots()` method provides only
the **interior knots**.

Assume that the number of data points is 21. Then,

| Degree  | Left End Knots | Interior Knots | Right End Knots | Total |
| ------- | -------------- | -------------- | --------------- | ----- |
| 1 | 2 | 19 | 2 | 23 |
| 2 | 3 | 18 | 3 | 24 |
| 3 | 4 | 17 | 4 | 25 |
| 4 | 5 | 16 | 5 | 26 |
| 5 | 6 | 15 | 6 | 27 |
| k | k+1 | 21 - k - 1 | k + 1 | 21 + k + 1 |

With 21 data points, the number of interior points is 19 and the number of
boundary points is 2. Here is the construction from degree 1 splines.

First, knots at all 21 data points are placed, and then 1 more knot is placed
at each boundary. 
Then, for each increase in degree, place a knot halfway between all the 
interior knots (do not place one between a boundary knot and the interior
knot immediately next to it). This decreases the interior knots by one.
Increase the number of knots at each of the two boundaries by one. This nets
a total of 1 knot increase per degree increase.

For n = 10, we have the following diagram:

```
k
1: x x x x x x x x x x
2: x  x x x x x x x  x
3: x   x x x x x x   x
4: x    x x x x x    x
5: x     x x x x     x
```

For n data points:

| Degree  | Left End Knots | Interior Knots | Right End Knots | Total |
| ------- | -------------- | -------------- | --------------- | ----- |
| 1 | 2 | n-2 | 2 | n+2 |
| 2 | 3 | n-3 | 3 | n+3 |
| 3 | 4 | n-4 | 4 | n+4 |
| 4 | 5 | n-5 | 5 | n+5 |
| 5 | 6 | n-6 | 6 | n+6 |
| k | k+1 | n - k - 1 | k + 1 | n + k + 1 |

## Coefficients
There are as many coefficients as there are b-splines, and there are
as many b-splines as there are knots. (This was confusing to me at first since
some knots are repeated.)

The coefficients start with the left most b-spline, which corresponds to using
all but one of the left boundary knots plus the first interior knot.
The support of each b-spline begins at some left knot and moves to the right
k + 1 knots.
For example, for linear b-splines, a b-spline in the interior has its left
bottom at one knot, its peak at the next, and its right bottom at the third knot,
so that the hat is two knots wide, and "touches" or "covers" three knots.

But, because there needs to be as many b-splines as there are knots, there are 
a few more knots that don't really "cover" anything. I tend to think of
this as using up the boundary knots only to continue the pattern,
but I'm not sure if that is accurate.

In any case, the final k+1 coeffs are 0 and the b-splines appear to be
horizontal lines at y=0.

## Keeping a right boundary constant
At least in this implementation, the right most coefficient that is non-zero
determines the right boundary value when the spline is evaluated.

My understanding of this is basically nonexistant. I've only noticed this
by trying it out, and I don't know if it applies to the left boundary or
any other point.

# Takeaways
I can probably fix the value of the spline at the right boundary.

# Code Implementation Notes
Private attribute of the `InterpolatedUnivariateSpline` instance.
```
_data =
        0: x,  # data x
        1: y,  # data y
        2: w,  # weights for spline fitting
        3: xb,  # beginning of x? i.e. lowest x value
        4: xe,  # end of x? i.e. highest value of x
        5: k,  # degree
        6: s,  # smoothing, set to 0 in this class
        7: n,
        8: t,  # knots
        9: c,  # coeffs
        10: fp,
        11: fpint,
        12: nrdata,
        13: ier
```

# Copyright
Copyright (C) 2021 Reid Ginoza
All rights reserved.