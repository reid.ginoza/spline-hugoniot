# Copyright (C) 2021 Reid Ginoza
# All rights reserved.
"""Fit sin(2 pi x) in several different ways to study coeffs."""
"""
I will need to determine the number of coefficients in the following
cases, assuming degree 3:

* Both ends are extrapolated
* No extrapolation, i.e. raises error

Then, confirm with the following degrees:

* degree 4
* degree 2
* degree 1
"""
from dataclasses import dataclass
from itertools import count
from textwrap import dedent, indent
from typing import Callable, List, Optional, Sequence, Tuple

from matplotlib.cm import get_cmap
import matplotlib.pyplot as plt
from matplotlib.ticker import MaxNLocator
import numpy as np
from scipy.interpolate import InterpolatedUnivariateSpline

log_spaced = True
if log_spaced:
    X = np.logspace(-1, 0, num=21)
else:
    X = np.linspace(0, 1, num=21)

Y_TRUE = np.sin(2 * np.pi * X) + 5

# Is this necessary?
# num_t = len(X) + 2*(k - 1)
# viridis = get_cmap("viridis", num_t)
viridis = get_cmap("viridis")


def new_spline_from_coeffs(spline: InterpolatedUnivariateSpline, coeffs):
    t = spline._data[8]
    k = spline._data[5]
    return spline._from_tck([t, coeffs, k])


def get_basis(
    spline: InterpolatedUnivariateSpline,
) -> List[InterpolatedUnivariateSpline]:
    basis = []
    for i in range(len(spline._data[9])):
        b = np.zeros_like(spline._data[9])
        b[i] = 1.0
        basis.append(new_spline_from_coeffs(spline, b))
    return basis


def plot_basis(
    basis: List[InterpolatedUnivariateSpline], name: Optional[str] = None
):
    fig: plt.Figure
    ax: plt.Axes
    fig, ax = plt.subplots()

    x = np.linspace(X.min() * 0.9 - 0.1, X.max() * 1.1, 10000)
    num_b = len(basis)
    for i, b in enumerate(basis):
        ax.plot(
            x,
            b(x),
            label=f"B-pline {i}",
            color=viridis((i / num_b * 2) % 1),
            alpha=0.8,
        )

    knots = basis[0]._data[8]
    ax.scatter(knots, np.zeros_like(knots))

    ax.set_ylim(-0.5, 2)
    ax.legend(bbox_to_anchor=(0, 0, 1.0, -1.0), loc="upper left")
    title = "Basis Splines"
    if name:
        title += "\n" + name
    ax.set_title(title)
    plt.tight_layout()
    return fig, ax


def plot_individual_b_splines(
    basis: List[InterpolatedUnivariateSpline], extra_name: Optional[str] = None,
) -> Tuple[plt.Figure, Sequence[plt.Axes]]:
    # ugh. why can't I figure this out.
    num_b = len(basis)
    s = np.sqrt(num_b)
    ncols = int(s) if s == int(s) else int(np.floor(s)) + 1
    nrows = int(s)
    while ncols * nrows < num_b:
        nrows += 1

    fig: plt.Figure
    axs: np.ndarray[plt.Axes]
    fig, axs = plt.subplots(
        nrows=nrows, ncols=ncols, sharex="all", sharey="all"
    )

    x = np.linspace(X.min() * 0.9 - 0.1, X.max() * 1.1, 10000)
    for i, b in enumerate(basis):
        row_idx = i // ncols
        col_idx = i % ncols
        axs[row_idx, col_idx].plot(x, b(x), alpha=0.8)
        knots = b._data[8]
        axs[row_idx, col_idx].scatter(
            knots, np.zeros_like(knots), s=0.5, color="orange"
        )
        axs[row_idx, col_idx].set_title(f"B-spline {i}")
    axs[0, 0].set_ylim(-0.5, 2)
    if extra_name:
        fig.suptitle(extra_name)
    return fig, axs


def plot_against_truth(
    spline: Callable, name=None
) -> Tuple[plt.Figure, plt.axes]:
    fig: plt.Figure
    ax: plt.Axes
    fig, ax = plt.subplots()

    ax.plot(X, Y_TRUE, label="Truth")

    kwargs = {"label": name} if name else {}
    ax.plot(X, spline(X), **kwargs)

    if name:
        ax.set_title(f"{name} Against Truth")

    return fig, ax


def print_spline_info(spline: InterpolatedUnivariateSpline, name: str) -> None:
    # _data =
    #         0,1: x,y,  # data
    #         2: w,  # weights for spline fitting
    #         3: xb,  # beginning of x? i.e. lowest x value
    #         4: xe,  # end of x? i.e. highest value of x
    #         5: k,  # degree
    #         6: s,  # smoothing, set to 0 in this class
    #         7: n,
    #         8: t,
    #         9: c,
    #         10: fp,
    #         11: fpint,
    #         12: nrdata,
    #         13: ier

    coeffs = spline.get_coeffs()
    knots = spline.get_knots()

    internal_knots_t = spline._data[8]
    internal_coeffs_c = spline._data[9]

    print(
        dedent(
            f"""\
        ==== {name} ====
        Number of Coeffs: {len(coeffs)}
        Number of 0 Coeffs: {len([c for c in coeffs if c == 0])}
        Number of Internal Coeffs (c) _data[9]: {len(internal_coeffs_c)}
        Number of 0 Internal Coeffs: {len([c for c in internal_coeffs_c if c == 0])}

        Number of Knots from get_knots(): {len(knots)}
        Number of Internal Knots (t) _data[8]: {len(internal_knots_t)}

        Number of Data Points: {len(X)}

        Full Coeffs from get_coeffs():
            {indent(str(coeffs), " " * 12)}
        Full Internal Coeffs (c) _data[9]:
            {indent(str(internal_coeffs_c), " " * 12)}

        Full Knots from get_knots():
            {indent(str(knots), " " * 12)}
        Full Internal Knots (t) _data[8]:
            {indent(str(internal_knots_t), " " * 12)}

        Full truth y:
            {indent(str(Y_TRUE), " " * 12)}
        Full truth x:
            {indent(str(X), " " * 12)}
        """
        )
    )


@dataclass
class SplineStudy:
    spline: InterpolatedUnivariateSpline
    name: str

    def __post_init__(self):
        self.basis = get_basis(self.spline)

    @classmethod
    def new_from_k(cls, k: int):
        names = {
            1: "Linear",
            2: "Quadratic",
            3: "Cubic",
            4: "Quartic",
            5: "Quintic",
        }
        try:
            name = names[k]
        except KeyError as e:
            raise ValueError(f"Degree {k} unsupported") from e

        return cls(
            spline=InterpolatedUnivariateSpline(x=X, y=Y_TRUE, k=k), name=name
        )


def run_study(k: int = 3, show_plots: bool = True):

    ss = SplineStudy.new_from_k(k)

    if show_plots:
        plot_against_truth(ss.spline, ss.name)

    print_spline_info(ss.spline, ss.name)

    b_fig, b_ax = plot_basis(ss.basis, ss.name)
    b_many_fig, b_many_axs = plot_individual_b_splines(ss.basis, ss.name)
    return ss, (b_fig, b_ax), (b_many_fig, b_many_axs)


def plot_knots(knots: List[np.array], show_v_lines=True):
    fig: plt.Figure
    ax: plt.Axes
    fig, ax = plt.subplots()

    for idx, ks in enumerate(knots, start=1):
        ax.scatter(
            ks,
            np.full_like(ks, idx),
            label=f"Degree {idx} N = {len(ks)}",
            alpha=0.8,
        )

    if show_v_lines:
        deg_1_knots = knots[0]
        ax.vlines(deg_1_knots, 1, 5, color="grey", alpha=0.6)

        halves = 0.5 * (deg_1_knots[1:] + deg_1_knots[:-1])
        ax.vlines(halves, 1, 5, color="grey", alpha=0.6, linestyle="--")

    ax.yaxis.set_major_locator(MaxNLocator(integer=True))
    ax.set_title("Knot Placement by Degree")
    return fig, ax


def plot_coeffs_with_knots(coeffs: List[np.array], knots: List[np.array]):
    assert len(coeffs) == len(knots) == 5, "Only wrote for five plots"

    fig: plt.Figure
    axs: Sequence[
        plt.Axes
    ]  # Not correct but used to help with PyCharm code autofill
    fig, axs = plt.subplots(nrows=3, ncols=2, sharex="all", sharey="all")

    plot_idx = {
        0: (0, 0),
        1: (0, 1),
        2: (1, 0),
        3: (1, 1),
        4: (2, 0),
    }

    for idx, coeff, knot in zip(count(), coeffs, knots):
        # create dummy spline and then make a new spline using tck
        k = idx + 1
        sp = InterpolatedUnivariateSpline(x=range(4), y=range(4))._from_tck(
            (knot, coeff, k)
        )

        axs[plot_idx[idx]].scatter(x=knot, y=coeff, alpha=0.8)
        axs[plot_idx[idx]].hlines(y=5, xmin=0, xmax=1, color="grey", alpha=0.5)
        axs[plot_idx[idx]].plot(
            knot, sp(knot), color="green", alpha=0.5, linestyle="--"
        )
        axs[plot_idx[idx]].set_title(f"Degree {idx+1}")
    return fig, axs


if __name__ == "__main__":
    show_plots = False

    ss_results = [run_study(k, show_plots) for k in range(1, 6)]

    knots = [ss_res[0].spline._data[8] for ss_res in ss_results]

    plot_knots(knots)

    coeffs = [ss_res[0].spline._data[9] for ss_res in ss_results]

    plot_coeffs_with_knots(coeffs, knots)

    quintic = ss_results[-1][0]
    t = quintic.spline._data[8]
    c = quintic.spline._data[9].copy()
    c[-7] = 1.0  # blah! shouldn't have to hard code this!
    k = quintic.spline._data[5]
    new_quintic = quintic.spline._from_tck([t, c, k])
    plot_against_truth(new_quintic, "Changed last coeff")
