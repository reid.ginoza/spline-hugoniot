# Copyright (C) 2021 Reid Ginoza
# All rights reserved.
"""Plots specifically for LX-17 data.

Code is currently split in two locations.
Most is in the main_lx17.py module for now.
"""
from pathlib import Path

import pandas as pd

from seos.splines.default_spline import many_nodes_spline
from seos.splines.spline_eos import SplineEOS


def get_data():
    file = Path(__file__).parent / "LX-17_data_v3_only_data.csv"
    return pd.read_csv(file, comment="#")


if __name__ == "__main__":
    v0 = 1 / 1.9
    data = get_data()

    spline = many_nodes_spline()

    for _, row in data.iterrows():
        print("\n====")
        print(row)

        p_spline = spline(row["v_spec"] / v0)
        print(f"P (no adjustment): {p_spline}")

        p_distended = spline.pressure_distended(row["v_spec"], row["rho_0"])
        print(f"P (distended): {p_distended}")
