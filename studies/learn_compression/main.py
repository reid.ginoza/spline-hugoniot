from pathlib import Path
from typing import Optional, Tuple

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from scipy.interpolate import InterpolatedUnivariateSpline


def v_rel_to_mu(v_rel):
    return 1 / v_rel - 1


def v_rel_and_mu_label(vol: str) -> str:
    print(f"vol: {vol}")
    v_rel = float(vol)
    mu = v_rel_to_mu(v_rel)
    return f"{v_rel}\n{mu:.3f}"


def get_data() -> pd.DataFrame:
    data_file = Path(__file__).parent / "lx17_jwl_ur.csv"
    return pd.read_csv(data_file)


def _parse_ax_arg(ax: Optional[plt.Axes] = None) -> Tuple[plt.Figure, plt.Axes]:
    if ax is None:
        return plt.subplots()
    else:
        return ax.get_figure(), ax


def plot_hugoniot(df: pd.DataFrame, ax: Optional[plt.Axes] = None) -> Tuple[plt.Figure, plt.Axes]:
    fig, ax = _parse_ax_arg(ax)

    ax.plot(df["V(--)"], df["P(GPA)"], "-o", label="data", alpha=0.8)
    spline = InterpolatedUnivariateSpline(df["V(--)"], df["P(GPA)"])
    v_rel = np.linspace(0.3, 1.0)
    ax.plot(v_rel, spline(v_rel), label="Spline", alpha=0.8)
    ax.set_xlabel(r"Volume (cm$^3$/g) / Compression $\mu$")
    return fig, ax


def change_vol_labels(ax: plt.Axes):
    old_labels = [t.get_text() for t in ax.get_xticklabels()]
    new_labels = [v_rel_and_mu_label(v_rel) for v_rel in old_labels]
    ax.set_xticklabels(new_labels)
    fig.tight_layout()
    ax.get_figure().show()
    return ax


if __name__ == "__main__":
    df = get_data()
    fig, ax = plot_hugoniot(df)
    change_vol_labels(ax)

