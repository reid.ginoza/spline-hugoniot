"""What kinds of shapes can be made with quadratic, rational polynomials?"""
import matplotlib.pyplot as plt
import numpy as np
from numpy.polynomial.polynomial import Polynomial
from numpy.random import default_rng


NUM_POLY = 20
RNG = default_rng()


def pick_random_denominator():
    while True:
        den = Polynomial(RNG.random(3) * 20 - 10)
        if np.all(np.logical_or(den.roots() > 1., den.roots() < 0.3)):
            return den


def str_poly(poly: Polynomial):
    return ", ".join(f"{c:.2f}" for c in poly.coef)


if __name__ == "__main__":
    for i in range(NUM_POLY):
        fig: plt.Figure
        ax: plt.Axes
        fig, ax = plt.subplots()

        num = Polynomial(RNG.random(3) * 20 - 10)
        den = pick_random_denominator()

        x_plot = np.linspace(0.3, 1.)
        ax.plot(x_plot, num(x_plot) / den(x_plot))
        ax.axhline(0, color="black")
        ax.axvline(0, color="black")
        ax.set_title(f"Ratio {i}\n{str_poly(num)} / {str_poly(den)}")

