"""Fits data within."""
from dataclasses import dataclass
from pathlib import Path
from typing import Optional

import matplotlib.pyplot as plt
import numpy as np
from numpy.random import default_rng
import pandas as pd

from fitting import MG, fit_mg

# for reproducibility
_RNG_SEED = int("10" * 16, 2)
RNG = default_rng(_RNG_SEED)

RHO_0 = 1.9


def get_data():
    data_file = Path(__file__).parent / "LX-17_data_v3_only_data.csv"
    df = pd.read_csv(data_file, comment="#")
    v0 = 1 / RHO_0
    df["v_rel"] = df["v_spec"] / v0
    return df


def add_noise(x:np.array, tol=None, type="binary"):
    if tol is None:
        tol = x * 0.05

    try:
        noise_size = x.size
    except AttributeError:
        noise_size = 1

    if type == "binary":
        noise = _make_binary_noise(tol, noise_size)
    elif type == "beta":
        noise = _make_beta_noise(tol, noise_size)
    else:
        raise ValueError(f"Unrecognized noise type {type}.")

    return x + noise


def _make_beta_noise(tol, noise_size):
    noise_sample = RNG.beta(0.5, 0.5, size=noise_size)
    centered_noise = noise_sample - 0.5
    scaled_noise = centered_noise * 2 * tol
    return scaled_noise


def _make_binary_noise(tol, noise_size):
    noise_sample = RNG.integers(0, 1, size=noise_size, endpoint=True)
    noise_sample[noise_sample == 0] = -1
    return tol * noise_sample


def sample_data_distribution(df, up_tol=None, us_tol=None, type="binary"):
    sampled_data = df.copy()
    sampled_data["UP"] = add_noise(df["UP"], up_tol, type)
    sampled_data["US"] = add_noise(df["US"], us_tol, type)
    return sampled_data


def test_data_sample():
    df = get_data()
    noisy_df = sample_data_distribution(df, up_tol=0.01, us_tol=None)

    fig, ax = plt.subplots()
    ax.scatter(df["UP"], df["US"], label="Original", zorder=2.5)
    ax.scatter(noisy_df["UP"], noisy_df["US"], label="Sampled", zorder=2.5)
    ax.legend()

    orig_mg = fit_mg(df["UP"], df["US"])
    noisy_mg = fit_mg(noisy_df["UP"], noisy_df["US"])

    up_min = np.min(np.concatenate((df["UP"], noisy_df["UP"])))
    up_max = np.max(np.concatenate((df["UP"], noisy_df["UP"])))
    up_plot = np.linspace(up_min, up_max)
    ax.plot(up_plot, orig_mg(up_plot), label="Orig. Fit")
    ax.plot(up_plot, noisy_mg(up_plot), label="Noisy Fit")




if __name__ == "__main__":
    for _ in range(10):
        test_data_sample()

