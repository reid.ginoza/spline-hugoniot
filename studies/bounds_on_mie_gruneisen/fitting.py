"""Attempts to look at how varied the Mie-Gruneisen can be when fit to data."""
from dataclasses import dataclass
from pathlib import Path
from typing import Optional

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import scipy.optimize as so

RHO_0 = 1.9


def get_data():
    data_file = Path(__file__).parent / "LX-17_data_v3_only_data.csv"
    df = pd.read_csv(data_file, comment="#")
    v0 = 1 / RHO_0
    df["v_rel"] = df["v_spec"] / v0
    return df


@dataclass
class MG:
    c0: float
    s1: float
    rho_0: Optional[float] = None

    def __call__(self, up):
        """Returns Us = c0 + s1 * up"""
        return self.c0 + self.s1 * up

    def up_from_v_rel(self, v_rel):
        """Solves for up using the conservation of mass equation.

        1 - nu = up/Us,
        where nu is relative volume.
        """
        # Us = c0 + s1 * up
        # 1 - nu = up/(c0 + s1 * up),
        # c0 (1 - nu) + s1 * up (1 - nu) = up
        # c0 (1 - nu) = up - s1 * up (1 - nu) = up (1 - s1 (1 - nu))
        # up = c0 (1 - nu) / (1 - s1 (1 - nu))

        top = self.c0 * (1 - v_rel)
        bot = 1 - self.s1 * (1 - v_rel)
        up = top / bot
        if np.any(up < 0):
            raise ValueError
        return up

    def p_from_v_rel(self, v_rel):
        if self.rho_0 is None:
            raise RuntimeError("Need to set attribute rho_0")
        up = self.up_from_v_rel(v_rel)
        Us = self(up)
        return self.rho_0 * Us * up


def eval_mg_in_opt(up, c0, s1):
    return MG(c0=c0, s1=s1)(up)


def fit_mg(up, Us, return_cov=False):
    params, cov = so.curve_fit(eval_mg_in_opt, up, Us, bounds=([0.1,0.1], [10.,10.]))
    mg = MG(*params, rho_0=RHO_0)
    if return_cov:
        return mg, cov
    else:
        return mg


def _eval_mg_in_opt_p_v(v_rel, c0, s1):
    return MG(c0, s1, rho_0=RHO_0).p_from_v_rel(v_rel)


def fit_mg_with_p_v(v, p, return_cov=False):
    params, cov = so.curve_fit(_eval_mg_in_opt_p_v, v, p, bounds=([.9,0.9], [5.,5.]))
    mg = MG(*params, rho_0=RHO_0)
    if return_cov:
        return mg, cov
    else:
        return mg


def plot_mg(mg:MG, df:pd.DataFrame):
    fig: plt.Figure
    ax_left: plt.Axes
    ax_right: plt.Axes
    fig, (ax_left, ax_right) = plt.subplots(ncols=2)

    plot_mg_us_up(mg, df, ax_left)
    plot_mg_p_v(mg, df, ax_right)

    return fig, (ax_left, ax_right)


def plot_mg_us_up(mg, df, ax):
    ax.scatter(df["UP"], df["US"], label="Data")
    up_plot = np.linspace(0., df["UP"].max() * 1.1)
    ax.plot(up_plot, mg(up_plot), label="Linear MG", color="orange")

    ax.set_xlabel(r"$u_p$ (mm/$\mu s$)")
    ax.set_ylabel(r"$U_s$ (mm/$\mu s$)")


def _make_p_v_points(mg, df):
    low_bound = min(df["v_rel"].min() * 0.95, 0.65)
    v_rel_plot = np.linspace(low_bound, 1.)
    try:
        p_plot = mg.p_from_v_rel(v_rel_plot)
    except ValueError:
        new_v_rel_plot = []
        p_plot = []
        for v_rel in v_rel_plot:
            try:
                p = mg.p_from_v_rel(v_rel)
            except ValueError:
                pass
            else:
                new_v_rel_plot.append(v_rel)
                p_plot.append(p)
        if not new_v_rel_plot:
            raise
        v_rel_plot = np.array(new_v_rel_plot)
        p_plot = np.array(p_plot)
    return v_rel_plot, p_plot


def plot_mg_p_v(mg, df, ax):
    ax.scatter(df["v_rel"], df["P"], label="Data")

    v_rel_plot, p_plot = _make_p_v_points(mg, df)
    ax.plot(v_rel_plot, p_plot, label="Linear MG", color="orange")

    ax.set_xlabel(r"$\nu$ (--)")
    ax.set_ylabel(r"$P$ (GPa)")



if __name__ == "__main__":
    df = get_data()

    mg = fit_mg(df["UP"], df["US"])
    print(mg)

    fig, axs = plot_mg(mg, df)

    pv_mg = fit_mg_with_p_v(df["v_rel"], df["P"])
    print(pv_mg)
    fig2, axs2 = plot_mg(pv_mg, df)
