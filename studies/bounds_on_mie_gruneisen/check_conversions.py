"""Study to verify conversions between thermodynamic variables."""
from dataclasses import dataclass
from pathlib import Path
from typing import Optional

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import scipy.optimize as so


RHO_0 = 1.905  # g/cc


def get_known_values():
    file = Path(__file__).parent / "lx17_jwl_ur.csv"
    return pd.read_csv(file)


def check_nu_us_up(df:pd.DataFrame, verbose=False):
    """Prints a check on the continuity equation.

    1 - nu = up/Us,
    where nu is relative volume.
    """
    lhs = 1 - df["V(--)"]
    if verbose:
        print(f"lhs\n{lhs}")

    rhs = df["UP(KM/S)"] / df["US(KM/S)"]
    if verbose:
        print(f"rhs\n{rhs}")

    diff = lhs - rhs
    if verbose:
        print(f"diff\n{diff}")

    print(f"Expecting the difference to be near 0 everywhere.\n{diff.describe()}")


def check_p_us_up(df: pd.DataFrame, verbose=False):
    """Prints a check on the pressure equation.

    P = rho_0 * Us * up.
    """
    if verbose:
        print(f"P: {df['P(GPA)']}")

    rhs = RHO_0 * df['US(KM/S)'] * df['UP(KM/S)']
    if verbose:
        print(f"rhs: {rhs}")

    diff = df['P(GPA)'] - rhs
    if verbose:
        print(f"diff\n{diff}")

    print(f"Expecting the difference to be near 0 everywhere.\n{diff.describe()}")


def check_up_from_v_rel(df:pd.DataFrame, verbose=False):
    """Prints a check to calculate up from v_rel.

    1 - nu = up/Us
    up = Us (1 - nu)
    """
    if verbose:
        print(f"up: {df['UP(KM/S)']}")

    rhs = df['US(KM/S)'] * (1 - df["V(--)"])
    if verbose:
        print(f"rhs: {rhs}")

    diff = df['UP(KM/S)'] - rhs
    if verbose:
        print(f"diff: {diff}")

    print(f"Expecting the difference to be near 0 everywhere.\n{diff.describe()}")


if __name__ == "__main__":
    df = get_known_values()
    check_nu_us_up(df, verbose=True)
    check_up_from_v_rel(df, verbose=True)
    check_p_us_up(df, verbose = True)
