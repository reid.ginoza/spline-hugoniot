# Copyright (C) 2021 Reid Ginoza
# All rights reserved.
"""Common functions used in this sound_speed_oscillations study."""
# Copyright (C) 2021 Reid Ginoza
# All rights reserved.
"""Checks the sound speed oscillations observed earlier.

The 2021-05-15_verify_sound_speed run showed oscillations in the verification
of the sound speed vs. the BCAT sound speed. Initially, the sound speed
calculation was just for verifying real (non-imaginary) values, but the plan
is to implement a monotonicity constraint on the sound speed. Thus,
we need to make sure the oscillations do not appear.

The first attempt is to test this with a smaller sample size.
"""
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from scipy.interpolate import InterpolatedUnivariateSpline

from seos.splines.spline_eos import SplineEOS

TARVER_RHO_0 = 1.905


def get_data() -> pd.DataFrame:
    df = pd.read_csv("hugo3.csv")
    df["V(--)"] = df["VOL(CC/G)"] * TARVER_RHO_0
    return df.sort_values(by="V(--)", ignore_index=True)


def get_truth(df: pd.DataFrame) -> pd.DataFrame:
    truth = df[["V(--)", "CS(KM/S)"]].copy()
    truth["Cs_sq"] = np.square(truth["CS(KM/S)"])
    return truth


def make_spline(df: pd.DataFrame) -> SplineEOS:
    return SplineEOS(
        x=df["V(--)"],
        y=df["P(GPA)"],
        rho_0=TARVER_RHO_0,
        p0=3.3027e-04,
        e0=6.7890e-02,
        t0=298.15,
        cv=1.3052623990693266,
        gamma=8.94000000e-01,  # assume gamma_kind = "constant"
    )


def make_spline_fewer_data_points(df: pd.DataFrame) -> SplineEOS:
    down_selected = df[::60]
    return SplineEOS(
        x=down_selected["V(--)"],
        y=down_selected["P(GPA)"],
        rho_0=TARVER_RHO_0,
        p0=3.3027e-04,
        e0=6.7890e-02,
        t0=298.15,
        cv=1.3052623990693266,
        gamma=8.94000000e-01,  # assume gamma_kind = "constant"
    )


def plot_hugo(truth: pd.DataFrame, spline: SplineEOS):
    fig: plt.Figure
    ax: plt.Axes

    fig, ax = plt.subplots()

    ax.plot(truth["V(--)"], truth["P(GPA)"], label="Truth", alpha=0.8)
    ax.plot(truth["V(--)"], spline(truth["V(--)"]), label="Spline", alpha=0.8)
    ax.legend()
    ax.set_xlabel("Relative volume " r"$\nu$")
    ax.set_ylabel("Pressure (GPa)")

    return fig, ax


def make_v_spec_spline(spline: SplineEOS) -> InterpolatedUnivariateSpline:
    # _data =
    #         0,1: x,y,  # data

    v_rels = spline._data[0]
    pressures = spline(v_rels)
    v_specs = v_rels / spline.rho_0

    return InterpolatedUnivariateSpline(x=v_specs, y=pressures)
