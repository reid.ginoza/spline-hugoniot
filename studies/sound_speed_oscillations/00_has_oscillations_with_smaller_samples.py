# Copyright (C) 2021 Reid Ginoza
# All rights reserved.
"""Checks the sound speed oscillations observed earlier.

The 2021-05-15_verify_sound_speed run showed oscillations in the verification
of the sound speed vs. the BCAT sound speed. Initially, the sound speed
calculation was just for verifying real (non-imaginary) values, but the plan
is to implement a monotonicity constraint on the sound speed. Thus,
we need to make sure the oscillations do not appear.

The first attempt is to test this with a smaller sample size.
"""
from typing import Sequence

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

from seos.splines.spline_eos import SplineEOS

from cs_support import TARVER_RHO_0, get_data, get_truth, make_spline, plot_hugo


def plot_sound_speed_sq(
    truth: pd.DataFrame, v_sample: np.ndarray, spline_Cs_sq: np.ndarray
):
    fig: plt.Figure
    ax: plt.Axes

    fig, ax = plt.subplots()

    ax.plot(
        truth["V(--)"], truth["Cs_sq"], label="Truth", alpha=0.5, color="blue"
    )
    # this got to be too many, since there were over a thousand of these
    # ax.scatter(
    #     truth["V(--)"], truth["Cs_sq"], label="Truth", alpha=0.5, color="blue"
    # )
    ax.plot(
        v_sample, spline_Cs_sq, label="SplineEOS", alpha=0.5, color="orange"
    )
    ax.scatter(
        v_sample, spline_Cs_sq, label="SplineEOS", alpha=0.5, color="orange"
    )
    ax.set_ylabel(r"Sound Speed Squared $C_S^2$ (km/s)$^2$")
    ax.legend()

    fig.suptitle("Sound Speed Squared Comparison")

    return fig, ax


if __name__ == "__main__":
    df = get_data()
    truth = get_truth(df)

    spline = make_spline(df)

    h_fig, h_axs = plot_hugo(df, spline)

    for sample_size in (10, 20, 25, 50, 100, 500, 1000):

        v_sample = np.linspace(
            df["V(--)"].min(), df["V(--)"].max(), num=sample_size,
        )

        spline_Cs_sq: np.ndarray = spline.sound_speed_squared_at_v_rel(
            v_sample, gamma_kind="constant"
        )

        cs_fig, cs_ax = plot_sound_speed_sq(truth, v_sample, spline_Cs_sq)
        cs_ax.set_title(cs_ax.get_title() + f"\n$N={sample_size}$")
