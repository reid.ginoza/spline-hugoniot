# Copyright (C) 2021 Reid Ginoza
# All rights reserved.
"""Checks the sound speed oscillations observed earlier.

The 2021-05-15_verify_sound_speed run showed oscillations in the verification
of the sound speed vs. the BCAT sound speed. Initially, the sound speed
calculation was just for verifying real (non-imaginary) values, but the plan
is to implement a monotonicity constraint on the sound speed. Thus,
we need to make sure the oscillations do not appear.

The first attempt is to test this with a smaller sample size.
"""
from typing import Sequence

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from scipy.interpolate import InterpolatedUnivariateSpline
from scipy.optimize import minimize_scalar

from seos.splines.spline_eos import SplineEOS

from cs_support import (
    TARVER_RHO_0,
    get_data,
    get_truth,
    make_spline_fewer_data_points,
    plot_hugo,
    make_v_spec_spline,
    make_spline,
)


def plot_sound_speed_sq(
    truth: pd.DataFrame, v_sample: np.ndarray, spline_Cs_sq: np.ndarray
):
    fig: plt.Figure
    ax: plt.Axes

    fig, ax = plt.subplots()

    ax.plot(
        truth["V(--)"], truth["Cs_sq"], label="Truth", alpha=0.5, color="blue"
    )
    # ax.scatter(truth["V(--)"], truth["Cs_sq"], label="Truth", alpha=0.5, color="blue")
    ax.plot(
        v_sample, spline_Cs_sq, label="SplineEOS", alpha=0.5, color="orange"
    )
    ax.scatter(
        v_sample, spline_Cs_sq, label="SplineEOS", alpha=0.5, color="orange"
    )
    ax.set_ylabel(r"Sound Speed Squared $C_S^2$ (km/s)$^2$")
    ax.legend()

    # delete if not used
    # diff = truth["Cs_sq"] - spline_Cs_sq
    # axs[1].plot(truth["V(--)"], diff)
    # axs[1].set_title(r"Residual")
    #
    # axs[1].set_ylabel(r"$C_S^2$ (km/s)$^2$")
    # axs[1].set_xlabel(r"Relative Volume $\nu$")

    fig.suptitle("Sound Speed Squared Comparison")

    return fig, ax


def plot_sound_speed(
    truth: pd.DataFrame, v_sample: np.ndarray, spline_Cs_sq: np.ndarray
):
    fig: plt.Figure
    ax: plt.Axes

    fig, ax = plt.subplots()

    ax.plot(
        truth["V(--)"],
        np.sqrt(truth["Cs_sq"]),
        label="Truth",
        alpha=0.5,
        color="blue",
    )
    # ax.scatter(truth["V(--)"], truth["Cs_sq"], label="Truth", alpha=0.5, color="blue")

    ax.plot(
        v_sample,
        np.sqrt(spline_Cs_sq),
        label="SplineEOS",
        alpha=0.5,
        color="orange",
    )
    ax.scatter(
        v_sample,
        np.sqrt(spline_Cs_sq),
        label="SplineEOS",
        alpha=0.5,
        color="orange",
    )
    ax.set_ylabel(r"Sound Speed $C_S$ (km/s)")
    ax.legend()

    # delete if not used
    # diff = truth["Cs_sq"] - spline_Cs_sq
    # axs[1].plot(truth["V(--)"], diff)
    # axs[1].set_title(r"Residual")
    #
    # axs[1].set_ylabel(r"$C_S^2$ (km/s)$^2$")
    # axs[1].set_xlabel(r"Relative Volume $\nu$")

    fig.suptitle("Sound Speed Comparison")

    return fig, ax


def spline_non_increasing(spline: InterpolatedUnivariateSpline):
    """Determines a spline is non-increasing by its derivative.

    Uses the opposite of the derivative (derivative times -1), and finds
    the minimum on a bounded region. If that minimum is greater or equal
    to zero, then the derivative is less than or equal 0, meaning the
    spline itself is monotonically decreasing on that interval.
    """
    derivative = spline.derivative()
    x_min, x_max = spline._data[0].min(), spline._data[0].max()
    opt_res = minimize_scalar(lambda v: -derivative(v), bounds=(x_min, x_max))
    print(opt_res)
    if not opt_res.success:
        raise RuntimeError(f"Could not find minimum.")

    return opt_res.fun >= 0


if __name__ == "__main__":
    df = get_data()
    truth = get_truth(df)

    spline = make_spline_fewer_data_points(df)
    print(f"Spline non-increasing: {spline_non_increasing(spline)}")
    spline_v_spec = make_v_spec_spline(spline)
    print(
        f"Specific Volume spline non-increasing: {spline_non_increasing(spline_v_spec)}"
    )

    h_fig, h_axs = plot_hugo(df, spline)

    for sample_size in (10, 20, 25, 50, 100, 500, 1000):

        v_sample = np.linspace(
            df["V(--)"].min(), df["V(--)"].max(), num=sample_size,
        )

        spline_Cs_sq: np.ndarray = spline.sound_speed_squared_at_v_rel(
            v_sample, gamma_kind="constant"
        )

        cs_fig, cs_ax = plot_sound_speed_sq(truth, v_sample, spline_Cs_sq)
        cs_ax.set_title(cs_ax.get_title() + f"\n$N={sample_size}$")

        cs2fig, cs2_ax = plot_sound_speed(truth, v_sample, spline_Cs_sq)
        cs2_ax.set_title(cs2_ax.get_title() + f"\n$N={sample_size}$")

    # check original sampling was not monotonic
    old_spline = make_spline(df)
    print(f"Spline non-increasing: {spline_non_increasing(old_spline)}")
