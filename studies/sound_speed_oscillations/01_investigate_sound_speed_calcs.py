# Copyright (C) 2021 Reid Ginoza
# All rights reserved.
"""Checks the sound speed oscillations observed earlier.

The 2021-05-15_verify_sound_speed run showed oscillations in the verification
of the sound speed vs. the BCAT sound speed. Initially, the sound speed
calculation was just for verifying real (non-imaginary) values, but the plan
is to implement a monotonicity constraint on the sound speed. Thus,
we need to make sure the oscillations do not appear.

The first attempt is to test this with a smaller sample size.
"""
import matplotlib.pyplot as plt

from seos.splines.spline_eos import SplineEOS

from cs_support import (
    TARVER_RHO_0,
    get_data,
    get_truth,
    make_spline,
    plot_hugo,
    make_spline_fewer_data_points,
    make_v_spec_spline,
)


def plot_hugo_and_v_spec(spline: SplineEOS):
    fig: plt.Figure
    ax: plt.Axes
    fig, ax = plt.subplots()

    v = spline._data[0]
    ax.plot(v, spline(v), label="Relative Volume", alpha=0.5)

    spline_v_spec = make_v_spec_spline(spline)
    v_spec = v / spline.rho_0
    ax.plot(v, spline_v_spec(v_spec), label="Specific Volume Spline", alpha=0.5)
    ax.legend()
    ax.set_title(
        "Confirm v spec spline behaves\n" "the same as the v rel spline"
    )
    return fig, ax


def plot_v_rel_and_v_spec_derivative(spline: SplineEOS):
    fig: plt.Figure
    ax_rel: plt.Axes
    ax_spec: plt.Axes
    fig, (ax_rel, ax_spec) = plt.subplots(nrows=2)

    # v_rel = spline._data[0]
    # trying more data points
    v_rel = spline.make_test_points()
    ax_rel.plot(v_rel, spline(v_rel), label="Spline")
    ax_rel.plot(v_rel, spline(v_rel, nu=1), label="Derivative")
    ax_rel.set_title("Relative Volume Spline")

    v_spec = v_rel / spline.rho_0
    spline_v_spec = make_v_spec_spline(spline)
    ax_spec.plot(v_spec, spline_v_spec(v_spec), label="Spline")
    ax_spec.plot(v_spec, spline_v_spec(v_spec, nu=1), label="Derivative")
    ax_spec.set_title("Specific Volume Spline")

    return fig, (ax_rel, ax_spec)


if __name__ == "__main__":
    df = get_data()
    truth = get_truth(df)

    # spline = make_spline(df)

    spline = make_spline_fewer_data_points(df)

    # already verified so reducing plots during development
    # h_fig, h_axs = plot_hugo(df, spline)
    #
    # plot_hugo_and_v_spec(spline)

    plot_v_rel_and_v_spec_derivative(spline)
