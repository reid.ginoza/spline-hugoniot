# Copyright (C) 2021 Reid Ginoza
# All rights reserved.
from pathlib import Path

from matplotlib import pyplot as plt
import numpy as np
import pandas as pd
from scipy.optimize import curve_fit
from scipy.interpolate import InterpolatedUnivariateSpline

from seos.optimizer.spline_opt import OptimizeSpline, SavePopulation
from seos.optimizer.opt_plots import sanity_check_plot, plot_spline_against_data
from seos.splines.default_spline import make_many_v_pts
from seos.splines.spline_eos import SplineEOS


def get_others_spline(name):
    if name == "dallman":
        file = Path(__file__).parent / "v_dallman_lx17.csv"
        rho_0 = 1.913
    elif name == "analytical_dallman":
        file = Path(__file__).parent / "v_analytical_dallman_lx17.csv"
        rho_0 = 1.913
    else:
        raise ValueError("unrecognized Hugoniot")

    data = pd.read_csv(file)
    data.sort_values(by="v_spec", inplace=True)
    try:
        spline = SplineEOS(
            x=data["v_spec"], y=data["P(GPA)"], ext="raise", rho_0=rho_0
        )
    except Exception as e:
        print(name, data, sep="\n")
        raise e
    return spline


a_d = get_others_spline("analytical_dallman")
d = get_others_spline("dallman")

possible_v_min = [0.38]
possible_v_max = [0.54]
for spline in (a_d, d):
    possible_v_min.append(spline._data[0].min())
    possible_v_max.append(spline._data[0].max())

v_min = max(possible_v_min)
v_max = min(possible_v_max)
v_spec = np.linspace(v_min, v_max)

fig: plt.Figure
ax: plt.Axes
fig, ax = plt.subplots()

for spline, label in zip((a_d, d), ("Analytical", "Tabular")):
    ax.plot(v_spec, spline(v_spec), label=label)
ax.legend()

fig: plt.Figure
ax: plt.Axes
fig, ax = plt.subplots()

ax.plot(v_spec, a_d(v_spec) - d(v_spec))
ax.set_title("Difference")
