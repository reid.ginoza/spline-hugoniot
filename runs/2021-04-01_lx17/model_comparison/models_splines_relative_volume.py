# Copyright (C) 2021 Reid Ginoza
# All rights reserved.
from collections.abc import Callable
from pathlib import Path
import pickle
from typing import Iterable, List, Optional, Tuple, Union

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from scipy.interpolate import InterpolatedUnivariateSpline
from scipy.optimize import root_scalar

from seos.optimizer.opt_plots import (
    ARA_BLUE,
    ARA_DARK_BLUE,
    ARA_BRIGHT_BLUE,
    ARA_ORANGE,
    ARA_GREEN,
    ARA_GREY,
    ARA_DARK_GREY,
)
from seos.splines.spline_eos import SplineEOS, RootSolveError

from plotting import (
    COLORS,
    add_data_to_ax,
)


def get_data():
    data = pd.read_csv(
        Path(__file__).parent / "LX-17_data_v3_only_data.csv", comment="#"
    )
    data = data.append(
        {"rho_0": 1.9, "v_spec": 1 / 1.9, "P": 0.0, "Label": "Reference Point"},
        ignore_index=True,
    )
    return data


def get_overdriven_data():
    data = pd.read_csv(
        Path(__file__).parent / "LX-17_overdriven_data_with_v3.csv", comment="#"
    )
    return data


def get_others_spline_v_rel(name):
    csv_dir = Path(__file__).parent / "with_relative_volumes"
    if name == "dallman":
        file = csv_dir / "v_rel_dallman_lx17.csv"
        rho_0 = 1.913
    elif name == "gustavsen":
        file = csv_dir / "v_rel_gustavsen_lx17.csv"
        rho_0 = 1.900
    elif name == "jackson":
        file = csv_dir / "v_rel_jackson_lx17.csv"
        rho_0 = 1.90
    elif name == "tarver":
        file = csv_dir / "v_rel_tarver_lx17.csv"
        rho_0 = 1.905
    else:
        raise ValueError("unrecognized Hugoniot")

    data = pd.read_csv(file)
    data.sort_values(by="V(--)", inplace=True)
    try:
        spline = SplineEOS(
            x=data["V(--)"],
            y=data["P(GPA)"],
            ext="raise",
            rho_0=rho_0,
            gamma=1.0,
        )
    except Exception as e:
        print(name, data, sep="\n")
        raise e
    return spline


def get_spline_hugoniot() -> SplineEOS:
    result_file = Path(
        r"C:\Users\rginoza\Documents\CODE\spline-eos\runs\lx17\round_6b_fewer_fewer_nodes\opt_result.pickle"
    )
    with result_file.open("rb") as f:
        opt_res = pickle.load(f)
    best_coeffs = opt_res.X
    spline_hugoniot: SplineEOS = (
        opt_res.problem._base_spline.new_spline_from_coeffs(best_coeffs)
    )
    assert spline_hugoniot.rho_0 == 1.9
    return spline_hugoniot


def process_rmsd(
    splines: Iterable[Tuple[str, SplineEOS, float, float]],
    data: pd.DataFrame,
    od_data: pd.DataFrame,
):
    for name, spline, v_rel_min, v_rel_max in splines:
        print("=====", name, sep="\n")
        try:
            print(
                f"RMSD: {spline.residual_objective_with_distending(data['v_spec'], data['P'], data['rho_0'])}"
            )
        except ValueError as e:
            print(f"RMSD failed. Will move on even though {e}")

        try:
            print(
                f"Overdriven Constraints: {''.join(v_distance(spline, od_data, v_rel_min, v_rel_max))}"
            )
        except ValueError as e:
            print(
                f"Overdriven Constraints failed. Will move on even though {e}"
            )


def v_distance(spline: SplineEOS, od_data: pd.DataFrame, v_rel_min, v_rel_max):
    v_dists = []
    for od_v_spec, od_p in np.column_stack((od_data["v_spec"], od_data["P"])):
        try:
            v_dists.append(
                spline_calc_v_dist_at_p(
                    spline, od_v_spec, od_p, v_rel_min, v_rel_max
                )
            )
        except (RootSolveError, ValueError) as e:
            print(f"appending None. {e}")
            v_dists.append(None)

    return v_dists


def spline_calc_v_dist_at_p(spline, v_spec_data, p_data, v_rel_min, v_rel_max):
    def to_solve(v):
        return spline(v) - p_data

    root_sol = root_scalar(to_solve, bracket=[v_rel_min, v_rel_max],)

    if not root_sol.converged:
        raise RootSolveError

    v_spec = root_sol.root / 1.9
    return (
        f"Pressure {p_data}\n"
        f"v_spec data {v_spec_data}\n"
        f"v root_sol {v_spec}\n"
        f"Diff {v_spec_data - v_spec}\n\n"
    )


if __name__ == "__main__":
    data = get_data()
    od_data = get_overdriven_data()
    all_data = pd.concat([data, od_data], ignore_index=True)
    no_ref_data = data[data["Label"] != "Reference Point"].sort_values("v_spec")

    dallman = get_others_spline_v_rel("dallman")
    gustavsen = get_others_spline_v_rel("gustavsen")
    jackson = get_others_spline_v_rel("jackson")
    tarver = get_others_spline_v_rel("tarver")
    spline_hugoniot = get_spline_hugoniot()

    splines = (
        ("Dallman", dallman, 0.73784, 0.989),
        ("Gustavsen", gustavsen, 0.561085, 0.9977),
        ("Jackson", jackson, 0.6385482, 0.999842130),
        ("Tarver", tarver, 0.5731869193, 0.9954940794),
        ("Spline", spline_hugoniot, 0.49, 0.95204617),
    )

    process_rmsd(splines, no_ref_data, od_data)
