# Copyright (C) 2021 Reid Ginoza
# All rights reserved.
import matplotlib

matplotlib.use("TkAgg")

from collections.abc import Callable
from pathlib import Path
import pickle
from typing import Iterable, List, Optional, Tuple, Union

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from scipy.interpolate import InterpolatedUnivariateSpline
from scipy.optimize import root_scalar

from seos.optimizer.opt_plots import (
    ARA_BLUE,
    ARA_DARK_BLUE,
    ARA_BRIGHT_BLUE,
    ARA_ORANGE,
    ARA_GREEN,
    ARA_GREY,
    ARA_DARK_GREY,
)
from seos.splines.spline_eos import SplineEOS

from plotting import (
    COLORS,
    add_data_to_ax,
)


def get_data():
    data = pd.read_csv(
        Path(__file__).parent / "LX-17_data_v3_only_data.csv", comment="#"
    )
    data = data.append(
        {"rho_0": 1.9, "v_spec": 1 / 1.9, "P": 0.0, "Label": "Reference Point"},
        ignore_index=True,
    )
    return data


def get_overdriven_data():
    data = pd.read_csv(
        Path(__file__).parent / "LX-17_overdriven_data_with_v3.csv", comment="#"
    )
    return data


def get_others_spline(name):
    if name == "dallman":
        file = (
            Path(__file__).parent
            / ".."
            / "existing_models"
            / "dallman"
            / "v_dallman_lx17.csv"
        )
        rho_0 = 1.913
    elif name == "gustavsen":
        file = (
            Path(__file__).parent
            / ".."
            / "existing_models"
            / "gustavsen"
            / "v_gustavsen.csv"
        )
        rho_0 = 1.900
    elif name == "jackson":
        file = (
            Path(__file__).parent
            / ".."
            / "existing_models"
            / "Jackson"
            / "v_jackson_lx17.csv"
        )
        rho_0 = 1.90
    elif name == "tarver":
        file = (
            Path(__file__).parent
            / ".."
            / "existing_models"
            / "tarver_cth"
            / "v_tarver_jwl.csv"
        )
        rho_0 = 1.905
    else:
        raise ValueError("unrecognized Hugoniot")

    data = pd.read_csv(file)
    data.sort_values(by="v_spec", inplace=True)
    try:
        spline = SplineEOS(
            x=data["v_spec"], y=data["P(GPA)"], ext="raise", rho_0=rho_0
        )
    except Exception as e:
        print(name, data, sep="\n")
        raise e
    return spline


def get_spline_hugoniot() -> SplineEOS:
    result_file = Path(
        r"C:\Users\rginoza\Documents\CODE\spline-eos\runs\2021-04-01_lx17\round_6b_fewer_fewer_nodes\opt_result.pickle"
    )
    with result_file.open("rb") as f:
        opt_res = pickle.load(f)
    best_coeffs = opt_res.X
    base_spline = opt_res.problem._base_spline
    # adding this in because the class changed after this code was written
    base_spline.gamma_kind = "mg"

    spline_hugoniot: SplineEOS = base_spline.new_spline_from_coeffs(best_coeffs)

    assert spline_hugoniot.rho_0 == 1.9
    return spline_hugoniot


def get_v_spec_spline_hugoniot(data: pd.DataFrame, od_data: pd.DataFrame):
    v_rel_spline = get_spline_hugoniot()

    all_data = pd.concat([data, od_data], ignore_index=True)
    v_spec = np.linspace(
        all_data["v_spec"].min() * 0.99, all_data["v_spec"].max() * 1.01, 10000
    )

    v_rel_in = v_spec * v_rel_spline.rho_0

    return SplineEOS(v_spec, v_rel_spline(v_rel_in), rho_0=1.9)


def _evaluate_spline_at_vspec(
    v_spec: np.ndarray, spline: InterpolatedUnivariateSpline,
) -> Tuple[np.ndarray, np.ndarray]:
    """Only put interpolated values into the arrays.

    Assumes the spline's extrapolate argument ext="raise".
    """
    new_v_spec = []
    spline_p = []
    for v in v_spec:
        try:
            p = spline(v)
        except ValueError:
            pass
        else:
            new_v_spec.append(v)
            spline_p.append(p)
    return np.array(new_v_spec), np.array(spline_p)


def others_data_plot(
    data: pd.DataFrame, od_data: pd.DataFrame, with_overdriven: bool = True
):
    fig: plt.Figure
    ax: plt.Axes
    fig, ax = plt.subplots()

    ax = add_data_to_ax(ax, data, od_data, with_overdriven)

    dallman = get_others_spline("dallman")
    gustavsen = get_others_spline("gustavsen")
    jackson = get_others_spline("jackson")
    tarver = get_others_spline("tarver")

    if with_overdriven:
        all_data = pd.concat([data, od_data], ignore_index=True)
    else:
        all_data = data

    v_spec = np.linspace(
        all_data["v_spec"].min() * 0.9, all_data["v_spec"].max() * 1.05, 10000
    )

    # Not sure why this plot doesn't match the data
    ax.plot(
        *_evaluate_spline_at_vspec(v_spec, dallman),
        label="Dallman (Linear) Hugoniot",
        ls="--",
        color=COLORS["dallman"],
    )
    ax.plot(
        *_evaluate_spline_at_vspec(v_spec, gustavsen),
        label="Gustavsen (Piecewise) Hugoniot",
        ls="--",
        color=COLORS["gustavsen"],
    )
    ax.plot(
        *_evaluate_spline_at_vspec(v_spec, jackson),
        label="Jackson (Linear) Hugoniot",
        ls="--",
        color=COLORS["jackson"],
    )
    ax.plot(
        *_evaluate_spline_at_vspec(v_spec, tarver),
        label="Tarver (JWL) Hugoniot",
        ls="--",
        color=COLORS["tarver"],
    )

    if not with_overdriven:
        # determined by trial and error
        ax.set_xlim(0.375, 0.5355)
        ax.set_ylim(-7.085, 35.0)

    ax.legend()
    ax.set_xlabel("Volume (cc/g)")
    ax.set_ylabel("Pressure (GPa)")
    if with_overdriven:
        title = "Hugoniot and Overdriven Data"
    else:
        title = "Hugoniot Data"
    ax.set_title(title)
    return fig, ax


def all_models_plot(
    data: pd.DataFrame,
    od_data: pd.DataFrame,
    with_overdriven: bool = True,
    with_spline_hugo: bool = True,
) -> Tuple[plt.Figure, plt.Axes]:
    fig: plt.Figure
    ax: plt.Axes
    fig, ax = plt.subplots()

    ax = add_data_to_ax(ax, data, od_data, with_overdriven=True)

    dallman = get_others_spline("dallman")
    gustavsen = get_others_spline("gustavsen")
    jackson = get_others_spline("jackson")
    tarver = get_others_spline("tarver")
    spline_hugoniot = get_v_spec_spline_hugoniot(data, od_data)

    if with_overdriven:
        all_data = pd.concat([data, od_data], ignore_index=True)
    else:
        all_data = data

    v_spec = np.linspace(
        all_data["v_spec"].min() * 0.99, all_data["v_spec"].max() * 1.01, 10000
    )

    # Not sure why this plot doesn't match the data
    ax.plot(
        *_evaluate_spline_at_vspec(v_spec, dallman),
        label="Dallman (Linear) Hugoniot",
        ls="--",
        color=COLORS["dallman"],
    )
    ax.plot(
        *_evaluate_spline_at_vspec(v_spec, gustavsen),
        label="Gustavsen (Piecewise) Hugoniot",
        ls="--",
        color=COLORS["gustavsen"],
    )
    ax.plot(
        *_evaluate_spline_at_vspec(v_spec, jackson),
        label="Jackson (Linear) Hugoniot",
        ls="--",
        color=COLORS["jackson"],
    )
    ax.plot(
        *_evaluate_spline_at_vspec(v_spec, tarver),
        label="Tarver (JWL) Hugoniot",
        ls="--",
        color=COLORS["tarver"],
    )
    if with_spline_hugo:
        ax.plot(
            *_evaluate_spline_at_vspec(v_spec, spline_hugoniot),
            label="Spline Hugoniot",
            ls="--",
            color=COLORS["result"],
        )

    if not with_overdriven:
        # determined by trial and error
        ax.set_xlim(0.375, 0.5355)
        ax.set_ylim(-1, 25.0)

    ax.legend()
    ax.set_xlabel("Volume (cc/g)")
    ax.set_ylabel("Pressure (GPa)")

    if with_overdriven:
        title = "Spline Model Comparison\nHugoniot and Overdriven Data"
    else:
        title = "Spline Model Comparison\nHugoniot Data"
    ax.set_title(title)

    return fig, ax


def process_rmsd(splines: Iterable[Tuple[str, SplineEOS]], data: pd.DataFrame):
    print(
        "This does not match what is reported in the paper, since that "
        "uses the RMSD with distended EOS. see "
        "models_splines_relative_volume.py for that approach"
    )
    for name, spline in splines:
        print(name)
        try:
            print(
                f"RMSD: {spline.residual_objective(data['v_spec'], data['P'])}"
            )
        except ValueError as e:
            print(f"Will move on even though {e}")


def residual_plot(data: pd.DataFrame):
    """Specific to this data set."""
    data = data[data["Label"] != "Reference Point"]

    fig: plt.Figure
    axs: np.ndarray
    fig, axs = plt.subplots(2, 2, sharex=True, sharey=True)

    splines = (
        ("Gustavsen", gustavsen, (0, 0)),
        ("Jackson", jackson, (0, 1)),
        ("Tarver", tarver, (1, 0)),
        ("Spline Hugoniot", spline_hugoniot, (1, 1)),
    )

    for title, spline, ax_idx in splines:
        axs[ax_idx].axhline(0, color="black", alpha=0.7)
        axs[ax_idx].set_title(title)

        for v_spec, p, label in zip(data["v_spec"], data["P"], data["Label"]):
            if label == "Dallman":
                plot_kwargs = {"label": r"Dallman ($\rho_0=1.913$)"}
            else:
                plot_kwargs = {"label": label}

            name = label.split()[0].lower()
            plot_kwargs["color"] = COLORS[name]

            try:
                marker_name = label.split()[1]
            except IndexError:
                pass
            else:
                if marker_name == "Gun":
                    plot_kwargs["marker"] = "^"
                elif marker_name == "Wedge":
                    plot_kwargs["marker"] = "s"
                else:
                    raise RuntimeError(
                        "Unexpected Data Label. Cannot find marker."
                    )

            axs[ax_idx].scatter(v_spec, p - spline(v_spec), **plot_kwargs)

    handless_labels = [ax.get_legend_handles_labels() for ax in fig.axes]
    handles, labels = [sum(lol, []) for lol in zip(*handless_labels)]
    by_label = dict(zip(labels, handles))
    fig.legend(
        by_label.values(), by_label.keys(), loc=7,
    )
    # fig.tight_layout()
    fig.subplots_adjust(right=0.75)
    # fig.text(0.375, 0.04, "Volume (cc/g)", ha="center", va="center")
    fig.text(
        0.06,
        0.5,
        "Pressure (GPa)",
        ha="center",
        va="center",
        rotation="vertical",
    )
    # fig.suptitle("Residual Plots for Hugoniot Models")
    fig.set_size_inches(8.54, 4.9)
    t = fig.suptitle("Residual Plots for Hugoniot Models")
    t.set_position((0.45, 0.98))
    fig.text(0.45, 0.04, "Volume (cc/g)", ha="center", va="center")
    return fig, axs


if __name__ == "__main__":
    out_dir = Path(__file__).parent / "plots"
    out_dir.mkdir(exist_ok=True)

    data = get_data()
    od_data = get_overdriven_data()
    all_data = pd.concat([data, od_data], ignore_index=True)
    no_ref_data = data.loc[data["P"] > 0, ["v_spec", "P"]].sort_values("v_spec")

    dallman = get_others_spline("dallman")
    gustavsen = get_others_spline("gustavsen")
    jackson = get_others_spline("jackson")
    tarver = get_others_spline("tarver")
    spline_hugoniot = get_v_spec_spline_hugoniot(data, od_data)

    process_rmsd(
        (
            ("Dallman", dallman),
            ("Gustavsen", gustavsen),
            ("Jackson", jackson),
            ("Tarver", tarver),
            ("Spline", spline_hugoniot),
        ),
        no_ref_data,
    )

    fig, ax = all_models_plot(data, od_data, with_overdriven=True)
    fig.set_figwidth(1.2 * fig.get_figwidth())
    fig.savefig(out_dir / "spline_vs_others_hugo_od.png")
    fig.savefig(out_dir / "spline_vs_others_hugo_od.eps")

    fig2, ax2 = all_models_plot(data, od_data, with_overdriven=False)
    fig2.savefig(out_dir / "spline_vs_others_hugo.png")
    fig2.savefig(out_dir / "spline_vs_others_hugo.eps")

    fig3, ax3 = all_models_plot(
        data, od_data, with_overdriven=True, with_spline_hugo=False
    )
    ax3.set_title("Model Comparison\nHugoniot and Overdriven Data")
    fig3.set_figwidth(1.2 * fig3.get_figwidth())
    fig3.savefig(out_dir / "other_models_hugo_od.png")
    fig3.savefig(out_dir / "other_models_hugo_od.eps")

    fig4, ax4 = all_models_plot(
        data, od_data, with_overdriven=False, with_spline_hugo=False
    )
    ax4.set_title("Model Comparison\nHugoniot Data")
    fig4.savefig(out_dir / "other_models_hugo.png")
    fig4.savefig(out_dir / "other_models_hugo.eps")

    fig5, axs = residual_plot(data)
    fig5.savefig(out_dir / "residual.png")
    fig5.savefig(out_dir / "residual.eps")
