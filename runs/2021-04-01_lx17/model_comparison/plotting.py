# Copyright (C) 2021 Reid Ginoza
# All rights reserved.
from matplotlib import pyplot as plt
import numpy as np
import pandas as pd

from seos.optimizer.opt_plots import (
    ARA_BLUE,
    ARA_DARK_BLUE,
    ARA_BRIGHT_BLUE,
    ARA_ORANGE,
    ARA_GREEN,
    ARA_GREY,
    ARA_DARK_GREY,
)

COLORS = {
    "dallman": ARA_BRIGHT_BLUE,
    "gustavsen": ARA_DARK_BLUE,
    "holmes": "aquamarine",
    "jackson": ARA_GREEN,
    "reference": "red",
    "result": ARA_ORANGE,
    "tarver": "darkgoldenrod",
}


def add_data_to_ax(
    ax: plt.Axes,
    data: pd.DataFrame,
    od_data: pd.DataFrame,
    with_overdriven: bool = True,
) -> plt.Axes:
    """Plots scatter data on ax in specific volume coordinates."""
    ax.scatter(
        data.loc[data["Label"] == "Jackson Gun", "v_spec"],
        data.loc[data["Label"] == "Jackson Gun", "P"],
        label="Jackson Gun",
        color=ARA_GREEN,
        marker="^",
    )

    ax.scatter(
        data.loc[data["Label"] == "Jackson Wedge", "v_spec"],
        data.loc[data["Label"] == "Jackson Wedge", "P"],
        label="Jackson Wedge",
        color=ARA_GREEN,
        marker="s",
    )

    ax.scatter(
        data.loc[data["Label"] == "Dallman", "v_spec"],
        data.loc[data["Label"] == "Dallman", "P"],
        label="Dallman " r"($\rho_0=1.913$)",
        color=ARA_BRIGHT_BLUE,
    )

    ax.scatter(
        data.loc[data["Label"] == "Gustavsen", "v_spec"],
        data.loc[data["Label"] == "Gustavsen", "P"],
        label="Gustavsen",
        color=ARA_DARK_BLUE,
    )

    if with_overdriven:
        ax.scatter(
            od_data.loc[od_data["Label"] == "Holmes", "v_spec"],
            od_data.loc[od_data["Label"] == "Holmes", "P"],
            label="Holmes (Overdriven)",
            color="aquamarine",
        )

    ax.scatter(
        1 / 1.9, 0, color="red", label="Reference Point " r"($\rho_0 = 1.9$)"
    )
    return ax
