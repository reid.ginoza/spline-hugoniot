# Copyright (C) 2021 Reid Ginoza
# All rights reserved.
"""Post process round 6b.

Messy code ahead!
"""
from datetime import datetime
from functools import partial
from pathlib import Path
import pickle
from typing import Collection, List, Optional, Tuple

from matplotlib import pyplot as plt
import numpy as np
import pandas as pd
from pymoo.algorithms.nsga2 import NSGA2
from pymoo.model.algorithm import Algorithm
from pymoo.model.population import Population
from pymoo.model.result import Result
from scipy.optimize import curve_fit
from scipy.interpolate import InterpolatedUnivariateSpline

from seos.optimizer.opt_plots import sanity_check_plot, plot_spline_against_data
from seos.splines.spline_eos import SplineEOS

from seos.optimizer.opt_plots import (
    ARA_BLUE,
    ARA_DARK_BLUE,
    ARA_BRIGHT_BLUE,
    ARA_ORANGE,
    ARA_GREEN,
    ARA_GREY,
    ARA_DARK_GREY,
)

from main_round_6b_few_nodes import (
    add_data_to_ax,
    get_bounds,
    get_data,
    get_overdriven_data,
    make_base_spline,
)

COLORS = {
    "dallman": ARA_BRIGHT_BLUE,
    "gustavsen": ARA_DARK_BLUE,
    "holmes": "aquamarine",
    "jackson": ARA_GREEN,
    "reference": "red",
    "result": ARA_ORANGE,
    "tarver": "darkgoldenrod",
}


def get_generation(gen: int) -> List[Population]:
    archive_dir = (
        Path(__file__).parent / "round_6b_fewer_fewer_nodes" / "archive"
    )
    start = str(gen * 100 + 1).zfill(4)
    pkl = next(
        p
        for p in archive_dir.iterdir()
        if start in p.name and ".pickle" in p.name
    )
    with pkl.open("rb") as f:
        generation = pickle.load(f)
    return generation


def compare_result_against_data(
    result_spline: SplineEOS,
    data: pd.DataFrame,
    od_data: pd.DataFrame,
    with_overdriven: bool,
):
    fig: plt.Figure
    ax: plt.Axes
    fig, ax = plt.subplots()

    ax = add_data_to_ax(ax, data, od_data, with_overdriven)

    if with_overdriven:
        all_data = pd.concat([data, od_data], ignore_index=True)
    else:
        all_data = data

    v_spec = np.linspace(
        all_data["v_spec"].min() * 0.95, all_data["v_spec"].max() * 1.05, 10000
    )
    v_rel = v_spec * 1.9
    ax.plot(
        v_spec, result_spline(v_rel), label="Spline", color=COLORS["result"]
    )

    ax.legend()
    ax.set_xlabel("Volume (cc/g)")
    ax.set_ylabel("Pressure (GPa)")
    if with_overdriven:
        title = "Hugoniot and Overdriven Data"
    else:
        title = "Hugoniot Data"
    ax.set_title(title)
    return fig, ax


def compare_od_distances(data, od_data, base_spline):
    fig: plt.Figure
    ax: plt.Axes
    fig, ax = plt.subplots()

    ax = add_data_to_ax(ax, data, od_data, True)

    all_data = pd.concat([data, od_data], ignore_index=True)

    v_spec = np.linspace(
        all_data["v_spec"].min() * 0.9, all_data["v_spec"].max() * 1.05, 10000
    )
    v_rel = v_spec * 1.9
    ax.plot(v_spec, base_spline(v_rel), label="Spline", color=COLORS["result"])

    ax.legend()
    ax.set_xlabel("Volume (cc/g)")
    ax.set_ylabel("Pressure (GPa)")
    title = "Hugoniot and Overdriven Data"
    ax.set_title(title)
    return fig, ax


def make_od_constraint_plot():
    base_spline = make_base_spline(data, od_data)
    lower_bounds, upper_bounds = get_bounds(base_spline)
    lower_bound_spline = base_spline.new_spline_from_coeffs(lower_bounds)
    fig, ax = compare_od_distances(
        data, od_data, lambda v: 0.5 * (lower_bound_spline(v) + base_spline(v))
    )
    ax.get_legend().remove()
    return fig, ax


def fake_result():
    result_path = r"C:\Users\rginoza\Documents\CODE\spline-eos\runs\lx17\round_4_many_node_overdriven_get_closer\opt_result.pickle"
    with open(result_path, "rb") as f:
        opt_res = pickle.load(f)
    problem_path = r"C:\Users\rginoza\Documents\CODE\spline-eos\runs\lx17\round_4_many_node_overdriven_get_closer\problem.pickle"
    with open(problem_path, "rb") as f:
        opt_prob = pickle.load(f)

    return opt_prob, opt_res


if __name__ == "__main__":
    data = get_data()
    od_data = get_overdriven_data()

    gen_0 = get_generation(0)
    indv = gen_0[1][0]

    fig, ax = compare_result_against_data(
        make_base_spline(data, od_data).new_spline_from_coeffs(indv.X),
        data,
        od_data,
        with_overdriven=False,
    )
    ax.set_title("Generation 2 Spline\nNot Convex")

    gen_101 = get_generation(1)
    indv = gen_101[4][6689]
    print(f"indv.f {indv.F} indv.G {indv.G}")

    fig, ax = compare_result_against_data(
        make_base_spline(data, od_data).new_spline_from_coeffs(indv.X),
        data,
        od_data,
        with_overdriven=True,
    )
    ax.set_title("Generation 105 Spline")

    # fig, ax = make_od_constraint_plot()

    opt_prob, opt_res = fake_result()
    fake_result_spline = opt_prob._base_spline.new_spline_from_coeffs(opt_res.X)
    fake_fig, fake_ax = compare_result_against_data(
        fake_result_spline, data, od_data, with_overdriven=False,
    )
    fake_ax.set_title(f"Final Result\n RMSE: {round(float(opt_res.F), 4)} GPa")
