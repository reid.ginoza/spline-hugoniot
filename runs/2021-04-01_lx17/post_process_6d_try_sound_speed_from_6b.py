# Copyright (C) 2021 Reid Ginoza
# All rights reserved.
"""Post process round 6b.

Messy code ahead!
"""
from pathlib import Path
import pickle
from typing import List, Optional, Sequence, Tuple, Union

from matplotlib import pyplot as plt
import numpy as np
from scipy.interpolate import interp1d
from scipy.optimize import root_scalar, RootResults

from seos.splines.spline_eos import SplineEOS

from seos.optimizer.opt_plots import (
    ARA_BLUE,
    ARA_DARK_BLUE,
    ARA_BRIGHT_BLUE,
    ARA_ORANGE,
    ARA_GREEN,
    ARA_GREY,
    ARA_DARK_GREY,
)

from main_round_6b_few_nodes import (
    add_data_to_ax,
    get_data,
    get_overdriven_data,
    make_base_spline,
)

Float = Union[float, np.float]


def get_result() -> SplineEOS:
    res_file = (
        Path(__file__).parent
        / "round_6b_fewer_fewer_nodes"
        / "opt_result.pickle"
    )
    with res_file.open("rb") as f:
        res = pickle.load(f)

    data = get_data()
    od_data = get_overdriven_data()

    base_spline = res.problem._base_spline
    # need this to be compatible with new version
    base_spline.gamma_kind = "mg"

    return base_spline.new_spline_from_coeffs(res.X)


def plot_c_sq(v_rels, c_sq, p):
    fig: plt.Figure
    axs: Sequence[plt.Axes]
    fig, axs = plt.subplots(nrows=2, sharex=True)

    axs[0].plot(v_rels, p)
    axs[0].set_ylabel("Pressure (GPa)")

    axs[1].plot(v_rels, c_sq)
    axs[1].hlines(
        y=0, xmin=v_rels.min(), xmax=v_rels.max(), lw=2, colors="black"
    )
    # This seemed useful, but might not be.
    # axs[1].set_ylim(-5.0, 0.5)
    axs[1].set_ylabel("Sound Speed Squared")
    axs[1].set_xlabel(r"Relative volume $\nu$")

    return fig, axs


def plot_sound_speed_for_paper(v_rels, c_sq, p):
    fig: plt.Figure
    axs: Sequence[plt.Axes]
    fig, axs = plt.subplots(nrows=2, sharex=True)

    axs[0].plot(v_rels, p, color=ARA_ORANGE)
    axs[0].set_ylabel("Hugoniot\nPressure (GPa)")
    # trying a new design. delete below if like.
    # axs[0].set_title("Spline Hugoniot")

    axs[1].plot(v_rels, np.sqrt(c_sq), color=ARA_BLUE)
    axs[1].set_ylabel("Sound Speed\n" r"(mm/$\mu$s)")
    axs[1].set_xlabel(r"Relative volume $\nu$")
    return fig, axs


def invalid_c_sq_regions(
    v_rels: Sequence, c_sq
) -> Optional[Tuple[Float, Float]]:
    """Assumes there's only one region in the middle that is invalid"""
    if (c_sq > 0).all():
        return None

    c_sq_line = interp1d(x=v_rels, y=c_sq)

    # find left side
    l_root_res: RootResults = root_scalar(c_sq_line, x0=v_rels[0], x1=v_rels[1])
    if not l_root_res.converged:
        raise RuntimeError("Could not find left root")
    left = l_root_res.root

    # right side
    r_root_res: RootResults = root_scalar(
        c_sq_line, x0=v_rels[-1], x1=v_rels[-2]
    )
    if not r_root_res.converged:
        raise RuntimeError("Could not find right root")
    right = r_root_res.root

    assert left <= right
    return left, right


def make_zoomed_in_plot(
    v_rels: np.ndarray,
    c_sq: np.ndarray,
    p: np.ndarray,
    left: Float,
    right: Float,
):
    fig: plt.Figure
    axs: Sequence[plt.Axes]
    fig, axs = plt.subplots(nrows=2, sharex=True)

    invalid = c_sq < 0
    inv_v_rels = v_rels[invalid]

    inv_p = p[invalid]
    inv_c_sq = c_sq[invalid]

    axs[0].plot(inv_v_rels, inv_p)
    axs[0].set_ylabel("Pressure (GPa)")

    axs[1].plot(inv_v_rels, inv_c_sq)
    axs[1].hlines(
        y=0, xmin=inv_v_rels.min(), xmax=inv_v_rels.max(), lw=2, colors="black"
    )
    axs[1].set_ylabel("Sound Speed Squared")
    axs[1].set_xlabel(r"Relative volume $\nu$")

    return fig, axs


def adjust_ax_for_jap(ax: plt.Axes):
    ax.tick_params(right=True, top=True)
    return ax


if __name__ == "__main__":
    result_spline = get_result()
    v_rels = result_spline.make_test_points()
    c_sq = result_spline.sound_speed_squared_at_v_rel(v_rels)
    p = result_spline(v_rels)
    print(f"All sound speed nonnegative? {(c_sq > 0).all()}")

    fig, axs = plot_c_sq(v_rels, c_sq, p)

    try:
        left, right = invalid_c_sq_regions(v_rels, c_sq)
    except TypeError:
        print("No invalid regions!")
        fig.suptitle("No non-negative sound speeds")
    else:
        fig.suptitle("Sound speed is negative")
        # This is what happened the first time. Hopefully I can delete!
        axs[0].axvspan(xmin=left, xmax=right, alpha=0.5, color="orange")

        # To Manually zoom in on interesting area
        axs[0].set_xlim(0.5008123278142961, 1.003599393274823)
        axs[0].set_ylim(-3.6949316614657164, 122.40464864072794)

        zoom_fig, zoom_axs = make_zoomed_in_plot(v_rels, c_sq, p, left, right)

    fig2, axs2 = plot_sound_speed_for_paper(v_rels, c_sq, p)
    for ax_ in axs2:
        adjust_ax_for_jap(ax_)
    fig2.suptitle("Convex Spline Hugoniot with\nNon-monotonic Sound Speed")
    fig2.tight_layout()
    fig2.savefig(Path(__file__).parent / "non_monotonic_sound_speed.png")
    fig2.savefig(Path(__file__).parent / "non_monotonic_sound_speed.eps")
