# Copyright (C) 2021 Reid Ginoza
# All rights reserved.
"""Post process round 6b.

Messy code ahead!
"""
from datetime import datetime
from functools import partial
from pathlib import Path
import pickle
from typing import Collection, List, Optional, Tuple

from matplotlib import pyplot as plt
import numpy as np
import pandas as pd
from pymoo.algorithms.nsga2 import NSGA2
from pymoo.model.algorithm import Algorithm
from pymoo.model.population import Population
from pymoo.model.result import Result
from scipy.optimize import curve_fit
from scipy.interpolate import InterpolatedUnivariateSpline

from seos.optimizer.opt_plots import sanity_check_plot, plot_spline_against_data
from seos.splines.spline_eos import SplineEOS

from seos.optimizer.opt_plots import (
    ARA_BLUE,
    ARA_DARK_BLUE,
    ARA_BRIGHT_BLUE,
    ARA_ORANGE,
    ARA_GREEN,
    ARA_GREY,
    ARA_DARK_GREY,
)

from main_round_6b_few_nodes import (
    add_data_to_ax,
    get_bounds,
    get_data,
    get_overdriven_data,
    make_base_spline,
)
from post_process_6b import compare_result_against_data

COLORS = {
    "dallman": ARA_BRIGHT_BLUE,
    "gustavsen": ARA_DARK_BLUE,
    "holmes": "aquamarine",
    "jackson": ARA_GREEN,
    "reference": "red",
    "result": ARA_ORANGE,
    "tarver": "darkgoldenrod",
}


def get_most_recent_batch(archive_dir: Path) -> Path:
    pikls = [p for p in archive_dir.iterdir() if ".pickle" in p.name]
    pikls.sort(key=lambda p: int(p.name.split("_")[1]))
    batch = pikls[-1]
    print(batch.name)
    return batch


def get_best_indv_from_pop(pop: Population):
    mininum = np.inf
    best_indv = None
    for indv in pop:
        if indv.F < mininum:
            best_indv = indv
    return best_indv


def get_best_indv_from_many_pops(pops: List[Population]):
    mininum = np.inf
    best_indv = None
    for pop in pops:
        for indv in pop:
            if indv.F < mininum:
                best_indv = indv
    return best_indv


if __name__ == "__main__":
    data = get_data()
    od_data = get_overdriven_data()

    archive_dir = (
        Path(__file__).parent / "round_6b_fewer_fewer_nodes" / "archive"
    )
    pikl = get_most_recent_batch(archive_dir)

    with pikl.open("rb") as f:
        batch = pickle.load(f)

    best_indv = get_best_indv_from_many_pops(batch[-5:])

    result_spline = make_base_spline(data, od_data).new_spline_from_coeffs(
        best_indv.X
    )
    knots_v_rel = result_spline.get_knots()
    knots_v_spec = knots_v_rel / 1.9
    knots_p = result_spline(knots_v_rel)

    od_fig, od_ax = compare_result_against_data(
        result_spline, data, od_data, with_overdriven=True,
    )
    x_lim, y_lim = od_ax.get_xlim(), od_ax.get_ylim()
    od_ax.scatter(
        knots_v_spec,
        knots_p,
        marker="2",
        color=COLORS["result"],
        label="Spline Knots",
    )
    od_ax.set_xlim(x_lim)
    od_ax.set_ylim(y_lim)
    od_ax.legend()
    od_ax.set_title(f"Final Result\nRMSD: {round(float(best_indv.F), 4)} GPa")

    data_fig, data_ax = compare_result_against_data(
        result_spline, data, od_data, with_overdriven=False,
    )
    x_lim, y_lim = data_ax.get_xlim(), data_ax.get_ylim()
    data_ax.scatter(
        knots_v_spec,
        knots_p,
        marker="2",
        color=COLORS["result"],
        label="Spline Knots",
    )
    data_ax.set_xlim(x_lim)
    data_ax.set_ylim(y_lim)
    data_ax.legend()
    data_ax.set_title(f"Final Result\nRMSD: {round(float(best_indv.F), 4)} GPa")
