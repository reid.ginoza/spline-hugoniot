# Copyright (C) 2021 Reid Ginoza
# All rights reserved.
"""Create a plot visualizing the constraint for the overdriven data."""
from functools import partial
from pathlib import Path
import pickle

from matplotlib import pyplot as plt
import numpy as np
import pandas as pd
from scipy.optimize import root_scalar

from seos.splines.spline_eos import SplineEOS

from seos.optimizer.opt_plots import (
    ARA_BLUE,
    ARA_DARK_BLUE,
    ARA_BRIGHT_BLUE,
    ARA_ORANGE,
    ARA_GREEN,
    ARA_GREY,
    ARA_DARK_GREY,
)

# messy import to take advantage of PyCharm's look up with relative
# imports, but there is not a package so relative imports do not work
# in the Python Console

try:
    from .main_lx17_orig import (
        get_data,
        get_overdriven_data,
        make_base_spline,
        get_bounds,
        add_data_to_ax,
        data_plot,
        result_spline_against_data,
    )
except ImportError:
    from main_round_6b_few_nodes import (
        get_data,
        get_overdriven_data,
        make_base_spline,
        get_bounds,
        add_data_to_ax,
        data_plot,
        result_spline_against_data,
    )

try:
    from .curlyBrace import curlyBrace
except ImportError:
    from curlyBrace import curlyBrace


def check_round_4():
    round_4_dir = (
        Path(__file__).parent / "round_4_many_node_overdriven_get_closer"
    )
    round_4_problem = round_4_dir / "problem.pickle"
    round_4_opt_result = round_4_dir / "opt_result.pickle"

    with round_4_problem.open("rb") as f:
        prob = pickle.load(f)

    print("===", "Problem", dir(prob), sep="\n")

    with round_4_opt_result.open("rb") as f:
        opt_result = pickle.load(f)
    print("===", "opt_result", dir(opt_result), sep="\n")
    return prob, opt_result


def get_round_4_base_spline() -> SplineEOS:
    round_4_dir = (
        Path(__file__).parent / "round_4_many_node_overdriven_get_closer"
    )
    round_4_problem = round_4_dir / "problem.pickle"

    with round_4_problem.open("rb") as f:
        prob = pickle.load(f)
    return prob._base_spline


def get_round_4_coeffs():
    round_4_dir = (
        Path(__file__).parent / "round_4_many_node_overdriven_get_closer"
    )
    round_4_opt_result = round_4_dir / "opt_result.pickle"

    with round_4_opt_result.open("rb") as f:
        opt_result = pickle.load(f)
    return opt_result.X


def get_round_r4_spline_for_plot() -> SplineEOS:
    base_spline = get_round_4_base_spline()
    final_coeffs = get_round_4_coeffs()
    return base_spline.new_spline_from_coeffs(final_coeffs)


def spline_match_pressure(p, spline):
    root_sol = root_scalar(find_v_rel_for_p, args=(p, spline), x0=0.6, x1=0.5,)
    if not root_sol.converged:
        # Warnings take too long to print.
        # warn(f"Could not solve for v at p.\n{root_sol}\n"
        #      f"Returning large v distance.")
        raise RuntimeError("Could not find matching volume for pressure")

    v_spec = root_sol.root / spline.rho_0
    return v_spec


def find_v_rel_for_p(v_rel, p, spline):
    return spline(v_rel) - p


def od_to_left_bracket(row: pd.Series, spline):
    """Wrapper for use in pd.Dataframe.apply using a row as the argument"""
    return spline_match_pressure(row["P"], spline)


def make_bracket_points(od_data: pd.DataFrame, spline: SplineEOS):
    p = partial(od_to_left_bracket, spline=spline)
    od_data["bracket_left_v"] = od_data.apply(p, axis="columns")
    return od_data


def add_brackets(fig: plt.Figure, ax: plt.Axes, od_data: pd.DataFrame):
    for idx, row in enumerate(
        od_data.sort_values("v_spec").itertuples(index=False), start=1
    ):
        curlyBrace(
            fig,
            ax,
            (row.bracket_left_v, row.P + 2),
            (row.v_spec, row.P + 2),
            str_text=rf"$v_{idx} - v^*_{idx}$",
            color=ARA_ORANGE,
            fontdict={"color": ARA_ORANGE},
        )


def make_od_constraints_plot():
    data = get_data()
    od_data = get_overdriven_data()

    final_spline = get_round_r4_spline_for_plot()

    fig, ax = result_spline_against_data(final_spline, data, od_data)
    ax.set_title("Visualization of Overdriven Constraints")
    ax.get_legend().remove()
    ax.get_lines()[0].set_color(ARA_DARK_BLUE)

    od_data = make_bracket_points(od_data, final_spline)

    add_brackets(fig, ax, od_data)

    ax.autoscale()
    ax.set_xlim(left=0.22, right=0.41)
    ylim = ax.get_ylim()
    ax.set_ylim(ylim[0], ylim[1] + 7)

    return fig, ax


if __name__ == "__main__":
    fig, ax = make_od_constraints_plot()
