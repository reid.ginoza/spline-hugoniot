# Copyright (C) 2021 Reid Ginoza
# All rights reserved.
"""Needs a docstring."""
from pathlib import Path
from typing import List

import matplotlib

matplotlib.use("TkAgg")
import matplotlib.text
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

from seos.splines.default_spline import few_nodes_spline

ARA_BLUE = "#002E6C"
ARA_DARK_BLUE = "#00204B"
ARA_BRIGHT_BLUE = "#0092CA"
ARA_ORANGE = "#F05532"
ARA_GREEN = "#D2DD3F"
ARA_GREY = "#DEDEDE"
ARA_DARK_GREY = "#666666"

PROB_2_DATA_FILE = Path(__file__).parent / "prob_2_data.csv"


def get_data():
    data = pd.read_csv(Path(__file__).parent / "lx17_jwl_ur.csv")
    return data.append({"V(--)": 1.0, "P(GPA)": 0.0}, ignore_index=True)


def get_prob_2_data():
    data = pd.read_csv(PROB_2_DATA_FILE)
    return data.append({"V(--)": 1.0, "P(GPA)": 0.0}, ignore_index=True)


def plot_data(data_v, data_P):
    fig: plt.Figure
    ax: plt.Axes
    fig, ax = plt.subplots()
    ax.scatter(data_v, data_P, label="Data", color=ARA_BLUE)
    ax.set_title("Artificial Data")
    ax.set_xlabel(r"Relative Volume $\nu$")
    ax.set_ylabel(r"Pressure $P$ (GPa)")
    return fig, ax


def plot_Us_up_data(data_Us, data_up):
    fig: plt.Figure
    ax: plt.Axes
    fig, ax = plt.subplots()
    ax.scatter(data_up, data_Us, label="Data", color=ARA_BLUE)
    ax.set_title("Artificial Data")
    ax.set_xlabel(r"Particle Velocity $u_p$ (mm/$\mu$s)")
    ax.set_ylabel(r"Shock Velocity $U_s$ (mm/$\mu$s)")
    return fig, ax


def p_v_to_us_up(p, v_rel, rho_0):
    up = np.sqrt(p * (1 - v_rel) / rho_0)
    us = up / (1 - v_rel)
    return us, up


def fix_prob_2():
    """ "Used one time only because the original data only added
    noise to the pressure, and did not adjust the US or UP."""
    data = pd.read_csv(PROB_2_DATA_FILE)
    data["US(KM/S)"], data["UP(KM/S)"] = p_v_to_us_up(
        data["P(GPA)"], data["V(--)"], 1.9
    )
    data.to_csv(PROB_2_DATA_FILE, index=False)


def plot_prob_2_data_against_truth():
    data = get_prob_2_data()
    fig, ax = plot_data(data["V(--)"], data["P(GPA)"])

    spline = few_nodes_spline()
    v_min, v_max = data["V(--)"].min() * 0.99, data["V(--)"].max()
    v_plot = np.linspace(v_min, v_max)
    ax.plot(v_plot, spline(v_plot), color=ARA_BLUE)
    ax.set_title("Artificial Experimental Data\nwith Known Truth")
    return fig, ax


def _double_tick_labels_size(tick_labels: List[matplotlib.text.Text]):
    """Assumes it changes in place."""
    for tick_label in tick_labels:
        tick_label.set_fontsize(2 * tick_label.get_fontsize())

    # Old code that does not assume it changes in place
    # new_tick_labels = []
    # for tick_label in tick_labels:
    #     tick_label.set_fontsize(2 * tick_label.get_fontsize())
    #     new_tick_labels.append(tick_label)
    # return new_tick_labels


def adjust_ax_for_jap(ax: plt.Axes, units:str) -> plt.Axes:
    # determined by guess and visually check
    units_limits = {
        "p_v": (
            (0.59, 1.01),  # xlim
            (-5, 105),  # ylim
        ),
        "us_up": (
            (-0.1, 4.6),  # xlim
            (1.8, 12),  # ylim
        )
    }
    assert units in units_limits

    # add tick marks to all axes
    ax.tick_params(right=True, top=True)

    # double the size since it will be scaled down in the paper
    ax.xaxis.label.set_fontsize(2 * ax.xaxis.label.get_fontsize())
    ax.yaxis.label.set_fontsize(2 * ax.yaxis.label.get_fontsize())

    # special check for Us up plot
    if units == "us_up":
        y_ticks = [2, 4, 6, 8, 10, 12]
        ax.set_yticks(y_ticks)
        ax.set_yticklabels([str(t) for t in y_ticks])

    # Same doubling of the tick labels
    ax.set_xticks(ax.get_xticks())
    _double_tick_labels_size(ax.get_xticklabels())

    ax.set_yticks(ax.get_yticks())
    _double_tick_labels_size(ax.get_yticklabels())

    # same doubling of the title
    ax.title.set_fontsize(2 * ax.title.get_fontsize())

    ax.set_xlim(units_limits[units][0])
    ax.set_ylim(units_limits[units][1])

    # make everything fit
    fig:plt.Figure = ax.get_figure()
    fig.set_figwidth(1.35 * fig.get_figwidth())
    fig.set_figheight(1.35 * fig.get_figheight())
    return ax


def adjust_ax_for_jap_no_size_change(ax: plt.Axes) -> plt.Axes:
    # add tick marks to all axes
    ax.tick_params(right=True, top=True)
    return ax


if __name__ == "__main__":
    out_dir = Path(__file__).parent / "pp_out"

    data = get_data()

    fig, ax = plot_data(data["V(--)"], data["P(GPA)"])
    ax = adjust_ax_for_jap(ax, "p_v")
    fig.savefig(out_dir / "prob_1_data_only.png")
    fig.savefig(out_dir / "prob_1_data_only.eps")

    fig, ax = plot_Us_up_data(data["US(KM/S)"], data["UP(KM/S)"])
    ax = adjust_ax_for_jap(ax, "us_up")
    fig.savefig(out_dir / "prob_1_data_only_us_up.eps")
    fig.savefig(out_dir / "prob_1_data_only_us_up.png")

    prob_2_data = get_prob_2_data()
    fig, ax = plot_data(prob_2_data["V(--)"], prob_2_data["P(GPA)"])
    ax.set_title("Artificial Data with Noise and Gaps")
    ax = adjust_ax_for_jap(ax, "p_v")
    fig.savefig(out_dir / "prob_2_data_only.png")
    fig.savefig(out_dir / "prob_2_data_only.eps")

    fig, ax = plot_Us_up_data(prob_2_data["US(KM/S)"], prob_2_data["UP(KM/S)"])
    ax.set_title("Artificial Data with Noise and Gaps")
    ax = adjust_ax_for_jap(ax, "us_up")
    fig.savefig(out_dir / "prob_2_data_only_us_up.png")
    fig.savefig(out_dir / "prob_2_data_only_us_up.eps")

    # this plot is not in a shared figure in the LaTeX,
    # so it does not get resized
    fig, ax = plot_prob_2_data_against_truth()
    ax = adjust_ax_for_jap_no_size_change(ax)
    fig.savefig(out_dir / "prob_2_known_truth.png")
    fig.savefig(out_dir / "prob_2_known_truth.eps")
