# Copyright (C) 2021 Reid Ginoza
# All rights reserved.
"""Post-processing for problem 2."""
from pathlib import Path
import pickle
from typing import Dict, List

import matplotlib

matplotlib.use("TkAgg")
import matplotlib.pyplot as plt
import matplotlib.ticker as mtick
import numpy as np
import pandas as pd

from pymoo.model.population import Population

from pp_prob_1_opt_progress import (
    collect_gens_fitnesses,
    plot_fitnesses,
adjust_ax_for_jap
)

pd.set_option("display.precision", 16)


if __name__ == "__main__":
    out_dir = Path(__file__).parent / "pp_out_prob_2"
    out_dir.mkdir(exist_ok=True)

    results_dir = Path(__file__).parent / "results" / "2021-06-13_prob_2"
    archive_dir = results_dir / "archive"

    data: pd.DataFrame = collect_gens_fitnesses(archive_dir)

    fig, ax = plot_fitnesses(data, first_feasible=24, all_feasible=55)
    adjust_ax_for_jap(ax)
    fig.savefig(out_dir / "prob_2_fit_by_gen.png")
    fig.savefig(out_dir / "prob_2_fit_by_gen.eps")

    fig2, ax2 = plot_fitnesses(data, start_gen=450)
    adjust_ax_for_jap(ax2)
    #ax2.yaxis.set_major_formatter(mtick.FormatStrFormatter("%.1e"))
    jap_axis_format = mtick.ScalarFormatter(useMathText=True, useOffset=False)
    jap_axis_format.set_scientific(False)
    jap_axis_format.set_powerlimits((-2, 3))
    ax2.yaxis.set_major_formatter(jap_axis_format)
    fig2.set_figwidth(7.3)
    fig2.savefig(out_dir / "prob_2_fit_by_gen_450.png")
    fig2.savefig(out_dir / "prob_2_fit_by_gen_450.eps")
