"""Verification study for the SplineEOS and OptimizeSpline classes.

Uses data from a known truth, but there is missing data
and added noise.
"""
import argparse
from datetime import date, datetime
import itertools
import multiprocessing
import os
from pathlib import Path
import pickle
from typing import Optional, Tuple

import matplotlib

matplotlib.use("Agg")
from matplotlib import pyplot as plt
import numpy as np
import pandas as pd
from pymoo.algorithms.nsga2 import NSGA2
from pymoo.optimize import minimize
from pymoo.util.termination.default import SingleObjectiveDefaultTermination

from seos.splines.spline_eos import SplineEOS
from seos.optimizer.opt_plots import plot_spline_against_data
from seos.optimizer.spline_opt import (
    SavePopulation,
    SEOSDisplay,
    default_low_bound,
    default_upper_bound,
)
from seos.optimizer.spline_opt_fixed_ref import OptimizeSplineFixedRef
from seos.utilities.save_stdout import PrintToFile

ARA_BLUE = "#002E6C"
ARA_DARK_BLUE = "#00204B"
ARA_BRIGHT_BLUE = "#0092CA"
ARA_ORANGE = "#F05532"
ARA_GREEN = "#D2DD3F"
ARA_GREY = "#DEDEDE"
ARA_DARK_GREY = "#666666"


def initial_spline() -> SplineEOS:
    hugo = pd.read_csv(Path(__file__).parent / "lx17_jwl_ur.csv")
    # Include the point (1, 0) which is theoretically required
    #   as the reference point/initial value
    hugo = hugo.append({"V(--)": 1, "P(GPA)": 0,}, ignore_index=True,)

    spline = SplineEOS(
        hugo["V(--)"],
        hugo["P(GPA)"],
        rho_0=1.9,
        p0=3.3027e-04,
        e0=6.7890e-02,
        t0=298.15,
        cv=1.3052623990693266,
        gamma=8.94000000e-01,
        gamma_kind="constant",
    )

    # contract for reference point on spline
    # included as a debugging tool since earlier runs had a mistake
    # where the reference point was not in the data set
    tol = 1e-12
    assert spline(1) - 0.0 < tol
    return spline


def get_data():
    """Data that already has noise added"""
    hugo = pd.read_csv(Path(__file__).parent / "prob_2_data.csv")
    # Include the point (1, 0) which is theoretically required
    #   as the reference point/initial value
    hugo = hugo.append({"V(--)": 1, "P(GPA)": 0,}, ignore_index=True,)
    return hugo


def plot_against_known_truth(initial_spline, result_spline, data_v, data_P):
    """Assumes initial_spline is the known truth."""
    fig: plt.Figure
    ax: plt.Axes
    fig, ax = plt.subplots()

    v = np.linspace(min(data_v) - 0.005, max(data_v) + 0.05, 10000)

    _err = result_spline(v) - initial_spline(v)
    _sq = _err ** 2
    _mean = _sq.mean()
    rmse = np.sqrt(_mean)

    ax.plot(v, initial_spline(v), color=ARA_BLUE, label="Known Truth")
    ax.scatter(data_v, data_P, color=ARA_BLUE, label="Data")
    ax.plot(v, result_spline(v), color=ARA_ORANGE, alpha=0.7, label="Result")
    ax.legend()
    ax.set_xlabel(r"Relative Volume $\nu$")
    ax.set_ylabel(r"Pressure $P$ (GPa)")
    ax.set_title(
        f"GA Spline Fit Against Known Truth\nRMSE: {round(rmse, 4)} GPa"
    )
    return fig, ax


def make_out_dir(run_name: Optional[str] = None) -> Path:
    date_stamp = date.today().isoformat()
    file_name = (
        f"{date_stamp}_{run_name}"
        if run_name is not None
        else f"{date_stamp}_spline_fit"
    )
    out_dir = Path(__file__).parent / "results" / file_name
    if not out_dir.exists():
        out_dir.mkdir(exist_ok=True, parents=True)
        return out_dir

    c = itertools.count()
    while out_dir.exists():
        suffix = str(next(c)).zfill(2)
        out_dir = out_dir.parent / f"{file_name}_{suffix}"

    out_dir.mkdir()
    return out_dir


def make_bounds(base_spline: SplineEOS) -> Tuple[np.ndarray, np.ndarray]:
    k = base_spline.get_private_data("k")
    n_data = len(base_spline.get_private_data("x"))
    expected_length = n_data - 1

    coeffs = base_spline.get_private_data("c")[: -(k + 1) - 1]
    assert len(coeffs) == expected_length

    xl = default_low_bound(coeffs)
    xu = default_upper_bound(coeffs)

    assert len(xl) == len(xu) == expected_length

    return xl, xu


def make_parser():
    parser = argparse.ArgumentParser(
        description="Run the nice data with no gaps model fitting."
    )
    parser.add_argument(
        "-np",
        "--num-processors",
        type=_parse_positive_ints_only,
        default=1,
        help="Number of processors to use in the optimization. If not "
        "specified, defaults to 1. Otherwise uses Python's "
        "multiprocessing library.\n"
        "On my computer, 8 seems to be a good number.",
    )
    return parser


def _parse_positive_ints_only(arg: str):
    arg = int(arg)
    if arg < 1:
        raise argparse.ArgumentTypeError(
            f"Invalid num-processors value: " f"{arg}. Must be positive int."
        )
    elif arg > os.cpu_count():
        raise argparse.ArgumentTypeError(
            f"Invalid num-processors value: "
            f"{arg}. Must not be more than "
            f"os.cpu_count(): {os.cpu_count()}."
        )
    return arg


def get_num_processors():
    parser = make_parser()
    args = parser.parse_args()
    return args.num_processors


if __name__ == "__main__":
    num_processors = get_num_processors()
    use_multiprocessing = num_processors > 1

    # Prep directory
    run_name = "prob_2"
    out_dir = make_out_dir(run_name)

    with PrintToFile(out_dir / f"{run_name}.log"):
        # Prep problem
        rho_0 = 1.9
        base_spline = initial_spline()
        xl, xu = make_bounds(base_spline)
        data = get_data()
        v_spec = data["V(--)"] / rho_0
        data_rho = np.full_like(data["V(--)"], rho_0)

        # Establish pymoo variables
        if use_multiprocessing:
            print(f"Using multiprocessing: Pool({num_processors})")
            pool = multiprocessing.Pool(num_processors)
            kwargs = {"parallelization": ("starmap", pool.starmap)}
        else:
            kwargs = {}

        problem = OptimizeSplineFixedRef(
            base_spline, v_spec, data["P(GPA)"], data_rho, xl, xu, **kwargs
        )

        algorithm = NSGA2(pop_size=10000)
        termination = SingleObjectiveDefaultTermination(
            x_tol=1e-8,
            cv_tol=1e-6,
            f_tol=1e-6,
            nth_gen=5,
            n_last=20,
            n_max_gen=100000,
            n_max_evals=None,
        )

        # Optimize
        tic = datetime.now()
        print(f"Starting: {tic}")
        res = minimize(
            problem,
            algorithm,
            termination,
            callback=SavePopulation(out_dir, batch_size=5),
            display=SEOSDisplay(),
            verbose=True,
            seed=1,
        )
        toc = datetime.now()
        print(toc - tic)
        if use_multiprocessing:
            pool.close()
        print(f"res.X:\n{res.X}")
        print(f"res.F:\n{res.F}")
        print(f"res.G:\n{res.G}")

        # post process
        with (out_dir / f"{run_name}_opt_problem.pickle").open("wb") as f:
            pickle.dump(problem, f)

        with (out_dir / f"{run_name}_opt_result.pickle").open("wb") as f:
            pickle.dump(res, f)

        result_coeffs = problem.make_full_coeffs(res.X)
        result_spline = base_spline.new_spline_from_coeffs(result_coeffs)

        res_sp_fig, res_sp_ax = plot_spline_against_data(
            result_spline, data["V(--)"], data["P(GPA)"]
        )
        res_sp_fig.savefig(out_dir / "result_spline_plot.eps")
        res_sp_fig.savefig(out_dir / "result_spline_plot.png")

        cmd = r"""res_sp_ax.set_title("GA Best Fit 575th Generation\nRMSD: 7.703e-5 GPa")"""
        print(f"Change title using (or whatever is appropriate): {cmd}")

        truth_fig, truth_ax = plot_against_known_truth(
            initial_spline(), result_spline, data["V(--)"], data["P(GPA)"]
        )
        truth_fig.savefig(out_dir / "result_against_truth.eps")
        truth_fig.savefig(out_dir / "result_against_truth.png")

        with (out_dir / f"{run_name}_result_spline.pickle").open("wb") as f:
            pickle.dump(result_spline, f)
