# Copyright (C) 2021 Reid Ginoza
# All rights reserved.
"""Needs a docstring."""
from pathlib import Path
import pickle
from typing import Dict, List, Optional

import matplotlib

matplotlib.use("TkAgg")
import matplotlib.pyplot as plt
import matplotlib.ticker as mtick
import numpy as np
import pandas as pd

from pymoo.model.population import Population
from pymoo.model.individual import Individual

from seos.optimizer.spline_opt_fixed_ref import OptimizeSplineFixedRef
from seos.splines.spline_eos import SplineEOS

pd.set_option("display.precision", 16)

ARA_BLUE = "#002E6C"
ARA_DARK_BLUE = "#00204B"
ARA_BRIGHT_BLUE = "#0092CA"
ARA_ORANGE = "#F05532"
ARA_GREEN = "#D2DD3F"
ARA_GREY = "#DEDEDE"
ARA_DARK_GREY = "#666666"


def get_data() -> pd.DataFrame:
    data = pd.read_csv(Path(__file__).parent / "lx17_jwl_ur.csv")
    return data.append({"V(--)": 1.0, "P(GPA)": 0.0}, ignore_index=True)


def collect_fitness_by_generation(archive_dir: Path) -> Dict[int, List[float]]:
    fitnesses = {}
    for gens_p in sorted(
        p for p in archive_dir.iterdir() if p.suffix == ".pickle"
    ):
        _, gen_start, gen_end = gens_p.stem.split("_")
        gen_start = int(gen_start)
        gen_end = int(gen_end)

        with gens_p.open("rb") as f:
            gens: List[Population] = pickle.load(f)

        for gen_idx, gen in enumerate(gens, start=gen_start):
            fitnesses[gen_idx] = [float(i.F) for i in gen]
    return fitnesses


def collect_gens_fitnesses_old(archive_dir: Path):
    gens_for_plot = []
    fits_for_plot = []
    gens_for_means = []
    means_for_plot = []
    for gens_p in sorted(
        p for p in archive_dir.iterdir() if p.suffix == ".pickle"
    ):
        _, gen_start, gen_end = gens_p.stem.split("_")
        gen_start = int(gen_start)
        gen_end = int(gen_end)

        with gens_p.open("rb") as f:
            gens: List[Population] = pickle.load(f)

        for gen_idx, gen in enumerate(gens, start=gen_start):
            fits = [float(i.F) for i in gen]
            fits_for_plot.extend(fits)
            gens_for_plot.extend([gen_idx] * len(gen))
            gens_for_means.append(gen_idx)
            means_for_plot.append(sum(fits) / len(fits))
    return (
        np.array(gens_for_plot),
        np.array(fits_for_plot),
        np.array(gens_for_means),
        np.array(means_for_plot),
    )


def collect_gens_fitnesses(archive_dir: Path):
    data = {"Generation": [], "Fitness": []}
    for gens_p in sorted(
        p for p in archive_dir.iterdir() if p.suffix == ".pickle"
    ):
        _, gen_start, _ = gens_p.stem.split("_")
        gen_start = int(gen_start)

        with gens_p.open("rb") as f:
            gens: List[Population] = pickle.load(f)

        for gen_idx, gen in enumerate(gens, start=gen_start):
            data["Fitness"].extend([float(i.F) for i in gen])
            data["Generation"].extend([gen_idx] * len(gen))

    return pd.DataFrame(data)


def plot_fitnesses_old(
    gens: np.array,
    fits: np.array,
    gen_means: np.array,
    means: np.array,
    start_gen: int = 0,
):
    """didn't work because overleaf has a size limit"""
    gens_to_use = gens >= start_gen
    gens_trunc = gens[gens_to_use]
    fits_trunc = fits[gens_to_use]

    means_to_use = gen_means >= start_gen
    gen_means_trunc = gen_means[means_to_use]
    means_trunc = means[means_to_use]

    fig: plt.Figure
    ax: plt.Axes
    fig, ax = plt.subplots()

    ax.scatter(gens_trunc, fits_trunc, color=ARA_BLUE)
    ax.plot(gen_means_trunc, means_trunc, color=ARA_ORANGE)
    ax.set_xlabel("Generation")
    ax.set_ylabel("Objective Function: RMSD (GPa)")
    return fig, ax


def plot_fitnesses_heat_map_not_visually_effective(
    gens: np.array,
    fits: np.array,
    gen_means: np.array,
    means: np.array,
    start_gen: int = 0,
):
    gens_to_use = gens >= start_gen
    gens_trunc = gens[gens_to_use]
    fits_trunc = fits[gens_to_use]

    means_to_use = gen_means >= start_gen
    gen_means_trunc = gen_means[means_to_use]
    means_trunc = means[means_to_use]

    fig: plt.Figure
    ax: plt.Axes
    fig, ax = plt.subplots()

    # probably try either a box plot per generation or just
    # min, max, median/mean
    # ax = sns.heatmap(np.vstack([gens_trunc, fits_trunc]).T, ax=ax)
    ax.hexbin(
        gens_trunc, fits_trunc, gridsize=len(gen_means_trunc), cmap="binary"
    )
    # ax.plot(gen_means_trunc, means_trunc, color=ARA_ORANGE)
    ax.set_xlabel("Generation")
    ax.set_ylabel("Objective Function: RMSD (GPa)")
    return fig, ax


def plot_fitnesses(
    data: pd.DataFrame,
    start_gen: int = 1,
    first_feasible: Optional[int] = None,
    all_feasible: Optional[int] = None,
):
    if first_feasible is not None and start_gen > first_feasible:
        first_feasible = None

    if all_feasible is not None and start_gen > all_feasible:
        all_feasible = None

    data_trunc = data[data.Generation >= start_gen]
    by_gen = data_trunc.groupby("Generation")

    fig: plt.Figure
    ax: plt.Axes
    fig, ax = plt.subplots()

    if first_feasible is not None:
        ax.axvline(
            first_feasible,
            color="grey",
            ls="dotted",
            label="First Feasible Spline Found",
        )

    if all_feasible is not None:
        ax.axvline(
            all_feasible,
            color="grey",
            ls="dashed",
            label="All Individuals are Feasible",
        )

    # probably try either a box plot per generation or just
    # min, max, median/mean
    medians = by_gen.quantile(0.5)
    gens_plot = medians.index.to_numpy()
    ax.fill_between(
        gens_plot,
        by_gen.quantile(0.10).to_numpy().flatten(),
        by_gen.quantile(0.90).to_numpy().flatten(),
        color=ARA_GREY,
        label="10th, 90th Percentiles",
    )
    ax.fill_between(
        gens_plot,
        by_gen.quantile(0.25).to_numpy().flatten(),
        by_gen.quantile(0.75).to_numpy().flatten(),
        color="#a8a8a8",
        label="25th, 75th Percentiles",
    )
    ax.plot(by_gen.max(), color=ARA_BRIGHT_BLUE, label="Max")
    ax.plot(by_gen.median(), color=ARA_BLUE, label="Median")
    ax.plot(by_gen.min(), color=ARA_ORANGE, label="Min")

    ax.set_xlabel("Generation")
    ax.set_ylabel("Objective Function: RMSD (GPa)")
    title = "Evolution Plot"
    if start_gen > 1:
        title += f" from Generation {start_gen}"
    ax.set_title(title)
    ax.legend()
    return fig, ax


def adjust_ax_for_jap(ax: plt.Axes):
    ax.tick_params(right=True, top=True)
    return ax


if __name__ == "__main__":
    out_dir = Path(__file__).parent / "pp_out"
    out_dir.mkdir(exist_ok=True)

    results_dir = Path(__file__).parent / "results" / "2021-06-13_prob_1"
    archive_dir = results_dir / "archive"

    data: pd.DataFrame = collect_gens_fitnesses(archive_dir)

    fig, ax = plot_fitnesses(data, first_feasible=24, all_feasible=52)
    adjust_ax_for_jap(ax)
    fig.savefig(out_dir / "prob_1_fit_by_gen.png")
    fig.savefig(out_dir / "prob_1_fit_by_gen.eps")

    fig2, ax2 = plot_fitnesses(
        data, first_feasible=24, all_feasible=52, start_gen=200
    )
    adjust_ax_for_jap(ax2)
    # ax2.yaxis.set_major_formatter(mtick.FormatStrFormatter("%.1e"))
    jap_axis_format = mtick.ScalarFormatter(useMathText=True, useOffset=False)
    jap_axis_format.set_scientific(False)
    jap_axis_format.set_powerlimits((-2, 3))
    ax2.yaxis.set_major_formatter(jap_axis_format)
    fig2.set_figwidth(7.0)
    fig2.savefig(out_dir / "prob_1_fit_by_gen_200.png")
    fig2.savefig(out_dir / "prob_1_fit_by_gen_200.eps")
