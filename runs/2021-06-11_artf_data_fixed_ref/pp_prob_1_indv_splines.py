# Copyright (C) 2021 Reid Ginoza
# All rights reserved.
"""Needs a docstring."""
from dataclasses import dataclass
from pathlib import Path
import pickle
from typing import List, Optional, Union

import matplotlib

matplotlib.use("TkAgg")
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

from pymoo.model.population import Population
from pymoo.model.individual import Individual

from seos.optimizer.spline_opt_fixed_ref import OptimizeSplineFixedRef
from seos.splines.spline_eos import SplineEOS

ARA_BLUE = "#002E6C"
ARA_DARK_BLUE = "#00204B"
ARA_BRIGHT_BLUE = "#0092CA"
ARA_ORANGE = "#F05532"
ARA_GREEN = "#D2DD3F"
ARA_GREY = "#DEDEDE"
ARA_DARK_GREY = "#666666"


def get_opt_problem(results_dir: Path):
    with open(results_dir / "prob_1_opt_problem.pickle", "rb") as f:
        opt_prob: OptimizeSplineFixedRef = pickle.load(f)

    return opt_prob


@dataclass
class IndvWhileSearching:
    indv: Individual
    gen: int
    indv_idx: int

    def file_name(self):
        mul_5 = self.gen % 5 == 0
        correction = -4 if mul_5 else +1
        start_gen = self.gen // 5 * 5 + correction
        end_gen = start_gen + 4
        return f"gen_{str(start_gen).zfill(4)}_{str(end_gen).zfill(4)}.pickle"


def convex_violation(indv: Optional[Union[Individual, IndvWhileSearching]]):
    if indv is None:
        return 0

    elif isinstance(indv, IndvWhileSearching):
        return indv.indv.G[2]

    else:
        return indv.G[2]


def find_non_convex_early_gens(archive_dir: Path):
    with (archive_dir / "gen_0001_0005.pickle").open("rb") as f:
        gen_1_to_5: List[Population] = pickle.load(f)

    worst_non_convex = None

    for g_idx, gen in enumerate(gen_1_to_5, start=1):
        for i_idx, indv in enumerate(gen):
            if convex_violation(indv) > convex_violation(worst_non_convex):
                worst_non_convex = IndvWhileSearching(indv, g_idx, i_idx)

    return worst_non_convex


def find_late_non_convex(archive_dir: Path, file_idx_to_search=7):
    worst_non_convex = None

    for gens_pickle in get_gens_pickles(archive_dir)[
        file_idx_to_search : file_idx_to_search + 1
    ]:
        with gens_pickle.open("rb") as f:
            gens: List[Population] = pickle.load(f)

        start = int(gens_pickle.stem.split("_")[1])

        for g_idx, gen in enumerate(gens, start=start):
            for i_idx, indv in enumerate(gen):
                print(f"checking {g_idx}.{i_idx}: {indv.G}")
                print(
                    f"==> worst_non_convex: {worst_non_convex} {convex_violation(worst_non_convex)}"
                )
                if convex_violation(indv) > 0:
                    worst_non_convex = IndvWhileSearching(indv, g_idx, i_idx)

    return worst_non_convex


def get_early_spline(
    archive_dir: Path,
    opt_problem: OptimizeSplineFixedRef,
    gen: int,
    indv_idx: int,
    file_name: str = "gen_0001_0005.pickle",
) -> SplineEOS:
    """Lots of hard-coded names specifically to make this one plot."""
    with (archive_dir / file_name).open("rb") as f:
        gens: List[Population] = pickle.load(f)

    indv: Individual = gens[gen][indv_idx]

    coeffs: np.ndarray = opt_problem.make_full_coeffs(indv.X)

    return opt_problem.base_spline.new_spline_from_coeffs(coeffs)


def get_late_spline(
    late_non_convex: IndvWhileSearching, opt_problem: OptimizeSplineFixedRef,
):
    coeffs: np.ndarray = opt_problem.make_full_coeffs(late_non_convex.indv.X)
    return opt_problem.base_spline.new_spline_from_coeffs(coeffs)


def get_gens_pickles(archive_dir: Path):
    return sorted(p for p in archive_dir.iterdir() if p.suffix == ".pickle")


# ==========================
# couldn't find any of these
@dataclass
class ShapeOkSoundSpeedNot:
    indv: Individual
    gen: int
    indv_idx: int


def shape_ok_sound_speed_violation(
    indv: Optional[Union[Individual, ShapeOkSoundSpeedNot]]
):
    if indv is None:
        return 0.0

    elif isinstance(indv, IndvWhileSearching):
        i = indv.indv
    else:
        i = indv

    if np.any(i.G[:3]):
        return 0.0
    return indv.G[3:].max()


def find_late_sound_speed_bad(archive_dir: Path):
    sound_speed_bad = None

    for gens_pickle in get_gens_pickles(archive_dir):
        with gens_pickle.open("rb") as f:
            gens: List[Population] = pickle.load(f)

        for g_idx, gen in enumerate(gens, start=1):
            for i_idx, indv in enumerate(gen):
                if shape_ok_sound_speed_violation(
                    indv
                ) > shape_ok_sound_speed_violation(sound_speed_bad):
                    sound_speed_bad = ShapeOkSoundSpeedNot(indv, g_idx, i_idx)

    return sound_speed_bad


def get_late_sound_speed_bad_spline(
    archive_dir: Path,
    file_to_search: str,
    opt_problem: OptimizeSplineFixedRef,
    gen: int,
    indv_idx: int,
) -> SplineEOS:
    """Lots of hard-coded names specifically to make this one plot."""
    with (archive_dir / file_to_search).open("rb") as f:
        gen_51_to_55: List[Population] = pickle.load(f)

    indv: Individual = gen_51_to_55[gen][indv_idx]

    coeffs: np.ndarray = opt_problem.make_full_coeffs(indv.X)

    return opt_problem.base_spline.new_spline_from_coeffs(coeffs)


def plot_late_spline_against_result(
    data: pd.DataFrame,
    late_spline: SplineEOS,
    gen: int,
    description: str = "Sound Speed Constraints Not Met",
):
    fig: plt.Figure
    ax: plt.Axes
    fig, ax = plt.subplots()

    ax.scatter(data["V(--)"], data["P(GPA)"], label="Data", color=ARA_BLUE)
    v_plot = np.linspace(data["V(--)"].min(), 1.0)
    ax.plot(
        v_plot, late_spline(v_plot), label="Result Spline", color=ARA_ORANGE
    )

    ax.set_xlabel("Relative volume " r"$\nu$")
    ax.set_ylabel("Pressure " r"$P$ (GPa)")
    ax.set_title(f"Spline in Generation {gen}\n{description}")
    ax.legend()
    return fig, ax


#
# ==========================


def get_data() -> pd.DataFrame:
    data = pd.read_csv(Path(__file__).parent / "lx17_jwl_ur.csv")
    return data.append({"V(--)": 1.0, "P(GPA)": 0.0}, ignore_index=True)


def plot_early_spline_against_result(
    data: pd.DataFrame, early_spline: SplineEOS
):
    fig: plt.Figure
    ax: plt.Axes
    fig, ax = plt.subplots()

    ax.scatter(data["V(--)"], data["P(GPA)"], label="Data", color=ARA_BLUE)
    v_plot = np.linspace(data["V(--)"].min(), 1.0)
    ax.plot(
        v_plot, early_spline(v_plot), label="Result Spline", color=ARA_ORANGE
    )

    ax.set_xlabel("Relative volume " r"$\nu$")
    ax.set_ylabel("Pressure " r"$P$ (GPa)")
    ax.set_title("Spline in Initial Population\nNoticeably Non-convex")
    ax.legend()
    return fig, ax


def is_feasible(indv) -> bool:
    if indv is None:
        return False

    i = indv.indv if isinstance(indv, IndvWhileSearching) else indv
    return np.all(i.G < 0)


def first_feasible(archive_dir: Path):
    with (archive_dir / "gen_0021_0025.pickle").open("rb") as f:
        gens: List[Population] = pickle.load(f)

    for g_idx, gen in enumerate(gens, start=21):
        for i_idx, indv in enumerate(gen):
            if is_feasible(indv):
                return IndvWhileSearching(indv, g_idx, i_idx)
    else:
        raise RuntimeError("Could not find a feasible spline")


def final_result(results_dir: Path, data: pd.DataFrame):
    with (results_dir / "prob_1_result_spline.pickle").open("rb") as f:
        result_spline = pickle.load(f)

    with (results_dir / "prob_1_opt_result.pickle").open("rb") as f:
        opt_result = pickle.load(f)

    fig: plt.Figure
    ax: plt.Axes
    fig, ax = plt.subplots()

    v = np.linspace(data["V(--)"].min() - 0.005, 1.0, 10000)

    ax.scatter(data["V(--)"], data["P(GPA)"], color=ARA_BLUE, label="Data")
    ax.plot(v, result_spline(v), color=ARA_ORANGE, label="Result")
    ax.legend()
    ax.set_xlabel(r"Relative Volume $\nu$")
    ax.set_ylabel(r"Pressure $P$ (GPa)")
    ax.set_title(
        f"Optimization Result Generation 315\nRMSD: {float(opt_result.F):.3e} GPa"
    )
    return fig, ax


def get_initial_spline() -> SplineEOS:
    hugo = pd.read_csv(Path(__file__).parent / "lx17_jwl_ur.csv")
    # Include the point (1, 0) which is theoretically required
    #   as the reference point/initial value
    hugo = hugo.append({"V(--)": 1, "P(GPA)": 0,}, ignore_index=True,)

    spline = SplineEOS(
        hugo["V(--)"],
        hugo["P(GPA)"],
        rho_0=1.9,
        p0=3.3027e-04,
        e0=6.7890e-02,
        t0=298.15,
        cv=1.3052623990693266,
        gamma=8.94000000e-01,
        gamma_kind="constant",
    )

    # contract for reference point on spline
    # included as a debugging tool since earlier runs had a mistake
    # where the reference point was not in the data set
    tol = 1e-12
    assert spline(1) - 0.0 < tol
    return spline


def redo_plot_against_known_truth(
    results_dir: Path,
    data: pd.DataFrame,
    result_spline_pickle="prob_1_result_spline.pickle",
):
    """Assumes initial_spline is the known truth.

    Redo-ing it only because the right end point was bigger than
    1.0, when really, the Hugoniot should stop at 1.0.
    """
    initial_spline = get_initial_spline()
    with (results_dir / result_spline_pickle).open("rb") as f:
        result_spline = pickle.load(f)

    data_v, data_P = data["V(--)"], data["P(GPA)"]

    fig: plt.Figure
    ax: plt.Axes
    fig, ax = plt.subplots()

    v = np.linspace(min(data_v) - 0.005, 1.0, 10000)

    _err = result_spline(v) - initial_spline(v)
    _sq = _err ** 2
    _mean = _sq.mean()
    rmse = np.sqrt(_mean)

    ax.plot(v, initial_spline(v), color=ARA_BLUE, label="Known Truth")
    ax.scatter(data_v, data_P, color=ARA_BLUE, label="Data")
    ax.plot(v, result_spline(v), color=ARA_ORANGE, alpha=0.7, label="Result")
    ax.legend()
    ax.set_xlabel(r"Relative Volume $\nu$")
    ax.set_ylabel(r"Pressure $P$ (GPa)")
    ax.set_title(
        f"GA Spline Fit Against Known Truth\nRMSE: {round(rmse, 4)} GPa"
    )
    return fig, ax


def adjust_ax_for_jap(ax: plt.Axes):
    ax.tick_params(right=True, top=True)
    return ax


if __name__ == "__main__":
    out_dir = Path(__file__).parent / "pp_out"
    out_dir.mkdir(exist_ok=True)

    results_dir = Path(__file__).parent / "results" / "2021-06-13_prob_1"
    archive_dir = results_dir / "archive"

    data = get_data()

    opt_problem = get_opt_problem(results_dir)

    # Find Worst Early Gen
    # --------------------

    worst = find_non_convex_early_gens(archive_dir)
    early_spline = get_early_spline(
        archive_dir, opt_problem, gen=worst.gen, indv_idx=worst.indv_idx
    )
    fig, ax = plot_early_spline_against_result(data, early_spline)
    adjust_ax_for_jap(ax)
    fig.savefig(out_dir / "prob_1_gen_1_not_convex.png")
    fig.savefig(out_dir / "prob_1_gen_1_not_convex.eps")

    # Find Latest Non-Convex
    # ----------------------

    late_non_convex = find_late_non_convex(archive_dir)
    gen = late_non_convex.gen % 5 - 1
    late_spline = get_late_spline(late_non_convex, opt_problem)
    fig2, ax2 = plot_late_spline_against_result(
        data,
        late_spline,
        late_non_convex.gen,
        description="Still Non-Convex between "
        r"$\nu \in \left(0.77, 0.78 \right)$",
    )
    adjust_ax_for_jap(ax2)
    fig2.savefig(out_dir / "prob_1_gen_40_not_convex.png")
    fig2.savefig(out_dir / "prob_1_gen_40_not_convex.eps")
    test_v = np.linspace(0.6, 1.0, 10000)
    d2p_dv2 = late_spline(test_v, nu=2)
    non_convex = d2p_dv2 < 0
    non_convex_v = test_v[non_convex]
    print(f"non_convex_v: {non_convex_v}")

    # Find first feasible
    # -------------------
    feasible_indv: IndvWhileSearching = first_feasible(archive_dir)
    feasible_spline = get_late_spline(feasible_indv, opt_problem)
    fig3, ax3 = plot_late_spline_against_result(
        data,
        feasible_spline,
        feasible_indv.gen,
        description=f"First Feasible Spline. RMSD: {round(float(feasible_indv.indv.F), 3)}",
    )
    adjust_ax_for_jap(ax3)
    fig3.savefig(out_dir / f"prob_1_gen_{feasible_indv.gen}_first_feasible.png")
    fig3.savefig(out_dir / f"prob_1_gen_{feasible_indv.gen}_first_feasible.eps")

    # Final Result Spline
    # -------------------
    fig4, ax4 = final_result(results_dir, data)
    adjust_ax_for_jap(ax4)
    fig4.savefig(out_dir / f"prob_1_final_rmsd_gen_315.png")
    fig4.savefig(out_dir / f"prob_1_final_rmsd_gen_315.eps")

    # Final against Truth
    # -------------------
    fig5, ax5 = redo_plot_against_known_truth(results_dir, data)
    adjust_ax_for_jap(ax5)
    fig5.savefig(out_dir / "prob_1_final_against_truth.png")
    fig5.savefig(out_dir / "prob_1_final_against_truth.eps")

    # Couldn't find any of these
    # ==========================
    # late_bad = find_late_sound_speed_bad(archive_dir)
    # start_gen = late_bad.gen // 5 * 5 + 1
    # end_gen = start_gen + 4
    # file_to_search = f"gen_{str(start_gen).zfill(4)}_{str(end_gen).zfill(4)}.pickle"
    # print(f"opening {file_to_search}")
    # late_spline = get_late_sound_speed_bad_spline(
    #     archive_dir,
    #     file_to_search,
    #     opt_problem,
    #     late_bad.gen,
    #     late_bad.indv_idx,
    # )
    # fig2, ax2 = plot_late_spline_against_result(data, late_spline, late_bad.gen)
