# Copyright (C) 2021 Reid Ginoza
# All rights reserved.
"""Plots Individual splines during optimization progress.

Specifically for problem 2.
"""
from dataclasses import dataclass
from pathlib import Path
import pickle
from typing import List, Optional, Union

import matplotlib

matplotlib.use("TkAgg")
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

from pymoo.model.population import Population
from pymoo.model.individual import Individual

from seos.optimizer.spline_opt_fixed_ref import OptimizeSplineFixedRef
from seos.splines.spline_eos import SplineEOS

from pp_prob_1_indv_splines import (
    find_non_convex_early_gens,
    get_early_spline,
    plot_early_spline_against_result,
    find_late_non_convex,
    get_late_spline,
    plot_late_spline_against_result,
    IndvWhileSearching,
    first_feasible,
    redo_plot_against_known_truth,
    find_late_sound_speed_bad,
    get_late_sound_speed_bad_spline,
)

ARA_BLUE = "#002E6C"
ARA_DARK_BLUE = "#00204B"
ARA_BRIGHT_BLUE = "#0092CA"
ARA_ORANGE = "#F05532"
ARA_GREEN = "#D2DD3F"
ARA_GREY = "#DEDEDE"
ARA_DARK_GREY = "#666666"


def get_data():
    data = pd.read_csv(Path(__file__).parent / "prob_2_data.csv")
    return data.append({"V(--)": 1.0, "P(GPA)": 0.0}, ignore_index=True)


def get_opt_problem(results_dir: Path):
    with open(results_dir / "prob_2_opt_problem.pickle", "rb") as f:
        opt_prob: OptimizeSplineFixedRef = pickle.load(f)

    return opt_prob


def final_result(results_dir: Path, data: pd.DataFrame):
    """has a lot of hard-coded stuff so couldn't just import from prob_1."""
    with (results_dir / "prob_2_result_spline.pickle").open("rb") as f:
        result_spline = pickle.load(f)

    with (results_dir / "prob_2_opt_result.pickle").open("rb") as f:
        opt_result = pickle.load(f)

    fig: plt.Figure
    ax: plt.Axes
    fig, ax = plt.subplots()

    v = np.linspace(data["V(--)"].min() - 0.005, 1.0, 10000)

    ax.scatter(data["V(--)"], data["P(GPA)"], color=ARA_BLUE, label="Data")
    ax.plot(v, result_spline(v), color=ARA_ORANGE, label="Result")
    ax.legend()
    ax.set_xlabel(r"Relative Volume $\nu$")
    ax.set_ylabel(r"Pressure $P$ (GPa)")
    ax.set_title(
        f"Optimization Result Generation 645\nRMSD: {float(opt_result.F):.3} GPa"
    )
    return fig, ax


def adjust_ax_for_jap(ax: plt.Axes):
    ax.tick_params(right=True, top=True)
    return ax


if __name__ == "__main__":
    out_dir = Path(__file__).parent / "pp_out_prob_2"
    out_dir.mkdir(exist_ok=True)

    results_dir = Path(__file__).parent / "results" / "2021-06-13_prob_2"
    archive_dir = results_dir / "archive"

    data = get_data()

    opt_problem = get_opt_problem(results_dir)

    # Find Worst Early Gen
    # --------------------

    worst = find_non_convex_early_gens(archive_dir)
    early_spline = get_early_spline(
        archive_dir, opt_problem, gen=worst.gen, indv_idx=worst.indv_idx
    )
    fig, ax = plot_early_spline_against_result(data, early_spline)
    adjust_ax_for_jap(ax)
    fig.savefig(out_dir / "prob_2_gen_1_not_convex.png")
    fig.savefig(out_dir / "prob_2_gen_1_not_convex.eps")

    # Find Later Non-Convex
    # ----------------------

    late_non_convex = find_late_non_convex(archive_dir, file_idx_to_search=7)
    gen = late_non_convex.gen % 5 - 1
    late_spline = get_late_spline(late_non_convex, opt_problem)
    fig2, ax2 = plot_late_spline_against_result(
        data,
        late_spline,
        late_non_convex.gen,
        description="Still Non-Convex around " r"$\nu =0.74$ and $\nu = 0.80$",
    )
    adjust_ax_for_jap(ax2)
    fig2.savefig(out_dir / "prob_2_gen_40_not_convex.png")
    fig2.savefig(out_dir / "prob_2_gen_40_not_convex.eps")
    test_v = np.linspace(0.6, 1.0, 10000)
    d2p_dv2 = late_spline(test_v, nu=2)
    non_convex = d2p_dv2 < 0
    non_convex_v = test_v[non_convex]
    print(f"non_convex_v: {non_convex_v}")

    # Find first feasible
    # -------------------
    feasible_indv: IndvWhileSearching = first_feasible(archive_dir)
    feasible_spline = get_late_spline(feasible_indv, opt_problem)
    fig3, ax3 = plot_late_spline_against_result(
        data,
        feasible_spline,
        feasible_indv.gen,
        description=f"First Feasible Spline. RMSD: {round(float(feasible_indv.indv.F), 3)}",
    )
    adjust_ax_for_jap(ax3)
    fig3.savefig(out_dir / f"prob_2_gen_{feasible_indv.gen}_first_feasible.png")
    fig3.savefig(out_dir / f"prob_2_gen_{feasible_indv.gen}_first_feasible.eps")

    # Final Result Spline
    # -------------------
    fig4, ax4 = final_result(results_dir, data)
    adjust_ax_for_jap(ax4)
    fig4.savefig(out_dir / f"prob_2_final_rmsd_gen_645.png")
    fig4.savefig(out_dir / f"prob_2_final_rmsd_gen_645.eps")

    # Final against Truth
    # -------------------
    fig5, ax5 = redo_plot_against_known_truth(
        results_dir, data, result_spline_pickle="prob_2_result_spline.pickle"
    )
    adjust_ax_for_jap(ax5)
    fig5.savefig(out_dir / "prob_2_final_against_truth.png")
    fig5.savefig(out_dir / "prob_2_final_against_truth.eps")

    # Couldn't find any of these
    # ==========================
    # late_bad = find_late_sound_speed_bad(archive_dir)
    # start_gen = late_bad.gen // 5 * 5 + 1
    # end_gen = start_gen + 4
    # file_to_search = f"gen_{str(start_gen).zfill(4)}_{str(end_gen).zfill(4)}.pickle"
    # print(f"opening {file_to_search}")
    # late_spline = get_late_sound_speed_bad_spline(
    #     archive_dir,
    #     file_to_search,
    #     opt_problem,
    #     late_bad.gen,
    #     late_bad.indv_idx,
    # )
    # fig2, ax2 = plot_late_spline_against_result(data, late_spline, late_bad.gen)
