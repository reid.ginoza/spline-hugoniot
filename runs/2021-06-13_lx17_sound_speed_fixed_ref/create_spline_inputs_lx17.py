# Copyright (C) 2021 Reid Ginoza
# All rights reserved.
"""Creates the required knots and bounds for LX-17 model fit."""
from functools import partial
from pathlib import Path

import matplotlib

try:
    matplotlib.use("Agg")
except RecursionError:
    pass
from matplotlib import pyplot as plt
import numpy as np
import pandas as pd
from scipy.optimize import curve_fit

from seos.optimizer.opt_plots import (
    ARA_BLUE,
    ARA_DARK_BLUE,
    ARA_BRIGHT_BLUE,
    ARA_ORANGE,
    ARA_GREEN,
    ARA_GREY,
    ARA_DARK_GREY,
)
from seos.optimizer.spline_opt_fixed_ref import make_v_data_for_base_spline
from seos.splines.spline_eos import SplineEOS


def simp_jwl(v_rel, a, r1, b, r2, c, rho_0):
    density = rho_0 / v_rel
    return a * np.exp(-r1 * v_rel) + b * np.exp(-r2 * v_rel) + c * density


def make_base_spline(data, od_data, rho_0, n_pts=20):
    p0 = 0.0
    e0 = 0.0
    t0 = 298.15
    cv = 1.3052623990693266
    gamma = 1
    gamma_kind = "constant"

    all_data = pd.concat([data, od_data], ignore_index=True).sort_values(
        by="v_spec"
    )
    v_rel = all_data.v_spec * rho_0
    p = all_data.P

    partial_simp_jwl = partial(simp_jwl, rho_0=rho_0)

    popt, pcov = curve_fit(
        partial_simp_jwl, v_rel, p, maxfev=12000, ftol=1.0e-03, xtol=1.0e-03
    )

    jwl_orig = partial(
        simp_jwl,
        a=popt[0],
        r1=popt[1],
        b=popt[2],
        r2=popt[3],
        c=popt[4],
        rho_0=rho_0,
    )

    v = make_v_data_for_base_spline(
        data.v_spec, od_data.v_spec, rho_0, n_pts=n_pts
    )

    # set reference to (v=1., P=0.)
    p_new = jwl_orig(v) - jwl_orig(1.0)

    return SplineEOS(
        v,
        p_new,
        rho_0=rho_0,
        p0=p0,
        e0=e0,
        t0=t0,
        cv=cv,
        gamma=gamma,
        gamma_kind=gamma_kind,
    )


def get_bounds(base_spline: SplineEOS):
    """This is specific to the LX-17 data set."""
    total_coeffs = len(base_spline.get_private_data("c"))
    right_end_coeffs = base_spline.get_private_data("k") + 1
    n_changing_coeffs = (
        len(base_spline.get_private_data("c")) - 1 - right_end_coeffs
    )
    changing_coeffs = base_spline.get_private_data("c")[
        :n_changing_coeffs
    ].copy()
    print(
        f"Knots ({len(base_spline.get_private_data('t'))}): {base_spline.get_private_data('t')}\n"
        f"Coeffs ({total_coeffs}): {base_spline.get_private_data('c')}\n"
        f"Degree: {base_spline.get_private_data('k')}\n"
        f"Right hand coeffs: {right_end_coeffs}\n"
        f"Changing coeffs: {n_changing_coeffs}\n"
        f"Coeffs to change: {changing_coeffs}"
    )

    xl = changing_coeffs * 0.45
    xu = changing_coeffs * 1.2

    print(f"xl: {xl}\nxu: {xu}")

    assert np.all(xl < xu)
    return xl, xu


def bounds_to_full_coeffs(bounds: np.ndarray, base_spline):
    return np.concatenate(
        [bounds, [0.0] * (base_spline.get_private_data("k") + 2)]
    )


if __name__ == "__main__":
    from main_lx17 import (
        get_data,
        get_overdriven_data,
        add_data_to_ax,
        data_plot,
        data_plot_with_bounds,
        opt_inputs_bounds_knots,
        result_spline_against_data,
        RHO_0,
    )

    print("Creating base_spline for LX-17 data set.")
    out_dir = Path(__file__).parent / "create_spline_plots"
    out_dir.mkdir(exist_ok=True)

    # get data
    data = get_data()
    od_data = get_overdriven_data()

    # Plot data only
    d_fig, d_ax = data_plot(data, od_data)
    d_fig.savefig(out_dir / "data_only.png")
    z_d_fig, z_d_ax = data_plot(data, od_data, with_overdriven=False)
    z_d_fig.savefig(out_dir / "data_onlyZoom.png")

    # make base spline
    base_spline = make_base_spline(data, od_data, RHO_0)

    # plot base spline
    full_res_fig, full_res_ax = result_spline_against_data(
        base_spline, data, od_data
    )
    full_res_fig.savefig(out_dir / "base_spline_data.png")
    z_full_res_fig, z_full_res_ax = result_spline_against_data(
        base_spline, data, od_data, with_overdriven=False,
    )
    z_full_res_fig.savefig(out_dir / "base_spline_dataZoom.png")

    # make data bounds

    lower_bounds, upper_bounds = get_bounds(base_spline)

    lower_bounds_for_spline = bounds_to_full_coeffs(lower_bounds, base_spline)
    upper_bounds_for_spline = bounds_to_full_coeffs(upper_bounds, base_spline)

    # Data bounds
    data_bounds_fig, data_bounds_ax = data_plot_with_bounds(
        base_spline,
        lower_bounds_for_spline,
        upper_bounds_for_spline,
        data,
        od_data,
    )
    data_bounds_fig.savefig(out_dir / "bounds.png")
