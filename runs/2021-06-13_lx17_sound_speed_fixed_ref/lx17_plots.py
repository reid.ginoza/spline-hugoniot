# Copyright (C) 2021 Reid Ginoza
# All rights reserved.
"""Plots specifically for LX-17 data.

Code is currently split in two locations.
Most is in the main_lx17.py module for now.
"""
from typing import Optional, Union

import matplotlib

try:
    matplotlib.use("Agg")
except RecursionError:
    pass
from matplotlib import pyplot as plt
import numpy as np
import pandas as pd

from seos.splines.spline_eos import SplineEOS
from seos.optimizer.opt_plots import (
    ARA_BLUE,
    ARA_DARK_BLUE,
    ARA_BRIGHT_BLUE,
    ARA_ORANGE,
    ARA_GREEN,
    ARA_GREY,
    ARA_DARK_GREY,
)

Float = Union[float, np.float]

# messy, need to just have this in one place.
RHO_0 = 1.9


# extremely messy. need to have this in just one place!
def add_data_to_ax(
    ax: plt.Axes,
    data: pd.DataFrame,
    od_data: pd.DataFrame,
    with_overdriven: bool = True,
) -> plt.Axes:
    """Plots scatter data on ax in specific volume coordinates."""
    ax.scatter(
        data.loc[data["Label"] == "Jackson Gun", "v_spec"],
        data.loc[data["Label"] == "Jackson Gun", "P"],
        label="Jackson Gun",
        color=ARA_GREEN,
        marker="^",
    )

    ax.scatter(
        data.loc[data["Label"] == "Jackson Wedge", "v_spec"],
        data.loc[data["Label"] == "Jackson Wedge", "P"],
        label="Jackson Wedge",
        color=ARA_GREEN,
        marker="s",
    )

    ax.scatter(
        data.loc[data["Label"] == "Dallman", "v_spec"],
        data.loc[data["Label"] == "Dallman", "P"],
        label="Dallman " r"($\rho_0=1.913$)",
        color=ARA_BRIGHT_BLUE,
    )

    ax.scatter(
        data.loc[data["Label"] == "Gustavsen", "v_spec"],
        data.loc[data["Label"] == "Gustavsen", "P"],
        label="Gustavsen",
        color=ARA_DARK_BLUE,
    )

    if with_overdriven:
        ax.scatter(
            od_data.loc[od_data["Label"] == "Holmes", "v_spec"],
            od_data.loc[od_data["Label"] == "Holmes", "P"],
            label="Holmes (Overdriven)",
            color="aquamarine",
        )

    ax.scatter(
        1 / RHO_0, 0, color="red", label="Reference Point " r"($\rho_0 = 1.9$)"
    )
    return ax


def plot_spline_with_sound_speed(
    spline: SplineEOS,
    data: pd.DataFrame,
    od_data: pd.DataFrame,
    rho_0: Float = 1.9,
    with_overdriven: bool = True,
):
    """Messy code!!"""
    if with_overdriven:
        all_data = pd.concat([data, od_data], ignore_index=True)
    else:
        all_data = data
    v_spec = np.linspace(
        all_data["v_spec"].min() * 0.999, all_data["v_spec"].max() * 1.05, 10000
    )
    v_rel = v_spec * rho_0

    fig: plt.Figure
    ax0: plt.Axes
    ax1: plt.Axes
    fig, (ax0, ax1) = plt.subplots(nrows=2, sharex="all")

    ####
    # Top Plot: Hugoniot in P-v
    ####
    ax0 = _add_spline_and_knots(
        ax0, spline, data, od_data, v_rel, with_overdriven
    )

    ####
    # Bot. Plot: Sound speed squared
    ####
    ax1.plot(
        v_spec,
        np.sqrt(spline.sound_speed_squared_at_v_rel(v_rel)),
        color=ARA_BLUE,
    )
    ax1.set_xlabel("Volume (cc/g)")
    ax1.set_ylabel(r"Sound Speed (mm/$\mu$s)")
    return fig, (ax0, ax1)


def _add_spline_and_knots(
    ax: plt.Axes, spline, data, od_data, v_rel, with_overdriven
):

    fig, ax = result_spline_against_data(
        spline, data, od_data, ax=ax, with_overdriven=with_overdriven
    )

    knots_v_rel = spline.get_private_data("t")
    if not with_overdriven:
        knots_v_rel = knots_v_rel[knots_v_rel > v_rel.min()]
    knots_p = spline(knots_v_rel)
    knots_v_spec = knots_v_rel / spline.rho_0

    ax.scatter(knots_v_spec, knots_p, marker="+", color="brown")
    ax.get_legend().remove()
    return ax


def result_spline_against_data(
    result_spline: SplineEOS,
    data: pd.DataFrame,
    od_data: pd.DataFrame,
    rmsd: Optional[float] = None,
    ax: Optional[plt.Axes] = None,
    with_overdriven: bool = True,
):
    """Specific to the LX17 data set"""
    if ax is None:
        fig: plt.Figure
        ax: plt.Axes
        fig, ax = plt.subplots()
    else:
        fig = ax.figure

    ax = add_data_to_ax(ax, data, od_data, with_overdriven)

    if with_overdriven:
        all_data = pd.concat([data, od_data], ignore_index=True)
    else:
        all_data = data
    v_spec = np.linspace(
        all_data["v_spec"].min() * 0.95, all_data["v_spec"].max() * 1.05, 10000
    )
    v_rel = v_spec * result_spline.rho_0
    spline_p = result_spline(v_rel)
    ax.plot(v_spec, spline_p, color=ARA_ORANGE, label="Result Spline")

    ax.legend()
    ax.set_xlabel("Volume (cc/g)")
    ax.set_ylabel("Pressure (GPa)")

    title = "Result Spline with Data"
    if not with_overdriven:
        title += " (zoomed)"
    if rmsd is not None:
        title += f"\nRMSD: {rmsd} GPa"
    ax.set_title(title)
    return fig, ax
