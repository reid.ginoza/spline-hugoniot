"""Test case for the SplineEOS and OptimizeSpline classes.

Uses a known truth with all available data to see if the algorithm
can optimize to the known truth.
"""
from datetime import date, datetime
import itertools
import multiprocessing
from pathlib import Path
import pickle

from matplotlib import pyplot as plt
import numpy as np
import pandas as pd
from pymoo.algorithms.nsga2 import NSGA2
from pymoo.optimize import minimize
from pymoo.util.termination.default import SingleObjectiveDefaultTermination

from seos.splines.spline_eos import SplineEOS
from seos.optimizer.opt_plots import plot_spline_against_data
from seos.optimizer.spline_opt import OptimizeSpline, SavePopulation

ARA_BLUE = "#002E6C"
ARA_DARK_BLUE = "#00204B"
ARA_BRIGHT_BLUE = "#0092CA"
ARA_ORANGE = "#F05532"
ARA_GREEN = "#D2DD3F"
ARA_GREY = "#DEDEDE"
ARA_DARK_GREY = "#666666"


def initial_spline() -> SplineEOS:
    hugo = pd.read_csv(Path(__file__).parent / "lx17_jwl_ur.csv")
    # Include the point (1, 0) which is theoretically required
    #   as the reference point/initial value
    hugo = hugo.append({"V(--)": 1, "P(GPA)": 0,}, ignore_index=True,)
    return SplineEOS(
        hugo["V(--)"],
        hugo["P(GPA)"],
        rho_0=1.9,
        p0=3.3027e-04,
        e0=6.7890e-02,
        t0=298.15,
        cv=1.3052623990693266,
        gamma=8.94000000e-01,
        gamma_kind="constant",
    )


def get_data():
    hugo = pd.read_csv(Path(__file__).parent / "lx17_jwl_ur.csv")
    # Include the point (1, 0) which is theoretically required
    #   as the reference point/initial value
    hugo.append(
        {"V(--)": 1, "P(GPA)": 0,}, ignore_index=True,
    )
    return hugo


def plot_against_known_truth(initial_spline, result_spline, data_v, data_P):
    fig: plt.Figure
    ax: plt.Axes
    fig, ax = plt.subplots()

    v = np.linspace(min(data_v) - 0.005, max(data_v) + 0.05, 10000)

    _err = result_spline(v) - initial_spline(v)
    _sq = _err ** 2
    _mean = _sq.mean()
    rmse = np.sqrt(_mean)

    ax.plot(v, initial_spline(v), color=ARA_BLUE, label="Known Truth")
    ax.scatter(data_v, data_P, color=ARA_BLUE, label="Data")
    ax.plot(v, result_spline(v), color=ARA_ORANGE, alpha=0.7, label="Result")
    ax.legend()
    ax.set_xlabel(r"Relative Volume $\nu$")
    ax.set_ylabel(r"Pressure $P$ (GPa)")
    ax.set_title(
        f"GA Spline Fit Against Known Truth\nRMSE: {round(rmse, 4)} GPa"
    )
    return fig, ax


def make_out_dir() -> Path:
    date_stamp = date.today().isoformat()
    file_name = f"{date_stamp}_prob_1"
    out_dir = Path(__file__).parent / "results" / file_name
    if not out_dir.exists():
        out_dir.mkdir(exist_ok=True, parents=True)
        return out_dir

    c = itertools.count()
    while out_dir.exists():
        suffix = str(next(c)).zfill(2)
        out_dir = out_dir.parent / f"{file_name}_{suffix}"

    out_dir.mkdir()
    return out_dir


if __name__ == "__main__":
    use_multiprocessing = False

    # Prep directory
    out_dir = make_out_dir()

    # Prep problem
    rho_0 = 1.9
    base_spline = initial_spline()
    data = get_data()
    v_spec = data["V(--)"] / rho_0
    data_rho = np.full_like(data["V(--)"], rho_0)

    # Establish pymoo variables
    if use_multiprocessing:
        pool = multiprocessing.Pool(8)
        kwargs = {"parallelization": ("starmap", pool.starmap)}
    else:
        kwargs = {}

    problem = OptimizeSpline(
        base_spline, v_spec, data["P(GPA)"], data_rho, **kwargs
    )

    algorithm = NSGA2(pop_size=10000)
    termination = SingleObjectiveDefaultTermination(
        x_tol=1e-8,
        cv_tol=1e-6,
        f_tol=1e-6,
        nth_gen=5,
        n_last=20,
        n_max_gen=100000,
        n_max_evals=None,
    )

    # Optimize
    tic = datetime.now()
    print(f"Starting: {tic}")
    res = minimize(
        problem,
        algorithm,
        termination,
        callback=SavePopulation(out_dir, batch_size=100),
        verbose=True,
        seed=1,
    )
    toc = datetime.now()
    print(toc - tic)
    if use_multiprocessing:
        pool.close()
    print(f"res.X:\n{res.X}")
    print(f"res.F:\n{res.F}")

    # post process
    result_spline = base_spline.new_spline_from_coeffs(res.X)

    res_sp_fig, res_sp_ax = plot_spline_against_data(
        result_spline, data["V(--)"], data["P(GPA)"]
    )
    res_sp_fig.savefig(out_dir / "result_spline_plot.eps")

    cmd = r"""res_sp_ax.set_title("GA Best Fit 575th Generation\nRMSD: 7.703e-5 GPa")"""
    print(f"Change title using: {cmd}")

    truth_fig, truth_ax = plot_against_known_truth(
        initial_spline(), result_spline, data["V(--)"], data["P(GPA)"]
    )
    truth_fig.savefig(out_dir / "prob_1_result_against_truth.eps")

    with (out_dir / "prob_1_opt_problem.pickle").open("wb") as f:
        pickle.dump(problem, f)

    with (out_dir / "prob_1_opt_result.pickle").open("wb") as f:
        pickle.dump(res, f)

    with (out_dir / "prob_1_result_spline.pickle").open("wb") as f:
        pickle.dump(result_spline, f)
