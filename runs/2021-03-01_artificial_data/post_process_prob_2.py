# Copyright (C) 2021 Reid Ginoza
# All rights reserved.
"""Mostly creating plots."""
from pathlib import Path
import pickle
from typing import List, Tuple

from matplotlib import pyplot as plt
import numpy as np
import pandas as pd

# typing support
from pymoo.model.individual import Individual
from pymoo.model.population import Population
from seos.optimizer.spline_opt import OptimizeSpline
from seos.splines.spline_eos import SplineEOS

results_dir = Path(__file__).parent / "results" / "2021-04-20_prob_2_00"


def get_base_spline():
    with (results_dir / "prob_2_opt_problem.pickle").open("rb") as f:
        problem: OptimizeSpline = pickle.load(f)
    base_spline: SplineEOS = problem._base_spline
    return base_spline


def get_early_gens() -> Tuple[Population, Population]:
    with (results_dir / "archive" / "gen_0001_0100.pickle").open("rb") as f:
        early_gens: List[Population] = pickle.load(f)

    first_gen: Population = early_gens[0]
    tenth_gen: Population = early_gens[9]
    return first_gen, tenth_gen


def plot_from_initial_population(first_gen: Population, base_spline: SplineEOS):
    indv: Individual = first_gen[10]
    indv_spline: SplineEOS = base_spline.new_spline_from_coeffs(indv.X)


if __name__ == "__main__":
    base_spline = get_base_spline()
    first_gen, tenth_gen = get_early_gens()
