"""Verification study for the SplineEOS and OptimizeSpline classes.

Uses data from a known truth, but there is missing data
and added noise.
"""
######################################
# Original Results from old repo
######################################
#   Time:  5:49:42.892907
#   Generations: 715
#   Number of Evals: 7150000
#
#   Coefficients
#        Results      Truth      Diff
# 0   100.359261  96.860000  3.499261
# 1    89.939947  90.859423 -0.919477
# 2    83.152898  82.526285  0.626613
# 3    74.004012  72.555294  1.448718
# 4    67.760302  65.237700  2.522602
# 5    61.390736  58.212562  3.178174
# 6    54.792035  51.570668  3.221366
# 7    48.455362  45.275533  3.179829
# 8    41.869414  39.304267  2.565148
# 9    35.133331  33.725495  1.407835
# 10   28.514362  28.478343  0.036019
# 11   21.259670  23.621855 -2.362185
# 12   17.225359  19.139287 -1.913929
# 13   13.634005  15.050111 -1.416106
# 14   11.454418  11.346431  0.107987
# 15    8.878797   8.071634  0.807163
# 16    5.734905   5.213550  0.521355
# 17    1.401741   1.274310  0.127431
# 18    0.533177   0.555944 -0.022766
# 19    0.005758   0.005234  0.000523

# Round 2
# == Problem Description ==
# Spline 19 Knots: [0.60932702 0.6237721  0.6316313  0.63999194 0.64888616 0.65844048
#  0.66872609 0.679823   0.69192213 0.7051638  0.71981863 0.73620343
#  0.75475436 0.77606225 0.80116074 0.83151462 0.86990274 0.92167013
#  1.        ]
# Spline Number of Coeffs: 21
# Coeffs lower bound: [87.174      81.77348115 74.27365664 65.29976477 58.71392981 52.39130577
#  46.41360143 40.74797951 35.37383991 30.3529459  25.63050879 21.25966944
#  17.22535908 13.5450969  10.21180153  7.26440363  4.69253467  2.53650971
#   0.8069245   0.2600494  -0.1       ]
# Coeffs upper bound: [1.06546000e+02 9.99453658e+01 9.07789137e+01 7.98108236e+01
#  7.17614698e+01 6.40338182e+01 5.67277351e+01 4.98030861e+01
#  4.32346932e+01 3.70980450e+01 3.13261774e+01 2.59840404e+01
#  2.10532166e+01 1.65551184e+01 1.24810908e+01 8.87871555e+00
#  5.73532016e+00 3.10017854e+00 9.86241057e-01 3.17838156e-01
#  1.00000000e-01]
#
# Result
# Time: 1:56:37.463143
#
# array([1.00358255e+02, 9.13986668e+01, 8.25164759e+01, 7.38948715e+01,
#        6.77126350e+01, 6.13997637e+01, 5.48003983e+01, 4.79766656e+01,
#        4.18209767e+01, 3.54762777e+01, 2.86701079e+01, 2.12596825e+01,
#        1.72253605e+01, 1.36310502e+01, 1.14527769e+01, 8.87871524e+00,
#        5.73531881e+00, 3.10017582e+00, 9.86240577e-01, 3.17831068e-01,
#        6.27253181e-07])
#
# Orig
# array([96.86      , 90.85942349, 82.52628515, 72.55529419, 65.23769979,
#        58.21256197, 51.57066825, 45.27553279, 39.30426657, 33.72549545,
#        28.4783431 , 23.62185493, 19.13928786, 15.05010767, 11.34644615,
#         8.07155959,  5.21392742,  2.81834413,  0.89658278,  0.28894378,
#         0.        ])
# Diff
# array([ 3.49825534e+00,  5.39243269e-01, -9.80923635e-03,  1.33957729e+00,
#         2.47493517e+00,  3.18720170e+00,  3.22973006e+00,  2.70113277e+00,
#         2.51671013e+00,  1.75078221e+00,  1.91764779e-01, -2.36217243e+00,
#        -1.91392732e+00, -1.41905750e+00,  1.06330712e-01,  8.07155652e-01,
#         5.21391400e-01,  2.81831693e-01,  8.96577981e-02,  2.88872896e-02,
#         6.27253181e-07])
from datetime import date, datetime
import itertools
from pathlib import Path
import pickle
import time
from typing import Optional

from matplotlib import pyplot as plt
import numpy as np
import pandas as pd
from pymoo.algorithms.nsga2 import NSGA2
from pymoo.optimize import minimize
from pymoo.util.termination.default import SingleObjectiveDefaultTermination

from seos.splines.spline_eos import SplineEOS
from seos.optimizer.opt_plots import plot_spline_against_data
from seos.optimizer.spline_opt import OptimizeSpline, SavePopulation

ARA_BLUE = "#002E6C"
ARA_DARK_BLUE = "#00204B"
ARA_BRIGHT_BLUE = "#0092CA"
ARA_ORANGE = "#F05532"
ARA_GREEN = "#D2DD3F"
ARA_GREY = "#DEDEDE"
ARA_DARK_GREY = "#666666"


def initial_spline() -> SplineEOS:
    hugo = pd.read_csv(Path(__file__).parent / "lx17_jwl_ur.csv")
    # Include the point (1, 0) which is theoretically required
    #   as the reference point/initial value
    hugo = hugo.append({"V(--)": 1, "P(GPA)": 0,}, ignore_index=True,)
    return SplineEOS(hugo["V(--)"], hugo["P(GPA)"], rho_0=1.9)


def get_data():
    """Data that already has noise added"""
    hugo = pd.read_csv(Path(__file__).parent / "prob_2_data.csv")
    # Include the point (1, 0) which is theoretically required
    #   as the reference point/initial value
    hugo = hugo.append({"V(--)": 1, "P(GPA)": 0,}, ignore_index=True,)
    return hugo


def plot_against_known_truth(initial_spline, result_spline, data_v, data_P):
    fig: plt.Figure
    ax: plt.Axes
    fig, ax = plt.subplots()

    v = np.linspace(min(data_v) - 0.005, max(data_v) + 0.05, 10000)

    _err = result_spline(v) - initial_spline(v)
    _sq = _err ** 2
    _mean = _sq.mean()
    rmse = np.sqrt(_mean)

    ax.plot(v, initial_spline(v), color=ARA_BLUE, label="Known Truth")
    ax.scatter(data_v, data_P, color=ARA_BLUE, label="Data")
    ax.plot(v, result_spline(v), color=ARA_ORANGE, alpha=0.7, label="Result")
    ax.legend()
    ax.set_xlabel(r"Relative Volume $\nu$")
    ax.set_ylabel(r"Pressure $P$ (GPa)")
    ax.set_title(
        f"GA Spline Fit Against Known Truth\nRMSE: {round(rmse, 4)} GPa"
    )
    return fig, ax


def make_out_dir(run_name: Optional[str] = None) -> Path:
    date_stamp = date.today().isoformat()
    file_name = (
        f"{date_stamp}_{run_name}"
        if run_name is not None
        else f"{date_stamp}_spline_fit"
    )
    out_dir = Path(__file__).parent / "results" / file_name
    if not out_dir.exists():
        out_dir.mkdir(exist_ok=True, parents=True)
        return out_dir

    c = itertools.count()
    while out_dir.exists():
        suffix = str(next(c)).zfill(2)
        out_dir = out_dir.parent / f"{file_name}_{suffix}"

    out_dir.mkdir()
    return out_dir


if __name__ == "__main__":
    time.sleep(int(60 * 60 * 2.5))
    # Prep directory
    run_name = "prob_2"
    out_dir = make_out_dir(run_name)

    # Prep problem
    rho_0 = 1.9
    base_spline = initial_spline()
    data = get_data()
    v_spec = data["V(--)"] / rho_0
    data_rho = np.full_like(data["V(--)"], rho_0)

    # Establish pymoo variables
    problem = OptimizeSpline(base_spline, v_spec, data["P(GPA)"], data_rho)
    algorithm = NSGA2(pop_size=10000)
    termination = SingleObjectiveDefaultTermination(
        x_tol=1e-8,
        cv_tol=1e-6,
        f_tol=1e-6,
        nth_gen=5,
        n_last=20,
        n_max_gen=100000,
        n_max_evals=None,
    )

    # Optimize
    tic = datetime.now()
    res = minimize(
        problem,
        algorithm,
        termination,
        callback=SavePopulation(out_dir, batch_size=100),
        verbose=True,
        seed=1,
    )
    toc = datetime.now()
    print(toc - tic)
    print(f"res.X:\n{res.X}")
    print(f"res.F:\n{res.F}")

    # post process
    result_spline = base_spline.new_spline_from_coeffs(res.X)

    res_sp_fig, res_sp_ax = plot_spline_against_data(
        result_spline, data["V(--)"], data["P(GPA)"]
    )
    res_sp_fig.savefig(out_dir / "result_spline_plot.eps")
    truth_fig, truth_ax = plot_against_known_truth(
        initial_spline(), result_spline, data["V(--)"], data["P(GPA)"]
    )
    truth_fig.savefig(out_dir / "prob_1_result_against_truth.eps")

    with (out_dir / f"{run_name}_opt_problem.pickle").open("wb") as f:
        pickle.dump(problem, f)

    with (out_dir / f"{run_name}_opt_result.pickle").open("wb") as f:
        pickle.dump(res, f)

    with (out_dir / f"{run_name}_result_spline.pickle").open("wb") as f:
        pickle.dump(result_spline, f)
