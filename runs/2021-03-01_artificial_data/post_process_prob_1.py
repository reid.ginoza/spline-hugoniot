# Copyright (C) 2021 Reid Ginoza
# All rights reserved.
"""Mostly creating plots."""
from pathlib import Path
import pickle

from matplotlib import pyplot as plt
import numpy as np
import pandas as pd


results_dir = Path(__file__).parent / "results" / "2021-04-19_prob_1"


def get_early_plots():
    with (results_dir / "archive" / "gen_0001_0100.pickle").open("rb") as f:
        early_gens = pickle.load(f)

    return early_gens


if __name__ == "__main__":
    early_gens = get_early_plots()
