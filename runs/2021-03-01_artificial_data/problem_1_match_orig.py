"""Test case for the SplineEOS and OptimizeSpline classes.

Uses a known truth with all available data to see if the algorithm
can optimize to the known truth.
"""
######################################
# Recent Results
######################################
# Time: 2:41:42.857953
# Generations: 575
# Number of Evals: 5750000
#
# Coefficients:
# Results         Truth               Residual
# 96.8600118      96.86000000000001   -1.1789282581275984e-05
# 90.8589027      90.85942349492585    0.0005208116093768922
# 82.5268583      82.5262851544149    -0.0005731388908145618
# 72.5549454      72.55529419142842    0.00034876426138907846
# 65.2379611      65.23769978787472   -0.00026130038004623657
# 58.2122982      58.21256196712225    0.0002637368640563409
# 51.5708771      51.57066825493778   -0.0002088055524183119
# 45.2754127      45.27553279393329    0.00012008026357790413
# 39.3043312      39.30426656785794   -6.46198789198138e-05
# 33.7254417      33.725495447262276   5.371423103639472e-05
# 28.4783689      28.478343099871626  -2.5829733466764537e-05
# 23.6218014      23.62185492866599    5.35717391620949e-05
# 19.1393709      19.1392878613843    -8.305040733347369e-05
# 15.05001        15.05010766854102    9.76193999431274e-05
# 11.3465818      11.346446147942249  -0.0001356934625320605
# 8.07138351      8.071559588153741    0.00017607663192009682
# 5.21417979      5.213927415193053   -0.000252375576767605
# 2.81770408      2.818344126786154    0.0006400490232261191
# 0.899375351     0.896582778904702   -0.002792571653237763
# 0.280008369     0.2889437784011535   0.008935409674278971
# 3.69253614e-07  0.0                 -3.692536138678186e-07
# max residual: 0.008935409674278971
######################################
# Original Results from old repo
######################################
# Results:
#   Time:  1:51:38.374735
#   Generations: 335
#   Number of Evals: 3350000
#
#   Coefficients
#         Results      Truth      Diff
#   0   96.859984  96.860000 -0.000016
#   1   90.858765  90.859423 -0.000658
#   2   82.526999  82.526285  0.000714
#   3   72.554706  72.555294 -0.000588
#   4   65.237880  65.237700  0.000181
#   5   58.212507  58.212562 -0.000055
#   6   51.570523  51.570668 -0.000145
#   7   45.275772  45.275533  0.000239
#   8   39.303973  39.304267 -0.000293
#   9   33.725772  33.725495  0.000277
#   10  28.478131  28.478343 -0.000212
#   11  23.622124  23.621855  0.000269
#   12  19.138784  19.139287 -0.000503
#   13  15.050983  15.050111  0.000872
#   14  11.344507  11.346431 -0.001924
#   15   8.075981   8.071634  0.004348
#   16   5.204816   5.213550 -0.008734
#   17   1.300770   1.274310  0.026459
#   18   0.517769   0.555944 -0.038175
#   19   0.005593   0.005234  0.000359

# Run 2
# == Problem Description ==
# Spline Knots: [0.60932702 0.6237721  0.6316313  0.63999194 0.64888616 0.65844048
#  0.66872609 0.679823   0.69192213 0.7051638  0.71981863 0.73620343
#  0.75475436 0.77606225 0.80116074 0.83151462 0.86990274 0.92167013
#  1.        ]
# Spline Number of Coeffs: 21
# Coeffs lower bound: [87.174      81.77348115 74.27365664 65.29976477 58.71392981 52.39130577
#  46.41360143 40.74797951 35.37383991 30.3529459  25.63050879 21.25966944
#  17.22535908 13.5450969  10.21180153  7.26440363  4.69253467  2.53650971
#   0.8069245   0.2600494  -0.1       ]
# Coeffs upper bound: [1.06546000e+02 9.99453658e+01 9.07789137e+01 7.98108236e+01
#  7.17614698e+01 6.40338182e+01 5.67277351e+01 4.98030861e+01
#  4.32346932e+01 3.70980450e+01 3.13261774e+01 2.59840404e+01
#  2.10532166e+01 1.65551184e+01 1.24810908e+01 8.87871555e+00
#  5.73532016e+00 3.10017854e+00 9.86241057e-01 3.17838156e-01
#  1.00000000e-01]

# Result
# Time: 2:35:49.411461

# array([9.68600223e+01, 9.08588930e+01, 8.25270211e+01, 7.25547950e+01,
#        6.52380483e+01, 5.82123430e+01, 5.15708237e+01, 4.52754123e+01,
#        3.93044024e+01, 3.37253900e+01, 2.84784114e+01, 2.36218020e+01,
#        1.91393525e+01, 1.50500066e+01, 1.13465591e+01, 8.07142667e+00,
#        5.21417923e+00, 2.81758646e+00, 9.00514907e-01, 2.76526323e-01,
#        6.96996117e-07])
# Original
# array([96.86      , 90.85942349, 82.52628515, 72.55529419, 65.23769979,
#        58.21256197, 51.57066825, 45.27553279, 39.30426657, 33.72549545,
#        28.4783431 , 23.62185493, 19.13928786, 15.05010767, 11.34644615,
#         8.07155959,  5.21392742,  2.81834413,  0.89658278,  0.28894378,
#         0.        ])
# Diff
# array([ 2.23319182e-05, -5.30516582e-04,  7.35938189e-04, -4.99190424e-04,
#         3.48549406e-04, -2.18988996e-04,  1.55436067e-04, -1.20515809e-04,
#         1.35851411e-04, -1.05496726e-04,  6.83067731e-05, -5.29369364e-05,
#         6.46170327e-05, -1.01096922e-04,  1.12909486e-04, -1.32913158e-04,
#         2.51814427e-04, -7.57662844e-04,  3.93212836e-03, -1.24174558e-02,
#         6.96996117e-07])

from datetime import date, datetime
import itertools
from pathlib import Path
import pickle

from matplotlib import pyplot as plt
import numpy as np
import pandas as pd
from pymoo.algorithms.nsga2 import NSGA2
from pymoo.optimize import minimize
from pymoo.util.termination.default import SingleObjectiveDefaultTermination

from seos.splines.spline_eos import SplineEOS
from seos.optimizer.opt_plots import plot_spline_against_data
from seos.optimizer.spline_opt import OptimizeSpline, SavePopulation

ARA_BLUE = "#002E6C"
ARA_DARK_BLUE = "#00204B"
ARA_BRIGHT_BLUE = "#0092CA"
ARA_ORANGE = "#F05532"
ARA_GREEN = "#D2DD3F"
ARA_GREY = "#DEDEDE"
ARA_DARK_GREY = "#666666"


def initial_spline() -> SplineEOS:
    hugo = pd.read_csv(Path(__file__).parent / "lx17_jwl_ur.csv")
    # Include the point (1, 0) which is theoretically required
    #   as the reference point/initial value
    hugo = hugo.append({"V(--)": 1, "P(GPA)": 0,}, ignore_index=True,)
    return SplineEOS(hugo["V(--)"], hugo["P(GPA)"], rho_0=1.9)


def get_data():
    hugo = pd.read_csv(Path(__file__).parent / "lx17_jwl_ur.csv")
    # Include the point (1, 0) which is theoretically required
    #   as the reference point/initial value
    hugo.append(
        {"V(--)": 1, "P(GPA)": 0,}, ignore_index=True,
    )
    return hugo


def plot_against_known_truth(initial_spline, result_spline, data_v, data_P):
    fig: plt.Figure
    ax: plt.Axes
    fig, ax = plt.subplots()

    v = np.linspace(min(data_v) - 0.005, max(data_v) + 0.05, 10000)

    _err = result_spline(v) - initial_spline(v)
    _sq = _err ** 2
    _mean = _sq.mean()
    rmse = np.sqrt(_mean)

    ax.plot(v, initial_spline(v), color=ARA_BLUE, label="Known Truth")
    ax.scatter(data_v, data_P, color=ARA_BLUE, label="Data")
    ax.plot(v, result_spline(v), color=ARA_ORANGE, alpha=0.7, label="Result")
    ax.legend()
    ax.set_xlabel(r"Relative Volume $\nu$")
    ax.set_ylabel(r"Pressure $P$ (GPa)")
    ax.set_title(
        f"GA Spline Fit Against Known Truth\nRMSE: {round(rmse, 4)} GPa"
    )
    return fig, ax


def make_out_dir() -> Path:
    date_stamp = date.today().isoformat()
    file_name = f"{date_stamp}_prob_1"
    out_dir = Path(__file__).parent / "results" / file_name
    if not out_dir.exists():
        out_dir.mkdir(exist_ok=True, parents=True)
        return out_dir

    c = itertools.count()
    while out_dir.exists():
        suffix = str(next(c)).zfill(2)
        out_dir = out_dir.parent / f"{file_name}_{suffix}"

    out_dir.mkdir()
    return out_dir


if __name__ == "__main__":
    # Prep directory
    out_dir = make_out_dir()

    # Prep problem
    rho_0 = 1.9
    base_spline = initial_spline()
    data = get_data()
    v_spec = data["V(--)"] / rho_0
    data_rho = np.full_like(data["V(--)"], rho_0)

    # Establish pymoo variables
    problem = OptimizeSpline(base_spline, v_spec, data["P(GPA)"], data_rho)
    algorithm = NSGA2(pop_size=10000)
    termination = SingleObjectiveDefaultTermination(
        x_tol=1e-8,
        cv_tol=1e-6,
        f_tol=1e-6,
        nth_gen=5,
        n_last=20,
        n_max_gen=100000,
        n_max_evals=None,
    )

    # Optimize
    tic = datetime.now()
    res = minimize(
        problem,
        algorithm,
        termination,
        callback=SavePopulation(out_dir, batch_size=100),
        verbose=True,
        seed=1,
    )
    toc = datetime.now()
    print(toc - tic)
    print(f"res.X:\n{res.X}")
    print(f"res.F:\n{res.F}")

    # post process
    result_spline = base_spline.new_spline_from_coeffs(res.X)

    res_sp_fig, res_sp_ax = plot_spline_against_data(
        result_spline, data["V(--)"], data["P(GPA)"]
    )
    res_sp_fig.savefig(out_dir / "result_spline_plot.eps")

    cmd = r"""res_sp_ax.set_title("GA Best Fit 575th Generation\nRMSD: 7.703e-5 GPa")"""
    print(f"Change title using: {cmd}")

    truth_fig, truth_ax = plot_against_known_truth(
        initial_spline(), result_spline, data["V(--)"], data["P(GPA)"]
    )
    truth_fig.savefig(out_dir / "prob_1_result_against_truth.eps")

    with (out_dir / "prob_1_opt_problem.pickle").open("wb") as f:
        pickle.dump(problem, f)

    with (out_dir / "prob_1_opt_result.pickle").open("wb") as f:
        pickle.dump(res, f)

    with (out_dir / "prob_1_result_spline.pickle").open("wb") as f:
        pickle.dump(result_spline, f)
