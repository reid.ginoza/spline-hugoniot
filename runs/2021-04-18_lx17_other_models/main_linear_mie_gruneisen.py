# Copyright (C) 2021 Reid Ginoza
# All rights reserved.
"""Needs a docstring."""

from datetime import datetime
from functools import partial
from pathlib import Path
import pickle
from typing import Collection, List, Optional

from matplotlib import pyplot as plt
import numpy as np
import pandas as pd
from pymoo.algorithms.nsga2 import NSGA2
from pymoo.model.algorithm import Algorithm
from pymoo.model.population import Population
from pymoo.model.result import Result
from pymoo.optimize import minimize
from pymoo.util.termination.default import SingleObjectiveDefaultTermination
from scipy.optimize import curve_fit

from seos.optimizer.spline_opt import OptimizeSpline, SavePopulation
from seos.optimizer.opt_plots import sanity_check_plot, plot_spline_against_data
from seos.other_models.mg import MGLinear
from seos.splines.default_spline import make_many_v_pts
from seos.splines.spline_eos import SplineEOS

from seos.optimizer.opt_plots import (
    ARA_BLUE,
    ARA_DARK_BLUE,
    ARA_BRIGHT_BLUE,
    ARA_ORANGE,
    ARA_GREEN,
    ARA_GREY,
    ARA_DARK_GREY,
)


def get_data():
    data = pd.read_csv(
        Path(__file__).parent / "LX-17_data_v3_only_data.csv", comment="#"
    )
    data = data.append(
        {"rho_0": 1.9, "v_spec": 1 / 1.9, "P": 0.0, "Label": "Reference Point"},
        ignore_index=True,
    )
    return data


def get_overdriven_data():
    data = pd.read_csv(
        Path(__file__).parent / "LX-17_overdriven_data_with_v3.csv", comment="#"
    )
    return data


def _filter_v(v: np.ndarray, data_v_rel: np.ndarray):
    """Get one point to the left of the smallest data v and all higher v's."""
    idx = np.searchsorted(v, data_v_rel.min())
    return v[idx - 1 :]


def _half_v_points(v: np.ndarray) -> np.ndarray:
    """Not robust! Just down samples the make_many_v_pts array."""
    new_v = np.array([v_ for idx, v_ in enumerate(v) if idx % 2 == 1])
    assert 1.0 in new_v
    assert len(new_v) > 2
    return new_v


def add_data_to_ax(
    ax: plt.Axes,
    data: pd.DataFrame,
    od_data: pd.DataFrame,
    with_overdriven: bool = True,
) -> plt.Axes:
    """Plots scatter data on ax in specific volume coordinates."""
    ax.scatter(
        data.loc[data["Label"] == "Jackson Gun", "v_spec"],
        data.loc[data["Label"] == "Jackson Gun", "P"],
        label="Jackson Gun",
        color=ARA_GREEN,
        marker="^",
    )

    ax.scatter(
        data.loc[data["Label"] == "Jackson Wedge", "v_spec"],
        data.loc[data["Label"] == "Jackson Wedge", "P"],
        label="Jackson Wedge",
        color=ARA_GREEN,
        marker="s",
    )

    ax.scatter(
        data.loc[data["Label"] == "Dallman", "v_spec"],
        data.loc[data["Label"] == "Dallman", "P"],
        label="Dallman " r"($\rho_0=1.913$)",
        color=ARA_BRIGHT_BLUE,
    )

    ax.scatter(
        data.loc[data["Label"] == "Gustavsen", "v_spec"],
        data.loc[data["Label"] == "Gustavsen", "P"],
        label="Gustavsen",
        color=ARA_DARK_BLUE,
    )

    if with_overdriven:
        ax.scatter(
            od_data.loc[od_data["Label"] == "Holmes", "v_spec"],
            od_data.loc[od_data["Label"] == "Holmes", "P"],
            label="Holmes (Overdriven)",
            color="aquamarine",
        )

    ax.scatter(
        1 / 1.9, 0, color="red", label="Reference Point " r"($\rho_0 = 1.9$)"
    )
    return ax


def data_plot(
    data: pd.DataFrame, od_data: pd.DataFrame, with_overdriven: bool = True
):
    """Specific to the LX17 data set"""
    fig: plt.Figure
    ax: plt.Axes
    fig, ax = plt.subplots()

    ax = add_data_to_ax(ax, data, od_data, with_overdriven)

    ax.legend()
    ax.set_xlabel("Volume (cc/g)")
    ax.set_ylabel("Pressure (GPa)")
    title = "Hugoniot Data"
    if not with_overdriven:
        title += " (zoomed)"
    ax.set_title(title)
    return fig, ax


def data_plot_with_bounds(
    base_spline: SplineEOS,
    lower_bounds,
    upper_bounds,
    data: pd.DataFrame,
    od_data: pd.DataFrame,
    with_overdriven: bool = True,
):
    """Specific to the LX-17 Data set."""
    if with_overdriven:
        all_data = pd.concat([data, od_data], ignore_index=True)
    else:
        all_data = data

    v_spec = np.linspace(
        all_data["v_spec"].min() * 0.9, all_data["v_spec"].max() * 1.05, 10000
    )
    v_rel = v_spec * 1.9

    lower_bound_spline = base_spline.new_spline_from_coeffs(lower_bounds)
    upper_bound_spline = base_spline.new_spline_from_coeffs(upper_bounds)

    fig: plt.Figure
    ax: plt.Axes
    fig, ax = plt.subplots()

    ax.fill_between(
        v_spec,
        lower_bound_spline(v_rel),
        upper_bound_spline(v_rel),
        color="lightsteelblue",
        label="Bounds for Spline",
        alpha=0.8,
    )
    # Used this to make sure the bounds don't cross, but don't want in
    #   final plot.
    # ax.plot(
    #     v_spec,
    #     lower_bound_spline(v_rel),
    #     label="Lower Bound",
    #     color="slategrey",
    # )

    # Originally, wanted to plot the distended bounds, but they differ by
    # less than 1 GPa.
    # ax.fill_between(
    #     v_spec,
    #     lower_bound_spline.pressure_distended(v_spec, 1.913),
    #     upper_bound_spline.pressure_distended(v_spec, 1.913),
    #     label="Higher Density Hugoniot " r"($\rho_0 = 1.913$)",
    #     color="wheat",
    #     alpha=0.8,
    # )

    # ax.plot(
    #     v_spec,
    #     lower_bound_spline.pressure_distended(v_spec, 1.913),
    #     label="Higher Density Hugoniot " r"($\rho_0 = 1.913$)",
    #         color="wheat",
    #         alpha=0.8,
    # )

    ax = add_data_to_ax(ax, data, od_data, with_overdriven)

    ax.legend()
    ax.set_xlabel("Volume (cc/g)")
    ax.set_ylabel("Pressure (GPa)")
    title = "Hugoniot Data and Spline Bounds"
    if not with_overdriven:
        title += " (zoomed)"
    ax.set_title(title)
    return fig, ax


# def result_spline_against_data(
#     result_spline: SplineEOS,
#     data: pd.DataFrame,
#     od_data: pd.DataFrame,
#     rmsd: Optional[float] = None,
#     ax: Optional[plt.Axes] = None,
#     with_overdriven: bool = True,
# ):
#     """Specific to the LX17 data set"""
#     if ax is None:
#         fig: plt.Figure
#         ax: plt.Axes
#         fig, ax = plt.subplots()
#     else:
#         fig = ax.figure
#
#     ax = add_data_to_ax(ax, data, od_data, with_overdriven)
#
#     if with_overdriven:
#         all_data = pd.concat([data, od_data], ignore_index=True)
#     else:
#         all_data = data
#     v_spec = np.linspace(
#         all_data["v_spec"].min() * 0.9, all_data["v_spec"].max() * 1.05, 10000
#     )
#     v_rel = v_spec * result_spline.rho_0
#     spline_p = result_spline(v_rel)
#     ax.plot(v_spec, spline_p, color=ARA_ORANGE, label="Result Spline")
#
#     ax.legend()
#     ax.set_xlabel("Volume (cc/g)")
#     ax.set_ylabel("Pressure (GPa)")
#
#     title = "Result Spline with Data"
#     if not with_overdriven:
#         title += " (zoomed)"
#     if rmsd is not None:
#         title += f"\nRMSD: {rmsd} GPa"
#     ax.set_title(title)
#     return fig, ax


if __name__ == "__main__":
    out_dir = Path(__file__).parent / "round_1_mg_linear"
    out_dir.mkdir(exist_ok=True)

    tic = datetime.now()
    data = get_data()
    od_data = get_overdriven_data()

    d_fig, d_ax = data_plot(data, od_data)
    d_fig.savefig(out_dir / "Data_only.png")
    z_d_fig, z_d_ax = data_plot(data, od_data, with_overdriven=False)
    z_d_fig.savefig(out_dir / "Data_only_zoom.png")

    plt.close("all")

    problem = OptimizeSpline(
        base_spline,
        data.v_spec,
        data.P,
        data.rho_0,
        xl=lower_bounds,
        xu=upper_bounds,
        overdriven_v_spec=od_data.v_spec,
        overdriven_P=od_data.P,
    )
    algorithm = NSGA2(
        pop_size=10000,
        # default of 1/n_var = 0.0526
        # left here for reference
        # mutation=get_mutation("real_pm", prob=0.1),
    )
    termination = SingleObjectiveDefaultTermination(
        x_tol=1e-8,
        cv_tol=1e-6,
        f_tol=1e-6,
        nth_gen=5,
        n_last=20,
        n_max_gen=100000,
        n_max_evals=None,
    )
    res = minimize(
        problem,
        algorithm,
        termination,
        # trying to handle history with call backs
        # save_history=True,
        callback=SavePopulation(out_dir, batch_size=100),
        verbose=True,
        seed=1,
    )
    toc = datetime.now()
    print(toc - tic)
    print(f"res.X:\n{res.X}")
    print(f"res.F:\n{res.F}")

    with (out_dir / "problem.pickle").open("wb") as f:
        pickle.dump(problem, f)

    with (out_dir / "opt_result.pickle").open("wb") as f:
        pickle.dump(res, f)

    result_spline = base_spline.new_spline_from_coeffs(res.X)

    res_fig, res_ax = plot_spline_against_data(
        result_spline, data.v_spec * base_spline.rho_0, data.P, rmsd=res.F
    )
    res_fig.savefig(out_dir / "Optimization_Result_generic.png")

    full_res_fig, full_res_ax = result_spline_against_data(
        result_spline, data, od_data, rmsd=res.F
    )
    full_res_fig.savefig(out_dir / "Optimization_Result_full.png")
    z_full_res_fig, z_full_res_ax = result_spline_against_data(
        result_spline, data, od_data, rmsd=res.F, with_overdriven=False,
    )
    z_full_res_fig.savefig(out_dir / "Optimization_Result_full_zoom.png")

    kb_fig, kb_ax = plot_knots_and_bounds_results(result_spline, data, od_data)
    kb_fig.savefig(out_dir / "Optimization_Result_knots_and_bounds.png")
    z_kb_fig, z_kb_ax = plot_knots_and_bounds_results(
        result_spline, data, od_data, with_overdriven=False
    )
    z_kb_fig.savefig(out_dir / "Optimization_Result_knots_and_bounds_zoom.png")

    # Plotting code note updated. Commented out as a reference.
    # result_spline = res_to_final_spline(res, base_spline)
    # res_sp_fig, res_sp_ax = plot_spline_against_data(
    #     result_spline, data["V(--)"], data["P(GPA)"]
    # )
    # res_sp_fig.savefig(out_dir / "result_spline.png")
    #
    # pp_fig, pp_ax = post_processing_plot(
    #     initial_spline(), result_spline, data["V(--)"], data["P(GPA)"]
    # )
    # pp_fig.savefig(out_dir / "result_comparison.png")
    #
    # fit_fig_1, fit_ax_1 = fitness_by_generation_plot(res, gen_start=0)
    # fit_fig_1.savefig(out_dir / "fitness_by_generation.png")
    #
    # try:
    #     fit_fig_2, fit_ax_2 = fitness_by_generation_plot(res, gen_start=200)
    # except KeyError:
    #     print(
    #         "Could not find 200th generation or higher, so did not "
    #         "plot zoomed in fitness by generation"
    #     )
    # else:
    #     fit_fig_2.savefig(out_dir / "zoomed_fitness_by_gen.png")
    #
    # try:
    #     fit_fig_3, fit_ax_3 = fitness_by_generation_plot(
    #         res, gen_start=int(len(res.history) * 0.8)
    #     )
    # except KeyError:
    #     print("Error in plotting code. Will not stop processing.")
    # else:
    #     fit_fig_3.savefig(out_dir / "zoomed_fitness_last_20_percent.png")
