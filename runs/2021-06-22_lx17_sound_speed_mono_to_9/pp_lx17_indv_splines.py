# Copyright (C) 2021 Reid Ginoza
# All rights reserved.
"""Examines individual splines in the optimization progress of LX-17 fit."""
from dataclasses import dataclass
from pathlib import Path
import pickle
from typing import List, Optional, Union

import matplotlib

matplotlib.use("TkAgg")
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

from pymoo.model.population import Population
from pymoo.model.individual import Individual
from pymoo.model.result import Result

from seos.optimizer.spline_opt_fixed_ref import OptimizeSplineFixedRef
from seos.splines.spline_eos import SplineEOS

from lx17_plots import plot_spline_with_sound_speed, add_data_to_us_up_ax
from main_lx17 import (
    get_data,
    get_overdriven_data,
    data_plot,
    result_spline_against_data,
)

ARA_BLUE = "#002E6C"
ARA_DARK_BLUE = "#00204B"
ARA_BRIGHT_BLUE = "#0092CA"
ARA_ORANGE = "#F05532"
ARA_GREEN = "#D2DD3F"
ARA_GREY = "#DEDEDE"
ARA_DARK_GREY = "#666666"


def get_opt_problem(results_dir: Path):
    with open(
        results_dir
        / "lx17_fixed_ref_upper_bound_sound_speed_opt_problem.pickle",
        "rb",
    ) as f:
        opt_prob: OptimizeSplineFixedRef = pickle.load(f)

    return opt_prob


@dataclass
class IndvWhileSearching:
    indv: Individual
    gen: int
    indv_idx: int

    def file_name(self):
        mul_5 = self.gen % 5 == 0
        correction = -4 if mul_5 else +1
        start_gen = self.gen // 5 * 5 + correction
        end_gen = start_gen + 4
        return f"gen_{str(start_gen).zfill(4)}_{str(end_gen).zfill(4)}.pickle"


def convex_violation(indv: Optional[Union[Individual, IndvWhileSearching]]):
    if indv is None:
        return 0

    elif isinstance(indv, IndvWhileSearching):
        return indv.indv.G[2]

    else:
        return indv.G[2]


def find_non_convex_early_gens(archive_dir: Path):
    with (archive_dir / "gen_0001_0005.pickle").open("rb") as f:
        gen_1_to_5: List[Population] = pickle.load(f)

    worst_non_convex = None

    for g_idx, gen in enumerate(gen_1_to_5, start=1):
        for i_idx, indv in enumerate(gen):
            if convex_violation(indv) > convex_violation(worst_non_convex):
                worst_non_convex = IndvWhileSearching(indv, g_idx, i_idx)

    return worst_non_convex


def find_late_non_convex(archive_dir: Path, file_idx_to_search=7):
    worst_non_convex = None

    for gens_pickle in get_gens_pickles(archive_dir)[
        file_idx_to_search : file_idx_to_search + 1
    ]:
        with gens_pickle.open("rb") as f:
            gens: List[Population] = pickle.load(f)

        start = int(gens_pickle.stem.split("_")[1])

        for g_idx, gen in enumerate(gens, start=start):
            for i_idx, indv in enumerate(gen):
                # commented out because I wasn't watching it
                # print(f"checking {g_idx}.{i_idx}: {indv.G}")
                # print(
                #     f"==> worst_non_convex: {worst_non_convex} "
                #     f"{convex_violation(worst_non_convex)}"
                # )
                if convex_violation(indv) > 0 and (
                    worst_non_convex is None
                    or convex_violation(indv)
                    > convex_violation(worst_non_convex)
                ):
                    worst_non_convex = IndvWhileSearching(indv, g_idx, i_idx)
    print(f"fine_late_non_convex: {worst_non_convex}")
    return worst_non_convex


def get_early_spline(
    archive_dir: Path,
    opt_problem: OptimizeSplineFixedRef,
    gen: int,
    indv_idx: int,
    file_name: str = "gen_0001_0005.pickle",
) -> SplineEOS:
    """Lots of hard-coded names specifically to make this one plot."""
    with (archive_dir / file_name).open("rb") as f:
        gens: List[Population] = pickle.load(f)

    indv: Individual = gens[gen][indv_idx]

    coeffs: np.ndarray = opt_problem.make_full_coeffs(indv.X)

    return opt_problem.base_spline.new_spline_from_coeffs(coeffs)


def get_late_spline(
    late_non_convex: IndvWhileSearching, opt_problem: OptimizeSplineFixedRef,
):
    coeffs: np.ndarray = opt_problem.make_full_coeffs(late_non_convex.indv.X)
    return opt_problem.base_spline.new_spline_from_coeffs(coeffs)


def get_gens_pickles(archive_dir: Path):
    return sorted(p for p in archive_dir.iterdir() if p.suffix == ".pickle")


def shape_ok_sound_speed_violation(
    indv: Optional[Union[Individual, IndvWhileSearching]]
):
    """Checks the individual for the highest sound speed violation that has okay shape."""
    if indv is None:
        return 0.0

    elif isinstance(indv, IndvWhileSearching):
        i = indv.indv
    else:
        i = indv

    shape_constraints = i.G[:3]

    if np.all(shape_constraints < 0.0):
        return 0.0

    sound_speed_constraints = i.G[3:5]
    return sound_speed_constraints.max()


def find_late_sound_speed_bad(archive_dir: Path):
    sound_speed_bad = None
    gen_idx = 1

    for gens_pickle in get_gens_pickles(archive_dir):
        with gens_pickle.open("rb") as f:
            gens: List[Population] = pickle.load(f)

        for gen in gens:
            for i_idx, indv in enumerate(gen):
                if shape_ok_sound_speed_violation(
                    indv
                ) > shape_ok_sound_speed_violation(sound_speed_bad):
                    sound_speed_bad = IndvWhileSearching(indv, gen_idx, i_idx)
            gen_idx += 1

    return sound_speed_bad


def is_sound_speed_bad(indv: Optional[Union[Individual, IndvWhileSearching]]):
    """Checks the individual for the highest sound speed violation."""
    if indv is None:
        return 0.0

    elif isinstance(indv, IndvWhileSearching):
        i = indv.indv
    else:
        i = indv

    sound_speed_constraints = i.G[3:5]
    return sound_speed_constraints.max()


def get_all_bad_sound_speed(archive_dir: Path):
    all_bad = []
    gen_idx = 1

    for gens_pickle in get_gens_pickles(archive_dir):
        with gens_pickle.open("rb") as f:
            gens: List[Population] = pickle.load(f)

        for gen in gens:
            for i_idx, indv in enumerate(gen):
                if is_sound_speed_bad(indv) > 0:
                    all_bad.append(IndvWhileSearching(indv, gen_idx, i_idx))
            gen_idx += 1
    return all_bad


def get_least_bad_shape(all_bad: List[IndvWhileSearching]):
    def get_max_shape_constraints(indv: IndvWhileSearching):
        return indv.indv.G[:3].max()

    return min(all_bad, key=get_max_shape_constraints)


def plot_sound_speed(spline:SplineEOS, ax:plt.Axes, data:pd.DataFrame, od_data:pd.DataFrame):
    all_data = pd.concat([data, od_data], ignore_index=True)
    v_spec = np.linspace(all_data["v_spec"].min() * 0.95, 1 / 1.9, 10000)
    v_rel = v_spec * spline.rho_0
    c_sq = spline.sound_speed_squared_at_v_rel(v_rel)
    c = np.sqrt(c_sq)
    ax.plot(v_spec, c, color=ARA_BLUE)
    ax.set_xlabel("Volume (cc/g)")
    ax.set_ylabel("Sound Speed (mm/" r"$\mu$" "s)")
    return ax.figure, ax


def get_late_sound_speed_bad_spline(
    archive_dir: Path,
    file_to_search: str,
    opt_problem: OptimizeSplineFixedRef,
    gen: int,
    indv_idx: int,
) -> SplineEOS:
    """Lots of hard-coded names specifically to make this one plot."""
    with (archive_dir / file_to_search).open("rb") as f:
        gen_51_to_55: List[Population] = pickle.load(f)

    indv: Individual = gen_51_to_55[gen][indv_idx]

    coeffs: np.ndarray = opt_problem.make_full_coeffs(indv.X)

    return opt_problem.base_spline.new_spline_from_coeffs(coeffs)


def plot_late_spline_against_result(
    data: pd.DataFrame,
    od_data: pd.DataFrame,
    late_spline: SplineEOS,
    gen: int,
    description: str = "Sound Speed Constraints Not Met",
):
    fig: plt.Figure
    ax: plt.Axes
    fig, ax = plt.subplots()

    v_plot = np.linspace(data["V(--)"].min(), 1.0)
    ax.plot(
        v_plot, late_spline(v_plot), label="Result Spline", color=ARA_ORANGE
    )

    ax.set_xlabel("Relative volume " r"$\nu$")
    ax.set_ylabel("Pressure " r"$P$ (GPa)")
    ax.set_title(f"Spline in Generation {gen}\n{description}")
    ax.legend()
    return fig, ax


#
# ==========================


def is_feasible(indv) -> bool:
    if indv is None:
        return False

    i = indv.indv if isinstance(indv, IndvWhileSearching) else indv
    return np.all(i.G < 0)


def first_feasible(archive_dir: Path):
    with (archive_dir / "gen_0061_0065.pickle").open("rb") as f:
        gens: List[Population] = pickle.load(f)

    for g_idx, gen in enumerate(gens, start=61):
        for i_idx, indv in enumerate(gen):
            if is_feasible(indv):
                return IndvWhileSearching(indv, g_idx, i_idx)
    else:
        raise RuntimeError("Could not find a feasible spline")


def get_opt_result(results_dir: Path):
    with (
        results_dir / "lx17_fixed_ref_upper_bound_sound_speed_opt_result.pickle"
    ).open("rb") as f:
        return pickle.load(f)


def make_result_spline_pickle(results_dir: Path):
    """Specific to this run. Meant to be run once, only."""
    opt_problem: OptimizeSplineFixedRef = get_opt_problem(results_dir)
    opt_result: Result = get_opt_result(results_dir)
    coeffs = opt_problem.make_full_coeffs(opt_result.X)
    spline = opt_problem.base_spline.new_spline_from_coeffs(coeffs)
    with (results_dir / "lx17_result_spline.pickle").open("wb") as f:
        pickle.dump(spline, f)


def final_result(results_dir: Path, data: pd.DataFrame):
    raise NotImplementedError("This doesn't work!")
    with (results_dir / "lx17_result_spline.pickle").open("rb") as f:
        result_spline = pickle.load(f)

    with (
        results_dir / "lx17_fixed_ref_upper_bound_sound_speed_opt_result.pickle"
    ).open("rb") as f:
        opt_result = pickle.load(f)

    fig: plt.Figure
    ax: plt.Axes
    fig, ax = plt.subplots()

    v = np.linspace(data["V(--)"].min() - 0.005, 1.0, 10000)

    ax.scatter(data["V(--)"], data["P(GPA)"], color=ARA_BLUE, label="Data")
    ax.plot(v, result_spline(v), color=ARA_ORANGE, label="Result")
    ax.legend()
    ax.set_xlabel(r"Relative Volume $\nu$")
    ax.set_ylabel(r"Pressure $P$ (GPa)")
    ax.set_title(
        f"Optimization Result Generation 315\nRMSD: {float(opt_result.F):.3e} GPa"
    )
    return fig, ax


def get_initial_spline() -> SplineEOS:
    hugo = pd.read_csv(Path(__file__).parent / "lx17_jwl_ur.csv")
    # Include the point (1, 0) which is theoretically required
    #   as the reference point/initial value
    hugo = hugo.append({"V(--)": 1, "P(GPA)": 0,}, ignore_index=True,)

    spline = SplineEOS(
        hugo["V(--)"],
        hugo["P(GPA)"],
        rho_0=1.9,
        p0=3.3027e-04,
        e0=6.7890e-02,
        t0=298.15,
        cv=1.3052623990693266,
        gamma=8.94000000e-01,
        gamma_kind="constant",
    )

    # contract for reference point on spline
    # included as a debugging tool since earlier runs had a mistake
    # where the reference point was not in the data set
    tol = 1e-12
    assert spline(1) - 0.0 < tol
    return spline


def redo_plot_against_known_truth(
    results_dir: Path, data: pd.DataFrame, result_spline_pickle: Path,
):
    """Assumes initial_spline is the known truth.

    Redo-ing it only because the right end point was bigger than
    1.0, when really, the Hugoniot should stop at 1.0.
    """
    raise NotImplementedError("This doesn't work! Probably was copy pasta")
    initial_spline = get_initial_spline()
    with (results_dir / result_spline_pickle).open("rb") as f:
        result_spline = pickle.load(f)

    data_v, data_P = data["V(--)"], data["P(GPA)"]

    fig: plt.Figure
    ax: plt.Axes
    fig, ax = plt.subplots()

    v = np.linspace(min(data_v) - 0.005, 1.0, 10000)

    _err = result_spline(v) - initial_spline(v)
    _sq = _err ** 2
    _mean = _sq.mean()
    rmse = np.sqrt(_mean)

    ax.plot(v, initial_spline(v), color=ARA_BLUE, label="Known Truth")
    ax.scatter(data_v, data_P, color=ARA_BLUE, label="Data")
    ax.plot(v, result_spline(v), color=ARA_ORANGE, alpha=0.7, label="Result")
    ax.legend()
    ax.set_xlabel(r"Relative Volume $\nu$")
    ax.set_ylabel(r"Pressure $P$ (GPa)")
    ax.set_title(
        f"GA Spline Fit Against Known Truth\nRMSE: {round(rmse, 4)} GPa"
    )
    return fig, ax


def make_line(x_1, y_1, x_2, y_2):
    m = (y_2 - y_1) / (x_2 - x_1)

    def line(x):
        return m * (x - x_1) + y_1

    return line


def make_line_pts(x_1, y_1, x_2, y_2):
    line = make_line(x_1, y_1, x_2, y_2)

    x_pts = np.linspace(x_1, x_2)
    y_pts = line(x_pts)
    return x_pts, y_pts


def adjust_ax_for_jap(ax: plt.Axes):
    ax.tick_params(right=True, top=True)
    return ax


def change_axis_label_from_cc(ax:plt.Axes):
    ax.set_xlabel(r"Volume (cm$^3$/g)")
    return ax


if __name__ == "__main__":
    out_dir = Path(__file__).parent / "pp_out"
    out_dir.mkdir(exist_ok=True)

    results_dir = (
        Path(__file__).parent
        / "results"
        / "2021-06-22_lx17_fixed_ref_upper_bound_sound_speed"
    )
    archive_dir = results_dir / "archive"

    data = get_data()
    od_data = get_overdriven_data()

    opt_problem = get_opt_problem(results_dir)

    # Find Worst Early Gen
    # --------------------

    worst = find_non_convex_early_gens(archive_dir)
    early_spline = get_early_spline(
        archive_dir, opt_problem, gen=worst.gen, indv_idx=worst.indv_idx
    )
    fig, ax = result_spline_against_data(early_spline, data, od_data)
    ax.set_title("Spline in Initial Population\nNoticeably Non-convex")
    adjust_ax_for_jap(ax)
    change_axis_label_from_cc(ax)
    fig.savefig(out_dir / "lx17_gen_1_not_convex.png")
    fig.savefig(out_dir / "lx17_gen_1_not_convex.eps")

    # Find Latest Non-Convex
    # ----------------------

    late_non_convex = find_late_non_convex(archive_dir, file_idx_to_search=12)
    gen = late_non_convex.gen % 5 - 1
    late_spline = get_late_spline(late_non_convex, opt_problem)
    fig2, ax2 = result_spline_against_data(late_spline, data, od_data)

    ax2.set_title(
        f"Generation {late_non_convex.gen}: Slight Non-Convexity at "
        r"$v=0.488$"
    )
    adjust_ax_for_jap(ax2)
    change_axis_label_from_cc(ax2)
    fig2.savefig(out_dir / f"lx17_gen_{late_non_convex.gen}_not_convex.eps")
    fig2.savefig(out_dir / f"lx17_gen_{late_non_convex.gen}_not_convex.png")
    test_v = np.linspace(0.6, 1.0, 10000)
    d2p_dv2 = late_spline(test_v, nu=2)
    non_convex = d2p_dv2 < 0
    non_convex_v = test_v[non_convex]
    print(f"non_convex_v relative volume: {non_convex_v}")
    non_convex_v_spec = sorted(set(round(i / 1.9, 2) for i in non_convex_v))
    print(f"v_spec: {non_convex_v_spec}")

    # make zoom plot. not robust.  only mildly helpful.
    x_1 = non_convex_v.min() / 1.9
    y_1 = late_spline(non_convex_v.min())
    x_2 = non_convex_v.max() / 1.9
    y_2 = late_spline(non_convex_v.max())
    x_pts, y_pts = make_line_pts(x_1, y_1, x_2, y_2)
    ax2.plot(x_pts, y_pts)
    ax2.set_xlim(x_1, x_2)
    ax2.set_ylim(y_2, y_1)
    fig2.savefig(out_dir / "zoom.png")

    # Find first feasible
    # -------------------
    feasible_indv: IndvWhileSearching = first_feasible(archive_dir)
    feasible_spline = get_late_spline(feasible_indv, opt_problem)
    fig3, ax3 = result_spline_against_data(late_spline, data, od_data)
    # fig3, ax3 = plot_late_spline_against_result(
    #     data,
    #     feasible_spline,
    #     feasible_indv.gen,
    #     description=f"First Feasible Spline. RMSD: {round(float(feasible_indv.indv.F), 3)}",
    # )
    ax3.set_title(
        f"Generation {feasible_indv.gen}: First Feasible Spline. RMSD: {round(float(feasible_indv.indv.F), 3)}"
    )
    adjust_ax_for_jap(ax3)
    change_axis_label_from_cc(ax3)
    fig3.savefig(out_dir / f"lx17_gen_{feasible_indv.gen}_first_feasible.png")
    fig3.savefig(out_dir / f"lx17_gen_{feasible_indv.gen}_first_feasible.eps")

    # Final Result Spline
    # -------------------
    # This doesn't work!
    # fig4, ax4 = final_result(results_dir, data)
    # fig4.savefig(out_dir / f"lx17_final_rmsd_gen_315.png")
    # fig4.savefig(out_dir / f"lx17_final_rmsd_gen_315.eps")

    # Final against Truth
    # -------------------
    # This doesn't work!
    # fig5, ax5 = redo_plot_against_known_truth(results_dir, data)
    # fig5.savefig(out_dir / "lx17_final_against_truth.png")
    # fig5.savefig(out_dir / "lx17_final_against_truth.eps")

    # try to look for these!!!!
    # ==========================
    late_bad = find_late_sound_speed_bad(archive_dir)
    all_bad_sound_speed = get_all_bad_sound_speed(archive_dir)
    least_bad = get_least_bad_shape(all_bad_sound_speed)
    least_bad_spline = get_late_spline(least_bad, opt_problem)
    fig4, (ax4_0, ax4_1) = plt.subplots(nrows=2, sharex="all")
    fig4, ax4_0 = result_spline_against_data(
        least_bad_spline, data, od_data, ax=ax4_0
    )
    ax4_0.get_legend().remove()
    ax4_0.set_title("Hugoniot")
    fig4, ax4_1 = plot_sound_speed(least_bad_spline, ax4_1, data, od_data)

    x_lim = ax4_0.get_xlim()
    ax4_0.set_xlim(od_data["v_spec"].min()-0.005, x_lim[1])
    fig4.suptitle("Sound Speed Not Monotonic")
    adjust_ax_for_jap(ax4_0)
    adjust_ax_for_jap(ax4_1)
    fig4.savefig(out_dir / f"lx17_gen_{least_bad.gen}_sound_speed_bad.png")
    fig4.savefig(out_dir / f"lx17_gen_{least_bad.gen}_sound_speed_bad.eps")

    # start_gen = late_bad.gen // 5 * 5 + 1
    # end_gen = start_gen + 4
    # file_to_search = f"gen_{str(start_gen).zfill(4)}_{str(end_gen).zfill(4)}.pickle"
    # print(f"opening {file_to_search}")
    # late_spline = get_late_sound_speed_bad_spline(
    #     archive_dir,
    #     file_to_search,
    #     opt_problem,
    #     late_bad.gen,
    #     late_bad.indv_idx,
    # )
    # fig2, ax2 = plot_late_spline_against_result(data, late_spline, late_bad.gen)
