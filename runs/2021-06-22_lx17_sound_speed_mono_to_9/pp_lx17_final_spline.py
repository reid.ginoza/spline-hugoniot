# Copyright (C) 2021 Reid Ginoza
# All rights reserved.
"""Making the final spline plots."""
from pathlib import Path
import pickle
from typing import Optional, Union

import matplotlib

try:
    matplotlib.use("TkAgg")
except RecursionError:
    pass
from matplotlib import pyplot as plt
import numpy as np
import pandas as pd

from seos.splines.spline_eos import SplineEOS
from seos.optimizer.opt_plots import (
    ARA_BLUE,
    ARA_DARK_BLUE,
    ARA_BRIGHT_BLUE,
    ARA_ORANGE,
    ARA_GREEN,
    ARA_GREY,
    ARA_DARK_GREY,
)

from lx17_plots import plot_spline_with_sound_speed, add_data_to_us_up_ax
from main_lx17 import (
    get_data,
    get_overdriven_data,
    data_plot,
    result_spline_against_data,
)

Float = Union[float, np.float]

# messy, need to just have this in one place.
RHO_0 = 1.9


def get_opt_problem(results_dir: Path):
    pkl = (
        results_dir
        / "lx17_fixed_ref_upper_bound_sound_speed_opt_problem.pickle"
    )
    with pkl.open("rb") as f:
        return pickle.load(f)


def get_opt_result(results_dir: Path):
    pkl = (
        results_dir / "lx17_fixed_ref_upper_bound_sound_speed_opt_result.pickle"
    )
    with pkl.open("rb") as f:
        return pickle.load(f)


def make_result_spline(opt_problem, opt_result):
    result_coeffs = opt_problem.make_full_coeffs(opt_result.X)
    return opt_problem.base_spline.new_spline_from_coeffs(result_coeffs)


def add_knots(ax: plt.Axes, result_spline: SplineEOS):
    kn_v_rel = result_spline.get_private_data("t")
    kn_v_spec = kn_v_rel / result_spline.rho_0
    kn_p = result_spline(kn_v_rel)

    ax.scatter(kn_v_spec, kn_p, marker="+", color=ARA_ORANGE, label="Knots")
    return ax


def adjust_ax_for_jap(ax: plt.Axes):
    ax.tick_params(right=True, top=True)
    return ax


def change_volume_label_for_jap(ax: plt.Axes):
    ax.set_xlabel(r"Volume (cm$^3$/g)")
    return ax


if __name__ == "__main__":
    # Set up input and output
    out_dir = Path(__file__).parent / "pp_out"
    out_dir.mkdir(exist_ok=True)

    results_dir = (
        Path(__file__).parent
        / "results"
        / "2021-06-22_lx17_fixed_ref_upper_bound_sound_speed"
    )
    archive_dir = results_dir / "archive"

    # Get run stuff

    opt_problem = get_opt_problem(results_dir)
    res = get_opt_result(results_dir)

    result_spline = make_result_spline(opt_problem, res)

    data = get_data()

    od_data = get_overdriven_data()

    # Start plotting
    full_res_fig: plt.Figure
    full_res_ax: plt.Axes

    full_res_fig, full_res_ax = result_spline_against_data(
        result_spline, data, od_data, rmsd=res.F
    )
    adjust_ax_for_jap(full_res_ax)
    change_volume_label_for_jap(full_res_ax)
    full_res_fig.savefig(out_dir / "Optimization_Result_full.png")
    full_res_fig.savefig(out_dir / "Optimization_Result_full.eps")

    x_lims = full_res_ax.get_xlim()
    full_res_ax = add_knots(full_res_ax, result_spline)
    full_res_ax.set_xlim(x_lims)
    full_res_fig.savefig(out_dir / "Optimization_Result_full_knots.png")
    full_res_fig.savefig(out_dir / "Optimization_Result_full_knots.eps")

    z_full_res_fig: plt.Figure
    z_full_res_ax: plt.Axes
    z_full_res_fig, z_full_res_ax = result_spline_against_data(
        result_spline, data, od_data, rmsd=res.F, with_overdriven=False,
    )
    adjust_ax_for_jap(z_full_res_ax)
    change_volume_label_for_jap(z_full_res_ax)
    z_full_res_fig.savefig(out_dir / "Optimization_Result_full_zoom.png")
    z_full_res_fig.savefig(out_dir / "Optimization_Result_full_zoom.eps")

    x_lims = z_full_res_ax.get_xlim()
    y_lims = z_full_res_ax.get_ylim()
    z_full_res_ax = add_knots(z_full_res_ax, result_spline)
    z_full_res_ax.set_xlim(x_lims)
    z_full_res_ax.set_ylim(y_lims)
    z_full_res_fig.savefig(out_dir / "Optimization_Result_full_zoom_knots.png")
    z_full_res_fig.savefig(out_dir / "Optimization_Result_full_zoom_knots.eps")
