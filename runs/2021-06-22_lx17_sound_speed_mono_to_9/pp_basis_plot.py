# Copyright (C) 2021 Reid Ginoza
# All rights reserved.
"""Needs a docstring."""
from pathlib import Path
import pickle
from textwrap import dedent

import matplotlib

matplotlib.use("TkAgg")

import matplotlib.pyplot as plt


def get_opt_result(opt_result_pickle: Path):
    assert opt_result_pickle.is_file()

    with opt_result_pickle.open("rb") as f:
        opt_result = pickle.load(f)

    return opt_result


def get_opt_problem(opt_problem_pickle: Path):
    assert opt_problem_pickle.is_file()

    with opt_problem_pickle.open("rb") as f:
        return pickle.load(f)


def get_base_spline(opt_problem):
    return opt_problem.base_spline


if __name__ == "__main__":
    results_dir = Path(__file__).parent / "results"
    run_dir = results_dir / "2021-06-22_lx17_fixed_ref_upper_bound_sound_speed"
    gen_pickle_archive = run_dir / "archive"

    opt_result_pickle = (
        run_dir / "lx17_fixed_ref_upper_bound_sound_speed_opt_result.pickle"
    )
    opt_problem_pickle = (
        run_dir / "lx17_fixed_ref_upper_bound_sound_speed_opt_problem.pickle"
    )

    opt_result = get_opt_result(opt_result_pickle)
    opt_problem = get_opt_problem(opt_problem_pickle)
    base_spline = get_base_spline(opt_problem)

    full_res_coeffs = opt_problem.make_full_coeffs(opt_result.X)

    result_spline = base_spline.new_spline_from_coeffs(full_res_coeffs)
    knots = result_spline.get_private_data("t")
    coeffs = result_spline.get_private_data("c")
    print(
        dedent(
            f"""
        degree (k): {result_spline.get_private_data("k")}
        {len(knots)} knots (t): {knots}
        {len(coeffs)} coeffs (c): {coeffs}
        """
        )
    )

    fig: plt.Figure
    ax: plt.Axes
    fig, ax = result_spline.plot_basis(
        x_min=0.4, x_max=1.05, x_left=0.45, x_right=1.05, show_legend=False
    )
    ax.tick_params(top=True, right=True)
    fig.savefig(Path.cwd() / "basis_splines.png")
    fig.savefig(Path.cwd() / "basis_splines.eps", format="eps")

    fig.set_figwidth(2 * fig.get_figwidth())
    fig.savefig(Path.cwd() / "basis_splines_wide.png")
    fig.savefig(Path.cwd() / "basis_splines_wide.eps", format="eps")
