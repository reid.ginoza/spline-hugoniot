# Copyright (C) 2021 Reid Ginoza
# All rights reserved.
from collections.abc import Callable
from pathlib import Path
import pickle
from typing import Iterable, List, Optional, Tuple, Union

import matplotlib

matplotlib.use("TkAgg")

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from scipy.interpolate import InterpolatedUnivariateSpline
from scipy.optimize import root_scalar

from seos.optimizer.opt_plots import (
    ARA_BLUE,
    ARA_DARK_BLUE,
    ARA_BRIGHT_BLUE,
    ARA_ORANGE,
    ARA_GREEN,
    ARA_GREY,
    ARA_DARK_GREY,
)
from seos.splines.spline_eos import SplineEOS, RootSolveError

from plotting import (
    COLORS,
    add_data_to_ax,
)


def get_data():
    data = pd.read_csv(
        Path(__file__).parent / "LX-17_data_v3_only_data.csv", comment="#"
    )
    data = data.append(
        {"rho_0": 1.9, "v_spec": 1 / 1.9, "P": 0.0, "Label": "Reference Point"},
        ignore_index=True,
    )
    return data


def get_overdriven_data():
    data = pd.read_csv(
        Path(__file__).parent / "LX-17_overdriven_data_with_v3.csv", comment="#"
    )
    if not ("US" in data.columns and "UP" in data.columns):
        data["V(--)"] = data["v_spec"] * data["rho_0"]
        data["UP"] = np.sqrt(data["P"] * (1 - data["V(--)"]) / data["rho_0"])
        data["US"] = data["UP"] / (1 - data["V(--)"])
        data.to_csv(
            Path(__file__).parent / "LX-17_overdriven_data_with_v3.csv",
            index=False,
        )
    return data


def add_data_to_us_up_ax(
    ax: plt.Axes,
    data: pd.DataFrame,
    od_data: pd.DataFrame,
    with_overdriven: bool = True,
    with_reference: bool = False,
) -> plt.Axes:
    """Plots scatter data on ax in specific volume coordinates."""
    ax.scatter(
        data.loc[data["Label"] == "Jackson Gun", "UP"],
        data.loc[data["Label"] == "Jackson Gun", "US"],
        label="Jackson Gun",
        color=ARA_GREEN,
        marker="^",
    )

    ax.scatter(
        data.loc[data["Label"] == "Jackson Wedge", "UP"],
        data.loc[data["Label"] == "Jackson Wedge", "US"],
        label="Jackson Wedge",
        color=ARA_GREEN,
        marker="s",
    )

    ax.scatter(
        data.loc[data["Label"] == "Dallman", "UP"],
        data.loc[data["Label"] == "Dallman", "US"],
        label="Dallman " r"($\rho_0=1.913$)",
        color=ARA_BRIGHT_BLUE,
    )

    ax.scatter(
        data.loc[data["Label"] == "Gustavsen", "UP"],
        data.loc[data["Label"] == "Gustavsen", "US"],
        label="Gustavsen",
        color=ARA_DARK_BLUE,
    )

    if with_overdriven:
        ax.scatter(
            od_data.loc[od_data["Label"] == "Holmes", "UP"],
            od_data.loc[od_data["Label"] == "Holmes", "US"],
            label="Holmes (Overdriven)",
            color="aquamarine",
        )

    if with_reference:
        ax.scatter(
            0, 0, color="red", label="Reference Point " r"($\rho_0 = 1.9$)"
        )

    ax.set_xlabel(r"$u_p$ (mm/$\mu$s)")
    ax.set_ylabel(r"$U_s$ (mm/$\mu$s)")
    return ax


def get_others_spline_us_up(name):
    csv_dir = Path(__file__).parent / "with_relative_volumes"
    if name == "dallman":
        file = csv_dir / "v_rel_dallman_lx17.csv"
        rho_0 = 1.913
    elif name == "gustavsen":
        file = csv_dir / "v_rel_gustavsen_lx17.csv"
        rho_0 = 1.900
    elif name == "jackson":
        file = csv_dir / "v_rel_jackson_lx17.csv"
        rho_0 = 1.90
    elif name == "tarver":
        file = csv_dir / "v_rel_tarver_lx17.csv"
        rho_0 = 1.905
    else:
        raise ValueError("unrecognized Hugoniot")

    data = pd.read_csv(file).sort_values(by="UP(KM/S)")
    try:
        spline = SplineEOS(
            x=data["UP(KM/S)"],
            y=data["US(KM/S)"],
            ext="raise",
            rho_0=rho_0,
            gamma=1.0,
        )
    except Exception as e:
        print(name, data, sep="\n")
        raise e
    return spline


def get_spline_hugoniot_us_up() -> SplineEOS:
    results_dir = (
        Path(__file__).parent.parent
        / "results"
        / "2021-06-22_lx17_fixed_ref_upper_bound_sound_speed"
    )
    result_file = (
        results_dir / "lx17_fixed_ref_upper_bound_sound_speed_opt_result.pickle"
    )
    with result_file.open("rb") as f:
        opt_res = pickle.load(f)

    problem_file = (
        results_dir
        / "lx17_fixed_ref_upper_bound_sound_speed_opt_problem.pickle"
    )
    with problem_file.open("rb") as f:
        opt_problem = pickle.load(f)

    best_coeffs = opt_res.X
    all_coeffs = opt_problem.make_full_coeffs(best_coeffs)

    spline_hugoniot = opt_res.problem.base_spline.new_spline_from_coeffs(
        all_coeffs
    )

    assert spline_hugoniot.rho_0 == 1.9

    knot_locations = np.unique(spline_hugoniot._data[8])
    v_rel = np.concatenate(
        (
            [0.99 * knot_locations.min()],
            knot_locations,
            [1.01 * knot_locations.max()],
        )
    )
    v_rel = np.flip(v_rel[v_rel < 1.0])
    p = spline_hugoniot(v_rel)

    up = np.sqrt(p * (1 - v_rel) / spline_hugoniot.rho_0)
    us = up / (1 - v_rel)
    return SplineEOS(
        x=up, y=us, rho_0=spline_hugoniot.rho_0, gamma=1.0, ext="raise",
    )


def _evaluate_spline_at_up(
    up: np.ndarray, spline: InterpolatedUnivariateSpline,
) -> Tuple[np.ndarray, np.ndarray]:
    """Only put interpolated values into the arrays.

    Assumes the spline's extrapolate argument ext="raise".
    """
    new_up = []
    spline_us = []
    for up_ in up:
        try:
            us = spline(up_)
        except ValueError:
            pass
        else:
            new_up.append(up_)
            spline_us.append(us)
    return np.array(new_up), np.array(spline_us)


def all_models_plot_us_up(
    data: pd.DataFrame,
    od_data: pd.DataFrame,
    with_overdriven: bool = True,
    with_spline_hugo: bool = True,
    with_reference: bool = False,
):
    fig: plt.Figure
    ax: plt.Axes
    fig, ax = plt.subplots()

    ax = add_data_to_us_up_ax(
        ax,
        data,
        od_data,
        with_overdriven=with_overdriven,
        with_reference=with_reference,
    )

    dallman = get_others_spline_us_up("dallman")
    gustavsen = get_others_spline_us_up("gustavsen")
    jackson = get_others_spline_us_up("jackson")
    tarver = get_others_spline_us_up("tarver")
    spline_hugoniot = get_spline_hugoniot_us_up()

    if with_overdriven:
        all_data = pd.concat([data, od_data], ignore_index=True)
    else:
        all_data = data

    up = np.linspace(
        all_data["UP"].min() * 0.99, all_data["UP"].max() * 1.01, 10000
    )

    ax.plot(
        *_evaluate_spline_at_up(up, dallman),
        label="Dallman (Linear) Hugoniot",
        ls="--",
        color=COLORS["dallman"],
    )
    ax.plot(
        *_evaluate_spline_at_up(up, gustavsen),
        label="Gustavsen (Piecewise) Hugoniot",
        ls="--",
        color=COLORS["gustavsen"],
    )
    ax.plot(
        *_evaluate_spline_at_up(up, jackson),
        label="Jackson (Linear) Hugoniot",
        ls="--",
        color=COLORS["jackson"],
    )
    ax.plot(
        *_evaluate_spline_at_up(up, tarver),
        label="Tarver (JWL) Hugoniot",
        ls="--",
        color=COLORS["tarver"],
    )
    if with_spline_hugo:
        ax.plot(
            *_evaluate_spline_at_up(up, spline_hugoniot),
            label="Spline Hugoniot",
            ls="--",
            color=COLORS["result"],
        )
    ax.legend()
    ax.set_xlabel(r"Particle Velocity $u_p$ (mm/$\mu$s)")
    ax.set_ylabel(r"Shock Velocity $U_s$ (mm/$\mu$s)")

    if with_overdriven:
        title = "Spline Model Comparison\nHugoniot and Overdriven Data"
    else:
        title = "Spline Model Comparison\nHugoniot Data"
    ax.set_title(title)

    return fig, ax


def adjust_ax_for_jap(ax: plt.Axes) -> plt.Axes:
    # add tick marks to all axes
    ax.tick_params(right=True, top=True)
    return ax


if __name__ == "__main__":
    out_dir = Path(__file__).parent / "plots"
    out_dir.mkdir(exist_ok=True)

    data = get_data()
    od_data = get_overdriven_data()

    fig: plt.Figure
    ax: plt.Axes
    fig, ax = plt.subplots()
    ax = add_data_to_us_up_ax(
        ax, data, od_data, with_overdriven=True, with_reference=False
    )
    adjust_ax_for_jap(ax)
    ax.set_title("Hugoniot and Overdriven Data\n" r"$U_s$-$u_p$ Space")
    ax.legend()
    fig.savefig(out_dir / "hugo_od_us_up_data_only.png")
    fig.savefig(out_dir / "hugo_od_us_up_data_only.eps")

    fig2, ax2 = plt.subplots()
    ax2 = add_data_to_us_up_ax(
        ax2, data, od_data, with_overdriven=False, with_reference=False
    )
    adjust_ax_for_jap(ax2)
    ax2.set_title("Hugoniot Data\n" r"$U_s$-$u_p$ Space")
    ax2.legend()
    fig2.savefig(out_dir / "hugo_us_up_data_only.png")
    fig2.savefig(out_dir / "hugo_us_up_data_only.eps")

    fig3, ax3 = plt.subplots()
    ax3 = add_data_to_us_up_ax(
        ax3, data, od_data, with_overdriven=True, with_reference=True
    )
    adjust_ax_for_jap(ax3)
    ax3.set_title("Hugoniot and Overdriven Data\n" r"$U_s$-$u_p$ Space")
    ax3.legend()
    fig3.savefig(out_dir / "hugo_od_us_up_data_only_with_ref.png")
    fig3.savefig(out_dir / "hugo_od_us_up_data_only_with_ref.eps")

    fig4, ax4 = plt.subplots()
    ax4 = add_data_to_us_up_ax(
        ax4, data, od_data, with_overdriven=False, with_reference=True
    )
    adjust_ax_for_jap(ax4)
    ax4.set_title("Hugoniot Data\n" r"$U_s$-$u_p$ Space")
    ax4.legend()
    fig4.savefig(out_dir / "hugo_us_up_data_only_with_ref.png")
    fig4.savefig(out_dir / "hugo_us_up_data_only_with_ref.eps")

    fig5, ax5 = all_models_plot_us_up(
        data,
        od_data,
        with_overdriven=True,
        with_spline_hugo=True,
        with_reference=False,
    )
    adjust_ax_for_jap(ax5)
    fig5.set_figwidth(1.2 * fig5.get_figwidth())
    fig5.savefig(out_dir / "hugo_od_us_up_result.png")
    fig5.savefig(out_dir / "hugo_od_us_up_result.eps")

    fig6, ax6 = all_models_plot_us_up(
        data,
        od_data,
        with_overdriven=False,
        with_spline_hugo=True,
        with_reference=False,
    )
    adjust_ax_for_jap(ax6)
    fig6.set_figwidth(1.2 * fig6.get_figwidth())
    fig6.savefig(out_dir / "hugo_us_up_result.png")
    fig6.savefig(out_dir / "hugo_us_up_result.eps")
