# Copyright (C) 2021 Reid Ginoza
# All rights reserved.
"""Plots analytical versions of the other models.

This doesn't seem to work well. There might be floating point erorrs.
"""
from collections.abc import Callable, Iterable
from pathlib import Path
import pickle
from typing import Optional, Union
from warnings import warn

import matplotlib

matplotlib.use("TkAgg")
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from scipy.interpolate import InterpolatedUnivariateSpline
from scipy.optimize import root_scalar

from seos.optimizer.opt_plots import (
    ARA_BLUE,
    ARA_DARK_BLUE,
    ARA_BRIGHT_BLUE,
    ARA_ORANGE,
    ARA_GREEN,
    ARA_GREY,
    ARA_DARK_GREY,
)
from seos.splines.spline_eos import SplineEOS

from plotting import COLORS, add_data_to_ax


def get_spline_hugoniot() -> SplineEOS:
    results_dir = (
        Path(__file__).parent.parent
        / "results"
        / "2021-06-22_lx17_fixed_ref_upper_bound_sound_speed"
    )
    result_file = (
        results_dir / "lx17_fixed_ref_upper_bound_sound_speed_opt_result.pickle"
    )
    with result_file.open("rb") as f:
        opt_res = pickle.load(f)

    problem_file = (
        results_dir
        / "lx17_fixed_ref_upper_bound_sound_speed_opt_problem.pickle"
    )
    with problem_file.open("rb") as f:
        opt_problem = pickle.load(f)

    best_coeffs = opt_res.X
    all_coeffs = opt_problem.make_full_coeffs(best_coeffs)

    return opt_res.problem.base_spline.new_spline_from_coeffs(all_coeffs)


def tarver(v_rel) -> float:
    A = 77800
    B = -5.031
    R1 = 11.3
    R2 = 1.13
    w = 0.8938

    f1 = (1 - w / (R1 * v_rel)) * A * np.exp(-R1 * v_rel)
    f2 = (1 - w / (R2 * v_rel)) * B * np.exp(-R2 * v_rel)
    num = f1 + f2
    denom = 1 - (w * (1 - v_rel) / (2 * v_rel))
    return num / denom


def up_from_v_rel_Us(v_rel: float, Us: Callable):
    root_res = root_scalar(_to_solve_v_rel_Us, args=(v_rel, Us), x0=0.5, x1=1.0)
    if not root_res.converged:
        warn(
            "Did not find solution for up from v_rel. Will attempt to continue."
        )
    return root_res.root


def _to_solve_v_rel_Us(up, v_rel: float, Us: Callable):
    return up / Us(up) - 1 + v_rel


def _gus_Us(up):
    if up <= 0.82:
        return 1.9 + 3 * up
    else:
        return 2.9 + 1.78 * up


def get_gustavsen() -> Callable:
    return make_p_from_m_g_v_rel(_gus_Us)


def _jackson_Us(up):
    return 2.33 + 2.32 * up


def get_jackson() -> Callable:
    return make_p_from_m_g_v_rel(_jackson_Us)


def _dallman_Us(up):
    return 1.05 + 3.65 * up


def get_dallman() -> Callable:
    return make_p_from_m_g_v_rel(_dallman_Us, rho_0=1.913)


def make_p_from_m_g_v_rel(Us: Callable, rho_0: float = 1.9) -> Callable:
    def hugoniot(v_rel: Union[float, Iterable]) -> Union[float, np.array]:
        if isinstance(v_rel, Iterable):
            return np.fromiter((hugoniot(v) for v in v_rel), dtype=float)
        up = up_from_v_rel_Us(v_rel, Us)
        return rho_0 * Us(up) * up

    return hugoniot


def get_data():
    data = pd.read_csv(
        Path(__file__).parent / "LX-17_data_v3_only_data.csv", comment="#"
    )
    data = data.append(
        {"rho_0": 1.9, "v_spec": 1 / 1.9, "P": 0.0, "Label": "Reference Point"},
        ignore_index=True,
    )
    return data


def get_overdriven_data():
    data = pd.read_csv(
        Path(__file__).parent / "LX-17_overdriven_data_with_v3.csv", comment="#"
    )
    return data


def quick_plot(hugo: Callable, use_data=True, ax: Optional[plt.Axes] = None):
    fig: plt.Figure
    ax: plt.Axes

    if ax is None:
        fig, ax = plt.subplots()
    else:
        fig = ax.get_figure()

    if use_data:
        add_data_to_ax(
            ax, get_data(), get_overdriven_data(), with_overdriven=True
        )

    rel_v = np.linspace(0.49, 1.0)
    p = hugo(rel_v)
    if use_data:
        v = rel_v / 1.9
    else:
        v = rel_v

    ax.plot(v, p)
    ax.legend()
    ax.set_ylabel("Pressure (GPa)")

    if use_data:
        ax.set_xlabel("Volume (cc/g)")
    else:
        ax.set_xlabel("Relative Volume (--)")

    return fig, ax


if __name__ == "__main__":
    spline_hugo = get_spline_hugoniot()

    out_dir = Path(__file__).parent / "test_fig_dir"
    out_dir.mkdir(exist_ok=True)

    fig, ax = quick_plot(spline_hugo)
    fig.savefig(out_dir / "spline_hugo.png")

    fig, ax = quick_plot(tarver)
    gustavsen = get_gustavsen()
    fig, ax = quick_plot(gustavsen)
    ax.set_title("Gustavsen")
    ax.set_ylim(-5.0, 140)

    jackson = get_jackson()
    fig, ax = quick_plot(jackson)
    ax.set_title("Jackson")
    ax.set_ylim(-5.0, 140)

    dallman = get_dallman()
    fig, ax = quick_plot(dallman)
    ax.set_title("Dallman")
    ax.set_ylim(-5.0, 140)
