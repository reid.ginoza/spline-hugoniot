# Copyright (C) 2021 Reid Ginoza
# All rights reserved.
"""Comparing to Aslam's complete EOS for PBX 9502."""
from pathlib import Path
import pickle
from typing import List, Optional, Tuple, Union

import matplotlib

try:
    matplotlib.use("TkAgg")
except RecursionError:
    pass
from matplotlib import pyplot as plt
import numpy as np
import pandas as pd
import scipy.interpolate as si

from seos.splines.spline_eos import SplineEOS
from seos.optimizer.opt_plots import (
    ARA_BLUE,
    ARA_DARK_BLUE,
    ARA_BRIGHT_BLUE,
    ARA_ORANGE,
    ARA_GREEN,
    ARA_GREY,
    ARA_DARK_GREY,
)

from lx17_plots import plot_spline_with_sound_speed, add_data_to_us_up_ax
from main_lx17 import (
    get_data,
    get_overdriven_data,
    data_plot,
    result_spline_against_data,
)
from pp_lx17_final_spline import (
get_opt_problem,
get_opt_result,
make_result_spline,
)

Float = Union[float, np.float]

# messy, need to just have this in one place.
RHO_0 = 1.9

ASLAM_COLOR = "mediumorchid"

ASLAM_DATA = Path(__file__).parent / "Reactants.dat"
assert ASLAM_DATA.is_file()


def read_aslam_data() -> pd.DataFrame:
    cols = _aslam_col_names()
    df = pd.read_csv(ASLAM_DATA, sep="\s+", skiprows=1, names=cols)
    df["vol"] = 1 / df["rho_s"]
    return df


def _aslam_col_names(get_units: bool = False) -> Union[List[str], Tuple[List[str], List[str]]]:
    with ASLAM_DATA.open() as f:
        cols_line = f.readline()

    # split on two spaces to group units with name, but has empty spaces
    cols = [c.strip() for c in cols_line.strip().split("  ") if c]
    col_vars, units = _separate_var_and_units(cols)

    if get_units:
        return col_vars, units
    else:
        return col_vars


def _separate_var_and_units(cols: List[str]):
    col_vars = []
    units = []
    for col in cols:
        v, u = col.split(" ")
        assert isinstance(v, str) and isinstance(u, str)
        col_vars.append(v)
        units.append(u)
    return col_vars, units


def _parse_ax_arg(ax: Optional[plt.Axes]) -> Tuple[plt.Figure, plt.Axes]:
    if ax is None:
        return plt.subplots()
    else:
        return ax.get_figure(), ax


def plot_aslam_hugoniot(df: pd.DataFrame, ax: Optional[plt.Axes] = None) -> Tuple[plt.Figure, plt.Axes]:
    fig, ax = _parse_ax_arg(ax)

    ax.plot(df["vol"], df["p_s"], label="Aslam PBX 9502", color=ASLAM_COLOR)
    ax.set_xlabel(r"Volume (cm$^3$/g)")
    ax.set_ylabel("Pressure (GPa)")
    adjust_ax_for_jap(ax)
    return fig, ax


def plot_aslam_sound_speed(df: pd.DataFrame, ax: Optional[plt.Axes] = None) -> Tuple[plt.Figure, plt.Axes]:
    fig, ax = _parse_ax_arg(ax)

    v_rel = df["vol"] * 1.89
    ax.plot(v_rel, df["ss"], label="Aslam PBX 9502", color=ASLAM_COLOR)
    ax.set_xlabel(r"Relative Volume (--)")
    ax.set_ylabel(r"Sound Speed (mm/$\mu$s)")
    adjust_ax_for_jap(ax)
    return fig, ax


def plot_aslam_derivatives(df: pd.DataFrame) -> Tuple[plt.Figure, Tuple[plt.Axes, plt.Axes, plt.Axes]]:
    fig: plt.Figure
    ax0: plt.Axes
    ax1: plt.Axes
    ax2: plt.Axes
    fig, (ax0, ax1, ax2) = plt.subplots(nrows=3, sharex="all")

    v = df.iloc[::-1, df.columns.get_loc("vol")]
    p_spline = si.InterpolatedUnivariateSpline(v, df.iloc[::-1, df.columns.get_loc("p_s")])

    ax0.plot(v, p_spline(v), color=ASLAM_COLOR)
    ax0.set_ylabel(r"$P$ (GPa)")

    ax1.plot(v, p_spline(v, nu=1), color=ASLAM_COLOR)
    ax1.set_ylabel(r"$\frac{\mathrm{d} P}{\mathrm{d}v}$")

    ax2.plot(v, p_spline(v, nu=2), color=ASLAM_COLOR)
    ax2.set_ylabel(r"$\frac{\mathrm{d}^2 P}{\mathrm{d}v^2}$")

    ax2.set_xlabel(r"Volume (cm$^3$/g)")
    return fig, (ax0, ax1, ax2)


def adjust_ax_for_jap(ax: plt.Axes):
    ax.tick_params(right=True, top=True)
    return ax



if __name__ == "__main__":
    print("Plotting Aslam only")
    cols = _aslam_col_names()
    df = read_aslam_data()

    fig: plt.Figure
    ax_top: plt.Axes
    ax_bot: plt.Axes
    fig, (ax_top, ax_bot) = plt.subplots(nrows=2)

    plot_aslam_hugoniot(df, ax_top)
    plot_aslam_sound_speed(df, ax_bot)
    fig.tight_layout()

    print("Plotting Aslam derivatives")
    der_fig, der_axs = plot_aslam_derivatives(df)

    print("Plotting comparison")

    # Set up input and output
    out_dir = Path(__file__).parent / "pp_compare_aslam"
    out_dir.mkdir(exist_ok=True)

    # Optimization info
    results_dir = (
            Path(__file__).parent
            / "results"
            / "2021-06-22_lx17_fixed_ref_upper_bound_sound_speed"
    )
    archive_dir = results_dir / "archive"

    # Get run stuff

    opt_problem = get_opt_problem(results_dir)
    res = get_opt_result(results_dir)

    result_spline = make_result_spline(opt_problem, res)

    data = get_data()

    od_data = get_overdriven_data()

    # Start plotting
    full_res_fig: plt.Figure
    full_res_ax: plt.Axes

    full_res_fig, full_res_ax = result_spline_against_data(
        result_spline, data, od_data, rmsd=res.F
    )
    plot_aslam_hugoniot(df, full_res_ax)
    full_res_ax.set_title("Spline Hugoniot Compared to Aslam's Hugoniot\n"
                          "with Overdriven Data")
    full_res_ax.legend()
    full_res_fig.savefig(out_dir / "Aslam_compare_full.png")
    full_res_fig.savefig(out_dir / "Aslam_compare_full.eps")

    z_full_res_fig: plt.Figure
    z_full_res_ax: plt.Axes
    z_full_res_fig, z_full_res_ax = result_spline_against_data(
        result_spline, data, od_data, rmsd=res.F, with_overdriven=False,
    )
    x_lim = z_full_res_ax.get_xlim()
    y_lim = z_full_res_ax.get_ylim()
    plot_aslam_hugoniot(df, z_full_res_ax)
    z_full_res_ax.set_xlim(x_lim)
    z_full_res_ax.set_ylim(y_lim)
    z_full_res_ax.legend()
    z_full_res_ax.set_title("Spline Hugoniot Compared to Aslam's Hugoniot")
    z_full_res_fig.savefig(out_dir / "Aslam_compare_zoom.png")
    z_full_res_fig.savefig(out_dir / "Aslam_compare_zoom.eps")
