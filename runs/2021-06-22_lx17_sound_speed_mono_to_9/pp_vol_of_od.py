# Copyright (C) 2021 Reid Ginoza
# All rights reserved.
"""Finds the volumes for the overdriven pressures."""
from pathlib import Path
import pickle
from typing import Optional, Union

import matplotlib

matplotlib.use("TkAgg")
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from scipy.optimize import root_scalar, RootResults

from seos.splines.spline_eos import SplineEOS
from seos.optimizer.opt_plots import (
    ARA_BLUE,
    ARA_DARK_BLUE,
    ARA_BRIGHT_BLUE,
    ARA_ORANGE,
    ARA_GREEN,
    ARA_GREY,
    ARA_DARK_GREY,
)

from lx17_plots import plot_spline_with_sound_speed, add_data_to_us_up_ax
from main_lx17 import (
    get_data,
    get_overdriven_data,
    data_plot,
    result_spline_against_data,
)


def get_opt_problem(results_dir: Path):
    pkl = (
        results_dir
        / "lx17_fixed_ref_upper_bound_sound_speed_opt_problem.pickle"
    )
    with pkl.open("rb") as f:
        return pickle.load(f)


def get_opt_result(results_dir: Path):
    pkl = (
        results_dir / "lx17_fixed_ref_upper_bound_sound_speed_opt_result.pickle"
    )
    with pkl.open("rb") as f:
        return pickle.load(f)


def make_result_spline(opt_problem, opt_result):
    result_coeffs = opt_problem.make_full_coeffs(opt_result.X)
    return opt_problem.base_spline.new_spline_from_coeffs(result_coeffs)


def find_v_rel_for_p(p: float, result_spline: SplineEOS, x0=0.49, x1=0.64):
    def to_find_root(v_rel: float):
        return result_spline(v_rel) - p

    root_res: RootResults = root_scalar(to_find_root, x0=x0, x1=x1)
    if not root_res.converged:
        print("WARNING! Root finder did not converge!")
    return root_res.root


if __name__ == "__main__":
    # Get run stuff
    results_dir = (
        Path(__file__).parent
        / "results"
        / "2021-06-22_lx17_fixed_ref_upper_bound_sound_speed"
    )

    opt_problem = get_opt_problem(results_dir)
    res = get_opt_result(results_dir)

    result_spline = make_result_spline(opt_problem, res)

    data = get_data()

    od_data: pd.DataFrame = get_overdriven_data()
    for _, row in od_data.iterrows():
        print(f"Pressure: {row.P}")
        v_rel = find_v_rel_for_p(row.P, result_spline)
        print(f"v_rel: {v_rel}")
        print(f"v_spec: {v_rel / 1.9}")
