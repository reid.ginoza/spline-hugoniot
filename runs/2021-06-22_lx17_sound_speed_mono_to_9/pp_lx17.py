# Copyright (C) 2021 Reid Ginoza
# All rights reserved.
"""Code run after the optimization has completed. Mostly plots."""
from pathlib import Path
from typing import Optional, Union

import matplotlib

try:
    matplotlib.use("TkAgg")
except RecursionError:
    pass
from matplotlib import pyplot as plt
import numpy as np
import pandas as pd

from seos.splines.spline_eos import SplineEOS
from seos.optimizer.opt_plots import (
    ARA_BLUE,
    ARA_DARK_BLUE,
    ARA_BRIGHT_BLUE,
    ARA_ORANGE,
    ARA_GREEN,
    ARA_GREY,
    ARA_DARK_GREY,
)

from lx17_plots import plot_spline_with_sound_speed, add_data_to_us_up_ax
from main_lx17 import get_data, get_overdriven_data, data_plot

Float = Union[float, np.float]

# messy, need to just have this in one place.
RHO_0 = 1.9


def p_v_to_us_up(p, v_rel, rho_0):
    up = np.sqrt(p * (1 - v_rel) / rho_0)
    us = up / (1 - v_rel)
    return us, up


def calc_us_up():
    """Run only once since original did not have Us, Up."""
    file_od = Path(__file__).parent / "LX-17_overdriven_data_with_v3.csv"
    od_data = pd.read_csv(file_od, comment="#")
    v_rel = od_data["rho_0"] * od_data["v_spec"]
    od_data["US"], od_data["UP"] = p_v_to_us_up(
        od_data["P"], v_rel, od_data["rho_0"]
    )
    od_data.to_csv(file_od, index=False)


def make_pp_out_dir(run_name: Optional[str] = None) -> Path:
    out_dir = Path(__file__).parent / run_name
    out_dir.mkdir(exist_ok=True, parents=True)
    return out_dir


def all_together_plot(data, od_data):
    fig: plt.Figure
    ax00: plt.Axes
    ax01: plt.Axes
    ax10: plt.Axes
    ax11: plt.Axes
    fig, ((ax00, ax01), (ax10, ax11)) = plt.subplots(nrows=2, ncols=2)
    data_plot(data, od_data, with_overdriven=False, ax=ax00)
    ax00.set_xlabel(r"Volume (cm$^3$/g)")
    edit_plots(ax00, "Hugoniot Data")

    data_plot(data, od_data, with_overdriven=True, ax=ax10)
    ax10.set_xlabel(r"Volume (cm$^3$/g)")
    handles, labels = ax10.get_legend_handles_labels()
    edit_plots(ax10, "Hugoniot and Overdriven Data")

    add_data_to_us_up_ax(ax01, data, od_data, with_overdriven=False)
    edit_plots(ax01, "Hugoniot Data\n$U_S$-$u_p$ Space")

    add_data_to_us_up_ax(ax11, data, od_data, with_overdriven=True)
    edit_plots(
        ax11, "Hugoniot and Overdriven Data\n$U_S$-$u_p$ Space"
    )

    # legend on right
    # ax01.legend(handles, labels, bbox_to_anchor=(1.05, 1), loc="upper left")
    # fig.set_figwidth(1.5 * fig.get_figwidth())
    # fig.set_figheight(1.2 * fig.get_figheight())

    # legend below
    # fig.legend(handles, labels, loc="upper center", bbox_to_anchor=(0.5, 0),
    #            bbox_transform=fig.transFigure)
    # fig.subplots_adjust(bottom=0.2)

    scale_factor = 1.05  # determined by trial and error
    fig.set_figwidth(1.2 * scale_factor * fig.get_figwidth())
    fig.set_figheight(1.3 * scale_factor * fig.get_figheight())

    # plt.legend(handles, labels, loc='upper center', bbox_to_anchor=(0, -0.5, 1, 0.5),
    #            bbox_transform=plt.gcf().transFigure)
    fig.legend(handles, labels, loc='lower center', bbox_to_anchor=(0, 0, 1, .2),
               bbox_transform=fig.transFigure, mode="expand", ncol=3)
    fig.tight_layout()
    fig.subplots_adjust(bottom=0.2)
    return fig, ((ax00, ax01), (ax10, ax11))


def edit_plots(ax: plt.Axes, title:str):
    ax.get_legend().remove()
    ax.set_title(title)
    ax.tick_params(right=True, top=True)


if __name__ == "__main__":
    out_dir = make_pp_out_dir("post_processing")

    data = get_data()
    od_data = get_overdriven_data()

    d_fig, d_ax = data_plot(data, od_data)
    d_ax.set_title("LX-17 Hugoniot and Overdriven Data")
    d_fig.savefig(out_dir / "LX-17_data_only_hugo_od.png")
    d_fig.savefig(out_dir / "LX-17_data_only_hugo_od.eps")
    z_d_fig, z_d_ax = data_plot(data, od_data, with_overdriven=False)
    z_d_ax.set_title("LX-17 Hugoniot Data")
    z_d_fig.savefig(out_dir / "LX-17_data_only_hugo.png")
    z_d_fig.savefig(out_dir / "LX-17_data_only_hugo.eps")

    fig: plt.Figure
    ax: plt.Axes
    fig, ax = plt.subplots()

    ax = add_data_to_us_up_ax(ax, data, od_data, with_overdriven=True)
    ax.set_title("Hugoniot and Overdriven Data\n" r"$U_s$-$u_p$ Space")
    fig.savefig(out_dir / "LX-17_data_us_up_with_od.png")
    fig.savefig(out_dir / "LX-17_data_us_up_with_od.eps")

    fig: plt.Figure
    ax: plt.Axes
    fig, ax = plt.subplots()

    ax = add_data_to_us_up_ax(ax, data, od_data, with_overdriven=False)
    ax.set_title("Hugoniot Data\n" r"$U_s$-$u_p$ Space")
    fig.savefig(out_dir / "LX-17_data_us_up.png")
    fig.savefig(out_dir / "LX-17_data_us_up.eps")

    fig, ((ax00, ax01), (ax10, ax11)) = all_together_plot(data, od_data)
    fig.savefig(out_dir / "LX-17_all_data.png")
    fig.savefig(out_dir / "LX-17_all_data.eps")
