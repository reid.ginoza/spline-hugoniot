# Copyright (C) 2021 Reid Ginoza
# All rights reserved.
from pathlib import Path
import pickle

from matplotlib import pyplot as plt
import pandas as pd

ARA_BLUE = "#002E6C"
ARA_DARK_BLUE = "#00204B"
ARA_BRIGHT_BLUE = "#0092CA"
ARA_ORANGE = "#F05532"
ARA_GREEN = "#D2DD3F"
ARA_GREY = "#DEDEDE"
ARA_DARK_GREY = "#666666"


def get_prob_1():
    hugo = pd.read_csv(Path(__file__).parent / "files" / "lx17_jwl_hugo.csv")
    hugo = hugo.append({"V(--)": 1, "P(GPA)": 0,}, ignore_index=True,)
    return hugo


def get_prob_2():
    with (
        Path(__file__).parent / "files" / "archive" / "prob_2_data.pickle"
    ).open("rb") as f:
        prob_2 = pickle.load(f)
    return prob_2


def plot_prob(df: pd.DataFrame, title=""):
    fig: plt.Figure
    ax: plt.Axes

    fig, ax = plt.subplots()
    ax.scatter(df["V(--)"], df["P(GPA)"], color=ARA_BLUE)
    ax.set_xlabel(r"Relative Volume $\nu$")
    ax.set_ylabel(r"Pressure $P$ (GPa)")
    if title:
        ax.set_title(title)
    return fig, ax


prob_1 = get_prob_1()
prob_2 = get_prob_2()

fig1, ax1 = plot_prob(prob_1, "Artificial Experimental Data")
fig2, ax2 = plot_prob(prob_2, "Artificial, Noisy Experimental Data")
