# Copyright (C) 2021 Reid Ginoza
# All rights reserved.
from pathlib import Path
import pickle

from matplotlib import pyplot as plt
import numpy as np
import pandas as pd

from seos.splines.default_spline import tarver_jwl_spline

ARA_BLUE = "#002E6C"
ARA_DARK_BLUE = "#00204B"
ARA_BRIGHT_BLUE = "#0092CA"
ARA_ORANGE = "#F05532"
ARA_GREEN = "#D2DD3F"
ARA_GREY = "#DEDEDE"
ARA_DARK_GREY = "#666666"


def get_prob_1():
    hugo = pd.read_csv(Path(__file__).parent / "files" / "lx17_jwl_hugo.csv")
    hugo = hugo.append({"V(--)": 1, "P(GPA)": 0,}, ignore_index=True,)
    return hugo


spline = tarver_jwl_spline()
df = get_prob_1()

fig, ax = spline.plot_basis(0.575, 1.025)
ax.get_legend().remove()
ax.scatter(
    spline.get_knots(), np.zeros_like(spline.get_knots()), color=ARA_BLUE
)
