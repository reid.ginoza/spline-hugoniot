# Copyright (C) 2021 Reid Ginoza
# All rights reserved.
from pathlib import Path
import pickle

from matplotlib import pyplot as plt
import numpy as np
import pandas as pd

ARA_BLUE = "#002E6C"
ARA_DARK_BLUE = "#00204B"
ARA_BRIGHT_BLUE = "#0092CA"
ARA_ORANGE = "#F05532"
ARA_GREEN = "#D2DD3F"
ARA_GREY = "#DEDEDE"
ARA_DARK_GREY = "#666666"


def get_prob_1():
    hugo = pd.read_csv(Path(__file__).parent / "files" / "lx17_jwl_hugo.csv")
    return hugo


def get_prob_2():
    with (
        Path(__file__).parent / "files" / "archive" / "prob_2_data.pickle"
    ).open("rb") as f:
        prob_2 = pickle.load(f)
    return prob_2


def plot_prob(df: pd.DataFrame, title=""):
    up = df["UP(KM/S)"]
    us = df["US(KM/S)"]
    p = np.polyfit(up, us, deg=2)
    up_poly = np.linspace(up.min(), up.max())
    us_poly = np.polyval(p, up_poly)

    fig: plt.Figure
    ax: plt.Axes

    fig, ax = plt.subplots()
    ax.scatter(up, us, color=ARA_BLUE, label="Data")
    ax.plot(up_poly, us_poly, color=ARA_DARK_GREY, label="Quad. Fit")
    ax.legend()
    ax.set_xlabel(r"Particle velocity $u_p$ (km/s)")
    ax.set_ylabel(r"Shock velocity $U_s$ (km/s)")
    if title:
        ax.set_title(title)
    return fig, ax


prob_1 = get_prob_1()
prob_2 = get_prob_2()

fig1, ax1 = plot_prob(prob_1, "Artificial Experimental Data")
fig2, ax2 = plot_prob(prob_2, "Artificial, Noisy Experimental Data")
