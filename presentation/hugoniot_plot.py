# Copyright (C) 2021 Reid Ginoza
# All rights reserved.
from pathlib import Path

from matplotlib import pyplot as plt
import pandas as pd

ARA_BLUE = "#002E6C"
ARA_DARK_BLUE = "#00204B"
ARA_BRIGHT_BLUE = "#0092CA"
ARA_ORANGE = "#F05532"
ARA_GREEN = "#D2DD3F"
ARA_GREY = "#DEDEDE"
ARA_DARK_GREY = "#666666"


hugo = pd.read_csv(Path(__file__).parent / "files" / "lx17_jwl_hugo.csv")
hugo = hugo.append({"V(--)": 1, "P(GPA)": 0,}, ignore_index=True,)

v = hugo["V(--)"]
p = hugo["P(GPA)"]

fig: plt.Figure
ax: plt.Axes

fig, ax = plt.subplots()
ax.plot(v, p, color=ARA_BLUE, label="Hugoniot")
ax.scatter(1, 0, color=ARA_BRIGHT_BLUE, label="Reference Point")
ax.set_xlabel(r"Relative Volume $\nu$")
ax.set_ylabel(r"Pressure $P$ (GPa)")
ax.legend()
ax.set_title(r"Hugoniot in $P$-$\nu$ Space")
