# Copyright (C) 2021 Reid Ginoza
# All rights reserved.
from pathlib import Path
import pickle

from matplotlib import pyplot as plt
import pandas as pd


def get_prob_2():
    with (
        Path(__file__).parent / "files" / "archive" / "prob_2_data.pickle"
    ).open("rb") as f:
        prob_2 = pickle.load(f)
    return prob_2


prob_2: pd.DataFrame = get_prob_2()

prob_2.to_csv(
    Path(__file__).parent / "files" / "archive" / "prob_2.csv", index=False
)
