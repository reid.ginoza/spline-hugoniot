# Copyright (C) 2021 Reid Ginoza
# All rights reserved.
"""Logger to be used in this project."""
import logging
from os import PathLike
from typing import Optional, Union


def adjust_other_loggers():
    logging.getLogger("matplotlib").setLevel(logging.WARNING)
    logging.getLogger("pandas").setLevel(logging.WARNING)


def make_logger(
    name: str,
    level: Union[int, str] = logging.DEBUG,
    file_name: Optional[Union[str, PathLike]] = None,
) -> logging.Logger:
    logger = logging.getLogger(name)
    logger.setLevel(level)

    formatter = logging.Formatter(
        fmt="%(asctime)s::%(name)s::%(levelname)s:: %(message)s",
        datefmt="%y-%m-%d-%H:%M:%S",
    )

    s_handler = logging.StreamHandler()
    s_handler.setLevel(level)
    s_handler.setFormatter(formatter)

    logger.addHandler(s_handler)

    if file_name:
        f_handler = logging.FileHandler(file_name)
        f_handler.setLevel(level)
        f_handler.setFormatter(formatter)
        logger.addHandler(f_handler)

    adjust_other_loggers()

    return logger
