import os
import sys
from typing import Union


class PrintToFile:
    """Allows all prints, stdout and stderr to be saved to a file and stdout.

    # source: https://stackoverflow.com/q/616645

    Usage:
    with PrintToFile('My_best_girlie_by_my.side'):
        print("we'd sing sing sing")

    Log=PrintToFile('Sleeps_all.night')
    print('works all day')
    Log.close()
    """

    def __init__(self, filename: Union[str, os.PathLike], mode="a", buff=1):
        self.stdout = sys.stdout
        self.file = open(filename, mode, buff)
        sys.stdout = self

        self.stderr = sys.stderr
        sys.stderr = self

    def __del__(self):
        self.close()

    def __enter__(self):
        pass

    def __exit__(self, *args):
        self.close()

    def write(self, message):
        self.stdout.write(message)
        self.file.write(message)

    def flush(self):
        self.stdout.flush()
        self.file.flush()
        os.fsync(self.file.fileno())

    def close(self):
        if self.stdout is not None:
            sys.stdout = self.stdout
            self.stdout = None

        if self.stderr is not None:
            sys.stderr = self.stderr
            self.stderr = None

        if self.file is not None:
            self.file.close()
            self.file = None
