# Copyright (C) 2021 Reid Ginoza
# All rights reserved.
"""Multipliers for unit conversions."""
# Mass
kg_to_g = 1e3

# Pressure
GPa_to_atm = 9869.23

# Energy
J_to_kJ = 1e-3
MJ_to_kJ = 1e3

cal_to_J = 4.184
J_to_cal = 1 / cal_to_J

cal_to_kJ = cal_to_J * J_to_kJ
kJ_to_cal = 1 / cal_to_kJ
