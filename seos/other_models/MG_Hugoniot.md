# Hugoniot

$U_s = C_0 + S_1 u_p$

# Conservation Laws

* $\mu = 1 - \frac{\rho_0}{\rho} = 1 - \frac{v}{v_0} = \frac{u_p}{U_S}$

* $P = \rho_0 U_S u_p $

* $E = (P + P_0 \frac{\mu}{2\rho_0})$

# Given $\nu$, find $P$

## Find $u_p$

$$
1-\nu = \frac{u_p}{U_S}\\
1 - \nu = \frac{u_p}{C_0 + S_1 u_p}\\
(C_0 + S_1 u_p) - \nu(C_0 + S_1 u_p)= u_p\\
(S_1 - \nu S_1 - 1)u_p = -C_0 + \nu C_0\\
u_p = \frac{(1-\nu)C_0}{1-(1-\nu)S_1}
$$

## Find $U_S$

Use Hugoniot: $U_s = C_0 + S_1 u_p$

## Find $P$

$P = \rho_0 U_S u_p$

## $P=P(\nu)$ as a function of $\nu$

$$
P = \rho_0 U_S u_p\\[10pt]
P = \rho_0 (C_0 + S_1 u_p) u_p\\[10pt]
P = \rho_0 \left[C_0 + S_1 \left(\frac{(1-\nu)C_0}{1-(1-\nu)S_1} \right)
\right] \left(\frac{(1-\nu)C_0}{1-(1-\nu)S_1} \right)\\[10pt]
P = -\frac{C_0^2*\rho_0*(\nu - 1)}{(S_1*(\nu - 1) + 1)^2}\\[10pt]
P = \frac{C_0^2*\rho_0*(1-\nu)}{(1 - S_1(1-\nu))^2}\\
$$

## Derivatives

$$
\frac{dP}{d\nu} = 
-\frac{
C_{0}^{2} \rho_{0} \left(S_{1} \left(1 - \nu\right) + 1\right)
}{
\left(1-S_{1} \left(1 - \nu \right)\right)^{3}
}
$$

$$
\frac{d^2 P}{d \nu^2} =
\frac{
2 C_{0}^{2} S_{1} \rho_{0} \left(2 + S_{1} \left(1 - \nu\right)\right)
}{
    \left(1 - S_{1} \left(1- \nu\right)\right)^{4}
}
$$
