# Copyright (C) 2021 Reid Ginoza
# All rights reserved.
"""Class for linear mie-gruneisen models with pymoo optimizer.

CURRENTLY INCOMPLETE!
"""
from pathlib import Path
import pickle
from typing import Collection, List, Optional, Union
from warnings import warn

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from scipy.optimize import root_scalar

from seos.utilities.seos_logger import make_logger

logger = make_logger(__name__, "INFO")


# TODO(RG): Move these to a general model module
class ReferenceStateMissing(Exception):
    """Raised when there is an issue with the SplineEOS reference state."""

    pass


class RootSolveError(Exception):
    """Raised when the root_scalar function does not converge."""

    pass


class MGLinear:
    """EOS of the form: Us = c0 + s1 * up.

    All units are in mm/us.
    Only the Hugoniot is implemented.
    """

    def __init__(self, x: np.array, rho_0: float, gamma: float = 1.0):
        """Inits MGLinear instance.

        x: np.array
            x[0] is the c0 parameter of a linear Mie-Gruneisen
            x[1] is the s1 parameter of the linear Mie-Gruneisen
            An np.array because that is how it will be passed from the
            optimization problem.
        """
        assert len(x) == 2
        self.n_var = 2
        self.name = "Linear Mie-Gruneisen"
        self.c0 = x[0]
        self.s1 = x[1]
        self.rho_0 = rho_0
        self.gamma = gamma
        # TODO(RG): figure these out
        self.default_bounds = (
            (0.1, 5.0),
            (0.1, 5.0),
        )

    def __call__(self, v_rel):
        """Evaluates pressure from relative volume along the Hugoniot."""
        up = self.up_from_v_rel(v_rel)
        Us = self.Us_from_up(up)
        P = self.rho_0 * Us * up
        return P

    def up_from_v_rel(self, v_rel):
        """Solves for up using the conservation of mass equation.

        1 - nu = up/Us,
        where nu is relative volume.
        """
        top = self.c0 * (1 - v_rel)
        bot = 1 - self.s1 * (1 - v_rel)
        return top / bot

    def Us_from_up(self, up):
        return self.c0 + self.s1 * up

    def dP_dv(self, v_rel: Union[np.array, float]) -> Union[np.array, float]:
        """Pressure derivative w.r.t. relative volume nu.

        dP/dnu = - f1/f2, where
        f1 = c0^2 rho_0 (s1 (1-nu) + 1)
        f2 = (1 - s1 (1 - nu))^3
        """
        f1 = self.c0 ** 2 * self.rho_0 * (self.s1 * (1 - v_rel) + 1)
        f2 = (1 - self.s1 * (1 - v_rel)) ** 3
        return -f1 / f2

    def d2P_dv2(self, v_rel: Union[np.array, float]) -> Union[np.array, float]:
        """Pressure second derivative w.r.t. relative volume nu.

        d2P/dnu2 = f1/f2, where
        f1 = 2 c0^2 s1 rho_0 (2 + s1 (1-nu))
        f2 = (1 - s1 (1 - nu))^4
        """
        f1 = (
            2
            * self.c0 ** 2
            * self.s1
            * self.rho_0
            * (2 + self.s1 * (1 - v_rel))
        )
        f2 = (1 - self.s1 * (1 - v_rel)) ** 4
        return f1 / f2

    def new_model(self, x: np.array):
        return self.__class__.__init__(x, self.rho_0)

    def residual_objective(self, data_v, data_P):
        """Calculate RMSE for residuals.

        Unfortunately, need to provide a separate method since the original
        SciPy/fitpack residual is calculated at the __init__, and we
        cannot add data and recompute the residual.
        """
        error = self(data_v) - data_P
        sq = error ** 2
        mean = sq.mean()
        root = mean ** 0.5
        return root

    def residual_objective_with_distending(
        self, data_v_spec, data_P, data_rho_0
    ):
        assert len(data_v_spec) == len(data_P) == len(data_rho_0), (
            "data all need to be the same length. found "
            f"data_v_spec: {len(data_v_spec)}, data_P: {data_P}, "
            f"data_rho_0: {len(data_rho_0)}"
        )

        error = []
        for v_spec, P, rho_0 in zip(data_v_spec, data_P, data_rho_0):
            if rho_0 == self.rho_0:
                v = v_spec * self.rho_0
                error.append(self(v) - P)
            else:
                error.append(self.pressure_distended(v_spec, rho_0) - P)

        return np.sqrt(np.square(error).mean())

    def constraint_1_0(self):
        """Evaluate constraint that the point (1, 0) must be on the curve.

        Constraint is satisfied if the value returned is <= 0.
        Constraint is violated if the value returned is > 0.

        A tolerance is set to make this an inequality constraint.
        Originally:
            P(1) = 0
        Now:
            abs(P(1) - 0) < tol
            abs(P(1)) - tol <= 0
        """
        tol = 1e-6
        return abs(self(1)) - tol

    @staticmethod
    def make_test_points(v_min, v_max):
        assert v_min < v_max
        num_test_points = 10000
        return np.linspace(v_min, v_max, num_test_points)

    def constraint_strictly_decreasing(self, v_min=0.5, v_max=1.01):
        """Evaluate constraint that the curve must be strictly decreasing.

        Constraint is satisfied if the value returned is <= 0.
        Constraint is violated if the value returned is > 0.

        This checks the maximum of the first derivative, sampled at
        10,000 points. The constraint is satisfied if this maximum
        is < 0.

        Originally:
            dP/dv < 0, for all v in [v_min, v_max]
        Now:
            max(dP/dv) <= 0

        If the max value happens to be exactly 0, a small penalty is applied.
        I am not sure if this is reasonable. Hopefully it is a rare occurrence.
        """
        penalty = 1e-6

        v = self.make_test_points(v_min, v_max)
        max_dPdv = self.dP_dv(v).max()

        if max_dPdv == 0:
            max_dPdv += penalty

        return max_dPdv

    def constraint_convex(self, v_min=0.5, v_max=1.01):
        """Evaluate constraint that the curve must be convex.

        Constraint is satisfied if the value returned is <= 0.
        Constraint is violated if the value returned is > 0.

        This checks the values of the second derivative multiplied by -1.

        Originally:
            d2P/dv2 > 0, for all v in [v_min, v_max]
        Now:
            max(-d2P/dv2) <= 0

        If the max value happens to be exactly 0, a small penalty is applied.
        I am not sure if this is reasonable. Hopefully it is a rare occurrence.
        """
        penalty = 1e-6

        v = self.make_test_points(v_min, v_max)
        max_d2Pdv2 = (-1 * self.d2P_dv2(v)).max()

        if max_d2Pdv2 == 0:
            max_d2Pdv2 += penalty

        return max_d2Pdv2

    def constraint_overdriven(self, data_v_spec, data_p) -> list:
        """Constraints for overdriven data.

        Originally, data_p - self(v_rel) > 0.
        Converted to stricter, data_p - self(v_rel) >= tol.
        or constraint = self(v_rel) - data_p + tol <= 0.

        Also makes sure that for the overdriven points, the distance
        on the v axis between the spline and the overdrive points
        decreases as we go to the left (i.e. decreases with smaller v
        or more compression).

        This constraint varies in length.
        """
        assert len(data_v_spec) == len(data_p)
        data_v_spec = np.array(data_v_spec)
        data_p = np.array(data_p)

        # sorts from lowest to highest, or left to right
        idx = data_v_spec.argsort()
        data_v_spec = data_v_spec[idx]
        data_p = data_p[idx]

        tol = 1e-6

        hugo_lower_than_overdriven = [
            self(v) - p + tol for v, p in zip(data_v_spec * self.rho_0, data_p)
        ]

        v_dists = []
        for od_v_spec, od_p in np.column_stack((data_v_spec, data_p)):
            try:
                v_dists.append(self.calc_v_dist_at_p(od_v_spec, od_p))
            except RootSolveError:
                v_dists.append(None)

        v_dists_constraint = []
        solve_errors = 0
        for prev, cur in zip(v_dists[:-1], v_dists[1:]):
            if prev is None or cur is None:
                # append positive numbers that increase with the number
                #   of constraint violates in hopes of encouraging fewer
                #   constraints to be violated.
                solve_errors += 1
                v_dists_constraint.append(solve_errors * 5)
            else:
                # Constraint is satisfied when negative.
                # if the prev dist is smaller than the cur dist
                # then constraint is satisfied.
                # i.e. distance grows from left to right.
                v_dists_constraint.append(prev - cur)

        return hugo_lower_than_overdriven + v_dists_constraint

    def calc_v_dist_at_p(self, data_v, data_p):
        root_sol = root_scalar(
            self.solve_to_find_v_rel_for_p,
            args=(data_p,),
            x0=data_v,
            x1=data_v - 0.1,
        )
        if not root_sol.converged:
            # Warnings take too long to print.
            # warn(f"Could not solve for v at p.\n{root_sol}\n"
            #      f"Returning large v distance.")
            raise RootSolveError

        v_spec = root_sol.root / self.rho_0
        return data_v - v_spec

    def solve_to_find_v_rel_for_p(self, v_rel, p):
        return self(v_rel) - p

    def pressure_distended(self, v_spec: float, rho_dist: float):
        """Calculates the pressure of one point on a distended Hugoniot.

        If the rho_dist is greater than self.rho_0, then it assumes that
        the current spline Hugoniot EOS is actually the distended
        Hugoniot and solves for the original reference Hugoniot.
        (In the code, this is called "anti-distended", although that is not
        a standard term.)

        Note: this EOS' gamma follows the CTH convention, so
        self.gamma is actually the constant G0.
        The Gruneisen gamma G = G0 * v_rel
        and therefore, G/v_spec = G0 * rho_0

        If distending, uses the following form in the calculations
        P_distended = f1 / f2 * P_hugoniot

        where
        f1 = gamma * rho_0 * (v_spec - v_orig_0) + 2
        f2 = gamma * rho_0 * (v_spec - v_dist_0) + 2

        If "anti-distending", then:
        P_hugoniot = f2 / f1 * P_distended,

        where P_distended is calculated on the currently known spline Hugoniot,
        f1 = gamma * rho_antidist * (v_spec - v_antidist_0) + 2
        f2 = gamma * rho_antidist * (v_spec - v_orig_0) + 2

        We must use rho_dist, which is now the reference density for the
        Hugoniot, which is not the spline.


        v_spec:
          The specific volume for the point on the Hugoniot.
        rho_dist:
          The density of the distended EOS.
        """
        if rho_dist <= self.rho_0:
            logger.debug("distending EOS")
            if rho_dist == self.rho_0:
                warn(
                    "distended density is the same as the current density!\n"
                    f"rho_0: {self.rho_0}  rho_dist: {rho_dist}\n"
                    'returning calculated "distended" pressure anyway, '
                    "which should be the pressure on the Hugoniot "
                    "(within numerical error)."
                )
            v_dist_0 = 1 / rho_dist  # cc/g distended reference specific volume
            v_orig_0 = 1 / self.rho_0  # cc/g reference specific volume

            v_rel = v_spec * self.rho_0  # for original Hugoniot
            p_hugo = self(v_rel)

            f1 = self.gamma * self.rho_0 * (v_spec - v_orig_0) + 2
            f2 = self.gamma * self.rho_0 * (v_spec - v_dist_0) + 2
            return f1 / f2 * p_hugo

        elif rho_dist > self.rho_0:
            logger.debug("anti-distending EOS")
            v_orig_0 = 1 / self.rho_0
            v_anti_dist_0 = 1 / rho_dist

            v_rel = v_spec * self.rho_0  # for original spline Hugoniot
            p_orig = self(v_rel)

            f2 = self.gamma * rho_dist * (v_spec - v_orig_0) + 2
            f1 = self.gamma * rho_dist * (v_spec - v_anti_dist_0) + 2

            return f2 / f1 * p_orig
        else:
            raise RuntimeError(
                "Code should not arrive here. Something wrong "
                f"with rho_dist {rho_dist} and self.rho_0 "
                f"{self.rho_0} comparison."
            )
