# Copyright (C) 2021 Reid Ginoza
# All rights reserved.
"""Class for non-spline Optimization Problems with pymoo optimizer.

CURRENTLY INCOMPLETE.
"""
from typing import Collection, Optional, Union

import numpy as np

from pymoo.model.problem import Problem


class OptimizeModel(Problem):
    def __init__(
        self,
        model: object,
        data_v_spec: Union[Collection, np.ndarray],
        data_P: Union[Collection, np.ndarray],
        data_rho_0: Union[Collection, np.ndarray],
        overdriven_v_spec: Optional[Union[Collection, np.ndarray]] = None,
        overdriven_P: Optional[Union[Collection, np.ndarray]] = None,
        xl: Optional[Union[Collection, np.ndarray]] = None,
        xu: Optional[Union[Collection, np.ndarray]] = None,
    ):
        # design by contracts
        assert len(data_v_spec) == len(data_P) == len(data_rho_0), (
            "data all need to be the same length. found "
            f"data_v_spec: {len(data_v_spec)}, data_P: {data_P}, "
            f"data_rho_0: {len(data_rho_0)}"
        )
        if overdriven_v_spec is not None or overdriven_P is not None:
            assert (
                overdriven_v_spec is not None and overdriven_P is not None
            ), "Must supply both overdriven specific volume and pressure data"
            assert len(overdriven_v_spec) == len(
                overdriven_P
            ), "overdriven data must have the same length"
        # end contracts

        print(
            f"== Problem Description ==\n"
            f"Model: {model.name}\n"
            f"Param. lower bound: {xl}\n"
            f"Param. upper bound: {xu}"
        )

        self.data_v_spec = data_v_spec
        self.data_P = data_P
        self.data_rho_0 = data_rho_0
        self.overdriven_v_spec = overdriven_v_spec
        self.overdriven_P = overdriven_P

        n_constr = 3  # point v=1, P=0; strictly decreasing; convex;
        if self.overdriven_v_spec is not None:
            # len(overdriven) constraints are for making the pressure of the
            #   hugoniot below the over driven points
            # the len(overdriven) - 1, as the v_spec gets lower,
            #   the distances on the volume axis between the spline and the
            #   overdriven points need to decrease
            #   (but reversed, since the constraints are satisfied when
            #   the values are positive).
            n_constr += 2 * len(overdriven_v_spec) - 1

        super().__init__(
            n_var=model.n_var,
            n_obj=1,  # RMSE is one objective
            n_constr=n_constr,
            xl=xl,
            xu=xu,
            elementwise_evaluation=True,
        )

    def _evaluate(self, x: np.ndarray, out, *args, **kwargs):
        """Overrides abstract base class method.

        :param x: 1-d array of the coefficients of the model
        :param out: dict(?) with the following keys:
                        "F": value of the objective function
                        "G": value(s) of the constraints where <= 0 means
                             the constraint is satisfied
        :param args:  *args
        :param kwargs:  **kwargs
        :return:
        """
        # Create a new spline with the coeffs
        new_model = self.model.new_model(x)

        out["F"] = new_model.residual_objective_with_distending(
            self.data_v_spec, self.data_P, self.data_rho_0
        )

        out["G"] = [
            new_model.constraint_1_0(),
            new_model.constraint_strictly_decreasing(v_min=0.4, v_max=1.01),
            new_model.constraint_convex(v_min=0.4, v_max=1.01),
        ]
        if self.overdriven_v_spec is not None:
            out["G"].extend(
                new_model.constraint_overdriven(
                    self.overdriven_v_spec, self.overdriven_P
                )
            )
