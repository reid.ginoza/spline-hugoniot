# Copyright (C) 2021 Reid Ginoza
# All rights reserved.
"""Alternative class for use with pymoo optimizer, fixes the reference point."""
from typing import Collection, Optional, Union

import numpy as np

from pymoo.model.problem import Problem

from seos.splines.spline_eos import SplineEOS

Float = Union[float, np.float]


def make_v_data_for_base_spline(
    data_v_spec, overdriven_v_spec, hugo_rho_0: Float, n_pts: int = 20
):
    if overdriven_v_spec is not None:
        data_min = np.concatenate([data_v_spec, overdriven_v_spec]).min()
    else:
        data_min = np.array(data_v_spec).min()

    data_min_v_rel = data_min * hugo_rho_0
    v_min = min(data_min_v_rel, 0.6)
    v_max = 1.0

    return np.logspace(np.log10(v_min), np.log10(v_max), n_pts)


def make_fixed_ref_base_spline(
    data_v_spec,
    overdriven_v_spec,
    rho_0: Float,
    p0: Optional[Float] = None,
    e0: Optional[Float] = None,
    t0: Optional[Float] = None,
    cv: Optional[Float] = None,
    gamma: Optional[Float] = None,
    gamma_kind: Optional[str] = None,
    w=None,
    bbox=None,
    k=3,
    ext=0,
    check_finite=False,
):
    v = make_v_data_for_base_spline(data_v_spec, overdriven_v_spec, rho_0)
    p = np.zeros_like(v)

    return SplineEOS(
        x=v,
        y=p,
        rho_0=rho_0,
        p0=p0,
        e0=e0,
        t0=t0,
        cv=cv,
        gamma=gamma,
        gamma_kind=gamma_kind,
        w=w,
        bbox=bbox,
        k=k,
        ext=ext,
        check_finite=check_finite,
    )


class OptimizeSplineFixedRef(Problem):
    def __init__(
        self,
        base_spline: Optional[SplineEOS],
        data_v_spec: Union[Collection, np.ndarray],
        data_P: Union[Collection, np.ndarray],
        data_rho_0: Union[Collection, np.ndarray],
        xl: Union[Collection, np.ndarray],
        xu: Union[Collection, np.ndarray],
        overdriven_v_spec: Optional[Union[Collection, np.ndarray]] = None,
        overdriven_P: Optional[Union[Collection, np.ndarray]] = None,
        check_sound_speed: bool = True,
        **kwargs,
    ):
        """Use the base_spline and data to initialize the optimization problem.

        Sets `self._n_coeffs` to the number of coefficients - 4 because
        there are always four extra basis splines that are set to zero.
        """
        # design by contracts
        assert len(data_v_spec) == len(data_P) == len(data_rho_0), (
            "data all need to be the same length. found "
            f"data_v_spec: {len(data_v_spec)}, data_P: {data_P}, "
            f"data_rho_0: {len(data_rho_0)}"
        )
        if overdriven_v_spec is not None or overdriven_P is not None:
            assert (
                overdriven_v_spec is not None and overdriven_P is not None
            ), "Must supply both overdriven specific volume and pressure data"
            assert len(overdriven_v_spec) == len(
                overdriven_P
            ), "overdriven data must have the same length"
        # end contracts

        self.check_sound_speed = check_sound_speed

        self.base_spline = base_spline
        self.right_end_knots = base_spline.get_private_data("k") + 1
        self.ref_idx = -1 - self.right_end_knots
        n_changing_coeffs = (
            len(base_spline.get_private_data("t")) - 1 - self.right_end_knots
        )

        if not len(xl) == len(xu) == n_changing_coeffs:
            raise ValueError(
                f"Must have the same length for xl, xu, and the number of "
                f"changing coefficients t - 1 - (k + 1). "
                f"found len(xl) {len(xl)}, len(xu) {len(xu)}, "
                f"n_changing_coeffs {n_changing_coeffs}"
            )

        print(
            f"== Problem Description ==\n"
            f"Spline {len(base_spline.get_private_data('t'))} Knots "
            f"({len(base_spline.get_knots())} Interior Knots): "
            f"{base_spline.get_private_data('t')}\n"
            f"Spline number of coeffs to change: {n_changing_coeffs}\n"
            f"Original coeffs (unused in optimization): "
            f"{base_spline.get_private_data('c')}\n"
            f"Spline degree (k): {base_spline.get_private_data('k')}\n"
            f"Coeffs lower bound: {xl}\n"
            f"Coeffs upper bound: {xu}"
        )

        self.data_v_spec = data_v_spec
        self.data_P = data_P
        self.data_rho_0 = data_rho_0
        self.overdriven_v_spec = overdriven_v_spec
        self.overdriven_P = overdriven_P

        # constraints are:
        # [0]: point v=1, P=0;
        # [1]: strictly decreasing;
        # [2]: convex;
        # if check_sound_speed:
        # [3]: sound speed non-imaginary;
        # [4]: sound speed non-increasing;
        n_constr = 3

        if self.check_sound_speed:
            n_constr += 2

        if self.overdriven_v_spec is not None:
            # len(overdriven) constraints are for making the pressure of the
            #   hugoniot below the over driven points
            # the len(overdriven) - 1, as the v_spec gets lower,
            #   the distances on the volume axis between the spline and the
            #   overdriven points need to decrease
            #   (but reversed, since the constraints are satisfied when
            #   the values are positive).
            n_constr += 2 * len(overdriven_v_spec) - 1

        super().__init__(
            n_var=n_changing_coeffs,
            n_obj=1,  # RMSE is one objective
            n_constr=n_constr,
            xl=xl,
            xu=xu,
            elementwise_evaluation=True,
            **kwargs,
        )

    def _evaluate(self, x: np.ndarray, out, *args, **kwargs) -> None:
        """Overrides abstract base class method.

        :param x: 1-d array of the coefficients of the spline
        :param out: dict(?) with the following keys:
                        "F": value of the objective function
                        "G": value(s) of the constraints where <= 0 means
                             the constraint is satisfied
        :param args:  *args
        :param kwargs:  **kwargs
        :return:
        """
        coeffs = self.make_full_coeffs(x)
        new_spline = self.base_spline.new_spline_from_coeffs(coeffs)

        out["F"] = new_spline.residual_objective_with_distending(
            self.data_v_spec, self.data_P, self.data_rho_0
        )

        out["G"] = [
            new_spline.constraint_1_0(),
            new_spline.constraint_strictly_decreasing(v_min=0.4, v_max=1.0),
            new_spline.constraint_convex(v_min=0.4, v_max=1.0),
        ]

        if self.check_sound_speed:
            out["G"].extend(
                [
                    new_spline.constraint_sound_speed_non_imaginary(),
                    new_spline.constraint_sound_speed_non_increasing(),
                ]
            )

        if self.overdriven_v_spec is not None:
            out["G"].extend(
                new_spline.constraint_overdriven(
                    self.overdriven_v_spec, self.overdriven_P
                )
            )

    def make_full_coeffs(self, x: np.ndarray) -> np.ndarray:
        """Makes full coeffs from the ones that changes.

        :param x: 1-d array of the coefficients of the spline
        :return: coeffs: full coeffs with n + k + 1 coeffs
        """
        # The reference point is currently set to zero, but it could
        # conceivably something like 1e-6 or even some other reference point.
        ref_point = np.array([0.0])
        right_end = np.zeros(self.right_end_knots)
        coeffs = np.concatenate([x, ref_point, right_end])

        assert len(coeffs) == len(self.base_spline.get_private_data("c"))
        return coeffs
