# Copyright (C) 2021 Reid Ginoza
# All rights reserved.
"""Main class for use with pymoo optimizer."""
from datetime import datetime, timedelta
from pathlib import Path
import pickle
from typing import Collection, Optional, Union

import numpy as np

from pymoo.model.algorithm import Algorithm
from pymoo.model.callback import Callback
from pymoo.model.problem import Problem
from pymoo.util.display import MultiObjectiveDisplay

from seos.optimizer.opt_plots import make_batch_plot
from seos.splines.spline_eos import SplineEOS

Float = Union[float, np.float]


def default_low_bound(starting_coeffs: np.ndarray) -> np.ndarray:
    xl = starting_coeffs * 0.9
    idx = starting_coeffs == 0
    xl[idx] = -0.1
    return xl


def default_upper_bound(starting_coeffs: np.ndarray) -> np.ndarray:
    xu = starting_coeffs * 1.1
    idx = starting_coeffs == 0
    xu[idx] = 0.1
    return xu


class OptimizeSpline(Problem):
    def __init__(
        self,
        base_spline: SplineEOS,
        data_v_spec: Union[Collection, np.ndarray],
        data_P: Union[Collection, np.ndarray],
        data_rho_0: Union[Collection, np.ndarray],
        overdriven_v_spec: Optional[Union[Collection, np.ndarray]] = None,
        overdriven_P: Optional[Union[Collection, np.ndarray]] = None,
        xl: Optional[Union[Collection, np.ndarray]] = None,
        xu: Optional[Union[Collection, np.ndarray]] = None,
        check_sound_speed: bool = True,
        **kwargs,
    ):
        """Use the base_spline and data to initialize the optimization problem.

        Sets `self._n_coeffs` to the number of coefficients - 4 because
        there are always four extra basis splines that are set to zero.
        """
        # design by contracts
        assert len(data_v_spec) == len(data_P) == len(data_rho_0), (
            "data all need to be the same length. found "
            f"data_v_spec: {len(data_v_spec)}, data_P: {data_P}, "
            f"data_rho_0: {len(data_rho_0)}"
        )
        if overdriven_v_spec is not None or overdriven_P is not None:
            assert (
                overdriven_v_spec is not None and overdriven_P is not None
            ), "Must supply both overdriven specific volume and pressure data"
            assert len(overdriven_v_spec) == len(
                overdriven_P
            ), "overdriven data must have the same length"
        # end contracts

        self.check_sound_speed = check_sound_speed

        self._base_spline: SplineEOS = base_spline
        starting_coeffs = self._base_spline.get_coeffs()
        n_coeffs: int = len(starting_coeffs)

        if xl is None:
            xl = default_low_bound(starting_coeffs)

        if xu is None:
            xu = default_upper_bound(starting_coeffs)

        assert len(xl) == len(xu) == n_coeffs

        print(
            f"== Problem Description ==\n"
            f"Spline {len(base_spline.get_knots())} Knots: "
            f"{base_spline.get_knots()}\n"
            f"Spline Number of Coeffs: {n_coeffs}\n"
            f"Coeffs lower bound: {xl}\n"
            f"Coeffs upper bound: {xu}"
        )

        self.data_v_spec = data_v_spec
        self.data_P = data_P
        self.data_rho_0 = data_rho_0
        self.overdriven_v_spec = overdriven_v_spec
        self.overdriven_P = overdriven_P

        # constraints are:
        # [0]: point v=1, P=0;
        # [1]: strictly decreasing;
        # [2]: convex;
        # if check_sound_speed:
        # [3]: sound speed non-imaginary;
        # [4]: sound speed non-increasing;
        n_constr = 3

        if self.check_sound_speed:
            n_constr += 2

        if self.overdriven_v_spec is not None:
            # len(overdriven) constraints are for making the pressure of the
            #   hugoniot below the over driven points
            # the len(overdriven) - 1, as the v_spec gets lower,
            #   the distances on the volume axis between the spline and the
            #   overdriven points need to decrease
            #   (but reversed, since the constraints are satisfied when
            #   the values are positive).
            n_constr += 2 * len(overdriven_v_spec) - 1

        super().__init__(
            n_var=n_coeffs,
            n_obj=1,  # RMSE is one objective
            n_constr=n_constr,
            xl=xl,
            xu=xu,
            elementwise_evaluation=True,
            **kwargs,
        )

    def _evaluate(self, x: np.ndarray, out, *args, **kwargs) -> None:
        """Overrides abstract base class method.

        :param x: 1-d array of the coefficients of the spline
        :param out: dict(?) with the following keys:
                        "F": value of the objective function
                        "G": value(s) of the constraints where <= 0 means
                             the constraint is satisfied
        :param args:  *args
        :param kwargs:  **kwargs
        :return:
        """
        new_spline = self._base_spline.new_spline_from_coeffs(x)

        out["F"] = new_spline.residual_objective_with_distending(
            self.data_v_spec, self.data_P, self.data_rho_0
        )

        out["G"] = [
            new_spline.constraint_1_0(),
            new_spline.constraint_strictly_decreasing(v_min=0.4, v_max=1.0),
            new_spline.constraint_convex(v_min=0.4, v_max=1.0),
        ]

        if self.check_sound_speed:
            out["G"].extend(
                [
                    new_spline.constraint_sound_speed_non_imaginary(),
                    new_spline.constraint_sound_speed_non_increasing(),
                ]
            )
        if self.overdriven_v_spec is not None:
            out["G"].extend(
                new_spline.constraint_overdriven(
                    self.overdriven_v_spec, self.overdriven_P
                )
            )


class SavePopulation(Callback):
    def __init__(self, out_dir: Path, batch_size: int = 100) -> None:
        super().__init__()
        self.archive_dir = out_dir / "archive"
        self.archive_dir.mkdir(exist_ok=True)
        self.batch_size = batch_size
        self.batch_pops = []
        self.batch_gens = []

    def notify(self, algorithm: Algorithm):
        self.batch_pops.append(algorithm.pop)
        self.batch_gens.append(algorithm.n_gen)

        if algorithm.has_terminated or len(self.batch_pops) >= self.batch_size:
            file_batch_name = "gen_%04d_%04d" % (
                self.batch_gens[0],
                self.batch_gens[-1],
            )

            pkl = self.archive_dir / (file_batch_name + ".pickle")
            with pkl.open("wb") as f:
                pickle.dump(self.batch_pops, f)

            make_batch_plot(
                self.batch_pops,
                self.batch_gens,
                file_batch_name,
                self.archive_dir,
            )
            self._clear_batch()

    def _clear_batch(self):
        self.batch_pops = []
        self.batch_gens = []


class SEOSDisplay(MultiObjectiveDisplay):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.gen_time_check = datetime.now()

    def _do(self, problem, evaluator, algorithm):
        super()._do(problem, evaluator, algorithm)
        self.output.append("fit (avg)", np.mean(algorithm.pop.get("F")))
        self.output.append("fit (min)", np.min(algorithm.pop.get("F")))
        gen_time = self._gen_time_diff()
        self.output.append("gen time", gen_time)

    def _gen_time_diff(self):
        now = datetime.now()
        diff = self._round_time(now - self.gen_time_check)
        self.gen_time_check = now
        return diff

    @staticmethod
    def _round_time(orig_diff: timedelta):
        return timedelta(seconds=orig_diff.seconds, microseconds=0,)
