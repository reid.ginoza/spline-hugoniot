# Copyright (C) 2021 Reid Ginoza
# All rights reserved.
"""Plot functions."""
from pathlib import Path
from typing import Collection, List, Optional

import matplotlib

try:
    matplotlib.use("Agg")
except RecursionError as e:
    print(
        f"Caught RecursionError {e}.\n"
        "Assuming matplotlib.use was called before. Will proceed."
    )
from matplotlib import pyplot as plt
import numpy as np
import pandas as pd

from pymoo.model.population import Population

from seos.splines.spline_eos import SplineEOS

ARA_BLUE = "#002E6C"
ARA_DARK_BLUE = "#00204B"
ARA_BRIGHT_BLUE = "#0092CA"
ARA_ORANGE = "#F05532"
ARA_GREEN = "#D2DD3F"
ARA_GREY = "#DEDEDE"
ARA_DARK_GREY = "#666666"

COLORS = {
    "ARA_BLUE": ARA_BLUE,
    "ARA_DARK_BLUE": ARA_DARK_BLUE,
    "ARA_BRIGHT_BLUE": ARA_BRIGHT_BLUE,
    "ARA_ORANGE": ARA_ORANGE,
    "ARA_GREEN": ARA_GREEN,
    "ARA_GREY": ARA_GREY,
    "ARA_DARK_GREY": ARA_DARK_GREY,
}


def sanity_check_plot(
    base_spline: SplineEOS,
    data_v: Collection,
    data_P: Collection,
    lower_bounds,
    upper_bounds,
):
    fig: plt.Figure
    ax: plt.Axes
    fig, ax = plt.subplots()

    lower_bound_spline = base_spline.new_spline_from_coeffs(lower_bounds)
    upper_bound_spline = base_spline.new_spline_from_coeffs(upper_bounds)

    v = np.linspace(min(data_v) - 0.005, max(data_v) + 0.05, 10000)
    ax.fill_between(
        v,
        lower_bound_spline(v),
        upper_bound_spline(v),
        color="grey",
        label="Bounds for Splines",
    )
    ax.plot(v, lower_bound_spline(v), color="darkgrey", label="Lower Bound")
    ax.plot(v, base_spline(v), label="Base Spline")
    ax.scatter(data_v, data_P, label="Data")
    ax.set_title("Inputs into Optimization")
    ax.legend()
    ax.set_xlabel(r"Relative Volume $\nu$")
    ax.set_ylabel(r"Pressure $P$ (GPa)")
    return fig, ax


def make_batch_plot(
    batch_pops: List[Population],
    batch_gens: List[int],
    file_batch_name: str,
    archive_dir: Path,
):
    """written for use with SavePopulation class."""
    individuals = {"Generation": [], "Fitness": []}
    means = {"Generation": [], "Mean": []}

    for idx, pop in enumerate(batch_pops):
        fitness = pop.get("F")
        individuals["Fitness"].extend([float(i) for i in fitness])
        individuals["Generation"].extend([batch_gens[idx] for _ in fitness])
        means["Generation"].append(batch_gens[idx])
        means["Mean"].append(float(fitness.mean()))

    fig: plt.Figure
    ax: plt.Axes
    fig, ax = plt.subplots()

    ax.scatter(
        individuals["Generation"],
        individuals["Fitness"],
        label="Individuals",
        alpha=0.5,
    )
    ax.plot(means["Generation"], means["Mean"], color="orange", label="Mean")
    ax.set_xlabel("Generation")
    ax.set_ylabel("Fitness RMSD (GPa)")
    ax.set_title(
        f"Generation {means['Generation'][0]} to {means['Generation'][-1]}"
    )
    ax.legend()
    fig.savefig(archive_dir / (file_batch_name + ".png"))
    fig.savefig(archive_dir / (file_batch_name + ".eps"))
    plt.close(fig)

    pd.DataFrame(individuals).to_csv(
        archive_dir / (file_batch_name + "_indv.csv"), index=False
    )
    pd.DataFrame(means).to_csv(
        archive_dir / (file_batch_name + "_means.csv"), index=False
    )


def plot_spline_against_data(
    res_spline: SplineEOS, data_v, data_P, rmsd: Optional[float] = None
):
    fig: plt.Figure
    ax: plt.Axes
    fig, ax = plt.subplots()

    v = np.linspace(min(data_v) - 0.005, max(data_v) + 0.05, 10000)
    ax.scatter(data_v, data_P, label="Data", color=ARA_BLUE)
    ax.plot(v, res_spline(v), label="Resulting Spline", color=ARA_ORANGE)

    if rmsd:
        title = f"Optimization Result\nRMSD: {float(rmsd):.4f} GPa"
    else:
        title = "Optimization Result"
    ax.set_title(title)

    ax.legend()
    ax.set_xlabel(r"Relative Volume $\nu$")
    ax.set_ylabel(r"Pressure $P$ (GPa)")
    return fig, ax
