# Copyright (C) 2021 Reid Ginoza
# All rights reserved.
"""Creates a starting spline.

Intended to be used as the initial spline from which the coeffs
will vary in the optimization.
"""
from pathlib import Path

import numpy as np
import pandas as pd

from seos.splines.spline_eos import SplineEOS

_P0 = 101325e-4  # 1 atm in GPa


def few_nodes_spline(
    rho_0=1.9, p0=0.0, e0=0.0, t0=298.15, cv=1.3052623990693266, gamma=1
):
    """Returns a SplineEOS made with 21 knots."""
    hugo = pd.read_csv(Path(__file__).parent / "files" / "lx17_jwl_hugo.csv")
    # Include the point (1, 0) which is theoretically required
    #   as the reference point/initial value
    hugo = hugo.append({"V(--)": 1, "P(GPA)": 0,}, ignore_index=True,)
    spline = SplineEOS(
        hugo["V(--)"],
        hugo["P(GPA)"],
        rho_0=rho_0,
        p0=p0,
        e0=e0,
        t0=t0,
        cv=cv,  # J/g/K
        gamma=gamma,
    )
    return spline


def make_many_v_pts(v_min=0.3, v_max=1.0) -> np.ndarray:
    """Makes 52 logspaced points from v_min to v_max, probably in v_rel."""
    return np.logspace(np.log10(v_min), np.log10(v_max))


def make_many_v_pts_old() -> np.ndarray:
    """Makes 52 logspaced points that start at 0.3, include 1.0,
    and slightly greater.
    """
    v_min = 0.3
    v_include = 1.0  # must have this point

    # get distance to follow log spaced pattern beyond 1.
    # e is the exponent of 10. The exponents are evenly spaced.
    e = np.linspace(np.log10(v_min), np.log10(v_include))
    de = (e[1:] - e[:-1]).mean()  # gets the distance between the points.

    v = np.logspace(np.log10(v_min), np.log10(v_include))
    v = np.concatenate([v, [10 ** de, 10 ** (2 * de)]])
    return v


def many_nodes_spline(
    rho_0=1.9, p0=0.0, e0=0.0, t0=298.15, cv=1.3052623990693266, gamma=1
):
    """Returns a SplineEOS with 50 log-spaced knots.

    CV in J/g/K
    """
    v = make_many_v_pts()

    p = few_nodes_spline()(v)
    new_spline = SplineEOS(
        v, p, rho_0=rho_0, p0=p0, e0=e0, t0=t0, cv=cv, gamma=gamma
    )
    return new_spline


def many_many_nodes_spline(
    rho_0=1.9, p0=0.0, e0=0.0, t0=298.15, cv=1.3052623990693266, gamma=1
):
    """Returns a SplineEOS with 101 knots and 103 coefficients.

    CV in J/g/K
    """
    v = make_many_v_pts()
    midpoints = 0.5 * (v[:-1] + v[1:])
    v = np.sort(np.concatenate([v, midpoints]))

    p = few_nodes_spline()(v)
    new_spline = SplineEOS(
        v, p, rho_0=rho_0, p0=p0, e0=e0, t0=t0, cv=cv, gamma=gamma
    )
    return new_spline


def tarver_jwl_spline():
    """Creates a spline to fit the Tarver JWL Hugoniot with the same
    reference point and CV and G0 (gamma).

    List of CV parameters
    ---------------------

    Cameron Moen LX-17 CV: 1.16 J/(g*K)
    1.16 * 11604.5 (K/eV) * 1e7 (erg/J) erg/g/eV
    1.346122e+11 erg/g/eV

    CTH units: erg/g/eV
    CTH MGR: 1.35E11 erg/g/eV
    1.163341807057607 J/(g*K)

    # from Tarver, 6-97
    CTH JWL LX-17_UR: 1.514691751E11 erg/g/eV
    1.3052623990693266 J/(g*K)
    """
    rho_0 = 1.905  # g/cc
    p0 = 3.3027e-04  # GPa
    e0 = 6.7890e-02  # MJ/kg or kJ/g
    cv = 1.3052623990693266  # J / (g * K)
    t0 = 298.15  # K
    gamma = 0.894
    return few_nodes_spline(rho_0, p0, e0, t0=t0, cv=cv, gamma=gamma)
