# Copyright (C) 2021 Reid Ginoza
# All rights reserved.
"""Main class for this project."""
from copy import copy
from typing import Iterable, Optional, Union
from warnings import warn

from matplotlib import pyplot as plt
import numpy as np
import pandas as pd
from scipy import integrate
from scipy.interpolate import InterpolatedUnivariateSpline
from scipy.optimize import minimize_scalar, root_scalar

from seos.utilities.conversions import GPa_to_atm, J_to_kJ, kJ_to_cal
from seos.utilities.seos_logger import make_logger

logger = make_logger(__name__, "INFO")
# logger = make_logger(__name__, "DEBUG", "spline_eos.log")

Float = Union[float, np.float]


class ReferenceStateMissing(Exception):
    """Raised when there is an issue with the SplineEOS reference state."""

    pass


class RootSolveError(Exception):
    """Raised when the root_scalar function does not converge."""

    pass


class SplineEOS(InterpolatedUnivariateSpline):
    def __init__(
        self,
        x,
        y,
        rho_0: Optional[Float] = None,
        p0: Optional[Float] = None,
        e0: Optional[Float] = None,
        t0: Optional[Float] = None,
        cv: Optional[Float] = None,
        gamma: Optional[Float] = None,
        gamma_kind: Optional[str] = "mg",
        w=None,
        bbox=None,
        k=3,
        ext=0,
        check_finite=False,
    ):
        """Initializes a SplineEOS.

        The Gruneisen Gamma is assumed to be a function of relative volume:
            Gamma = self.gamma * v_rel

        gamma_kind must be None, 'mg', or 'constant'
        """
        if bbox is None:
            bbox = [None] * 2
        super().__init__(x, y, w, bbox, k, ext, check_finite)
        self.rho_0 = rho_0
        self.p0 = p0
        self.e0 = e0
        self.t0 = t0
        self.cv = cv
        self.gamma = gamma
        self.gamma_kind = gamma_kind

    def new_spline_from_coeffs(self, coeffs: np.ndarray):
        coeffs = self._check_new_coeffs(coeffs)
        new = self.__class__._from_tck([self._data[8], coeffs, self._data[5]])
        new.rho_0 = self.rho_0
        new.p0 = self.p0
        new.e0 = self.e0
        new.t0 = self.t0
        new.cv = self.cv
        new.gamma = self.gamma
        new.gamma_kind = self.gamma_kind
        return new

    def _check_new_coeffs(self, coeffs: np.ndarray):
        if coeffs.ndim != 1:
            raise ValueError(f"Expected 1D array. Received {coeffs.ndim}")

        # catch the case where the last four coeffs for the
        # constant zero splines are not specified
        if len(coeffs) == len(self._data[9]) - 4:
            coeffs = np.concatenate([coeffs, np.zeros(4)])
        elif len(coeffs) == len(self._data[9]):
            # all coeffs are specified and this is normal
            pass
        else:
            raise ValueError(
                "coeffs needs to be same length as this "
                f"spline's coeffs. Provided {len(coeffs)} "
                f"but need {len(self._data[9])}"
            )

        return coeffs

    def residual_objective(self, data_v, data_P):
        """Calculate RMSE for residuals.

        Unfortunately, need to provide a separate method since the original
        SciPy/fitpack residual is calculated at the __init__, and we
        cannot add data and recompute the residual.
        """
        # sourcery skip: inline-immediately-returned-variable
        error = self(data_v) - data_P
        sq = error ** 2
        mean = sq.mean()
        root = mean ** 0.5
        return root

    def residual_objective_with_distending(
        self, data_v_spec, data_P, data_rho_0
    ):
        assert len(data_v_spec) == len(data_P) == len(data_rho_0), (
            "data all need to be the same length. found "
            f"data_v_spec: {len(data_v_spec)}, data_P: {data_P}, "
            f"data_rho_0: {len(data_rho_0)}"
        )

        error = []
        for v_spec, P, rho_0 in zip(data_v_spec, data_P, data_rho_0):
            if rho_0 == self.rho_0:
                v = v_spec * self.rho_0
                error.append(self(v) - P)
            else:
                error.append(self.pressure_distended(v_spec, rho_0) - P)

        return np.sqrt(np.square(error).mean())

    def constraint_1_0(self):
        """Evaluate constraint that the point (1, 0) must be on the curve.

        Constraint is satisfied if the value returned is <= 0.
        Constraint is violated if the value returned is > 0.

        A tolerance is set to make this an inequality constraint.
        Originally:
            P(1) = 0
        Now:
            abs(P(1) - 0) < tol
            abs(P(1)) - tol <= 0
        """
        tol = 1e-6
        return abs(self(1)) - tol

    def make_test_points(
        self, v_min: Optional[Float] = None, v_max: Optional[Float] = None
    ) -> np.ndarray:
        if v_min is not None and v_max is not None:
            assert v_min < v_max

        knots = self.get_knots()

        # make sure we check beyond the outer knots
        if v_min is None or knots.min() < v_min:
            v_min = knots.min() * 0.98

        if v_max is None or knots.max() > v_max:
            v_max = knots.max() * 1.02

        # don't exceed the reference point
        v_max = min(1.0, v_max)

        num_test_points = max(10000, len(knots) * 10)

        return np.linspace(v_min, v_max, num_test_points)

    def constraint_strictly_decreasing(self, v_min=0.5, v_max=1.0):
        """Evaluate constraint that the curve must be strictly decreasing.

        Constraint is satisfied if the value returned is <= 0.
        Constraint is violated if the value returned is > 0.

        This checks the maximum of the first derivative, sampled at
        10,000 points. The constraint is satisfied if this maximum
        is < 0.

        Originally:
            dP/dv < 0, for all v in [v_min, v_max]
        Now:
            max(dP/dv) <= 0

        If the max value happens to be exactly 0, a small penalty is applied.
        I am not sure if this is reasonable. Hopefully it is a rare occurrence.
        """
        # sourcery skip: move-assign
        penalty = 1e-6

        v = self.make_test_points(v_min, v_max)
        max_dPdv = self.derivative(1)(v).max()

        if max_dPdv == 0:
            max_dPdv += penalty

        return max_dPdv

    def constraint_convex(self, v_min=0.5, v_max=1.0):
        """Evaluate constraint that the curve must be convex.

        Constraint is satisfied if the value returned is <= 0.
        Constraint is violated if the value returned is > 0.

        This checks the values of the second derivative multiplied by -1.

        Originally:
            d2P/dv2 > 0, for all v in [v_min, v_max]
        Now:
            max(-d2P/dv2) <= 0

        If the max value happens to be exactly 0, a small penalty is applied.
        I am not sure if this is reasonable. Hopefully it is a rare occurrence.
        """
        # sourcery skip: move-assign
        penalty = 1e-6

        v = self.make_test_points(v_min, v_max)
        max_d2Pdv2 = (-1 * self.derivative(2)(v)).max()

        if max_d2Pdv2 == 0:
            max_d2Pdv2 += penalty

        return max_d2Pdv2

    def constraint_overdriven(self, data_v_spec, data_p) -> list:
        """Constraints for overdriven data.

        Originally, data_p - self(v_rel) > 0.
        Converted to stricter, data_p - self(v_rel) >= tol.
        or constraint = self(v_rel) - data_p + tol <= 0.

        Also makes sure that for the overdriven points, the distance
        on the v axis between the spline and the overdrive points
        decreases as we go to the left (i.e. decreases with smaller v
        or more compression).

        This constraint varies in length.
        """
        assert len(data_v_spec) == len(data_p)
        data_v_spec = np.array(data_v_spec)
        data_p = np.array(data_p)

        # sorts from lowest to highest, or left to right
        idx = data_v_spec.argsort()
        data_v_spec = data_v_spec[idx]
        data_p = data_p[idx]

        tol = 1e-6

        hugo_lower_than_overdriven = [
            self(v) - p + tol for v, p in zip(data_v_spec * self.rho_0, data_p)
        ]

        v_dists = []
        for od_v_spec, od_p in np.column_stack((data_v_spec, data_p)):
            try:
                v_dists.append(self.calc_v_dist_at_p(od_v_spec, od_p))
            except RootSolveError:
                v_dists.append(None)

        v_dists_constraint = []
        solve_errors = 0
        for prev, cur in zip(v_dists[:-1], v_dists[1:]):
            if prev is None or cur is None:
                # append positive numbers that increase with the number
                #   of constraint violates in hopes of encouraging fewer
                #   constraints to be violated.
                solve_errors += 1
                v_dists_constraint.append(solve_errors * 5)
            else:
                # Constraint is satisfied when negative.
                # if the prev dist is smaller than the cur dist
                # then constraint is satisfied.
                # i.e. distance grows from left to right.
                v_dists_constraint.append(prev - cur)

        return hugo_lower_than_overdriven + v_dists_constraint

    def constraint_sound_speed_non_imaginary(self) -> Float:
        """Checks that the square of the sound speed is non-negative.

        Since the optimizer uses a value to select for the constraint,
        based on whether it is <= 0, this first samples C_s^2 and takes
        the minimum, then returns the opposite (i.e. multiplies by -1).
        Looks at the interval just to the left of the first knot and up to
        a relative volume of one.
        """
        v_min = self._data[8].min() * 0.95
        v_max = 1.0

        v = np.linspace(v_min, v_max, 10000)

        cs_sq_min = self.sound_speed_squared_at_v_rel(v).min()

        return -1 * cs_sq_min

    def constraint_sound_speed_non_imaginary_original(self) -> Float:
        """Checks that the square of the sound speed is non-negative.

        Had to set aside implementation because either the minimization or
        the FloatPointErrors were too slow.

        Since the optimizer uses a value to select for the constraint,
        based on whether it is <= 0, this first finds the minimum of
        the C_s^2, then returns the opposite (i.e. multiplies by -1).
        Looks at the interval just to the left of the first knot and up to
        a relative volume of one.
        """
        # sourcery skip: move-assign
        v_min = self._data[8].min() * 0.95
        v_max = 1.0
        with np.errstate(all="raise"):
            try:
                opt_res = minimize_scalar(
                    self.sound_speed_squared_at_v_rel, bounds=(v_min, v_max)
                )
            except FloatingPointError:
                # logger.exception(
                #     "constraint_sound_speed_non_imaginary "
                #     "raised FloatingPointError"
                # )
                return 5000.0  # dummy value for calculation failed

        if opt_res.success:
            return -1 * opt_res.fun
        else:
            # logger.info(
            #     "minimization in constraint_sound_speed_non_imaginary "
            #     "was not successful"
            # )
            return 5000.0  # dummy value for calculation failed

    def constraint_sound_speed_non_increasing(self):
        """Evaluates constraint that the sound speed must be non-increasing.

        Constraint is satisfied if the value returned is <= 0.
        Constraint is violated if the value returned is > 0.

        This checks the maximum of the first derivative, as a forward
        difference, sampled at 10,000 points. The constraint is satisfied
        if this maximum is < 0.

        Must check only up to relative volume of 0.925 because of
        numerical errors.
        """
        v_min = self._data[8].min() * 0.95
        v_max = 0.925
        v = np.linspace(v_min, v_max, 10000)
        cs_sq = self.sound_speed_squared_at_v_rel(v)
        d_cs_sq_dv = np.diff(cs_sq)
        return d_cs_sq_dv.max()

    def calc_v_dist_at_p(self, data_v, data_p):
        root_sol = root_scalar(
            self.solve_to_find_v_rel_for_p,
            args=(data_p,),
            x0=data_v,
            x1=data_v - 0.1,
        )
        if not root_sol.converged:
            # Warnings take too long to print.
            # warn(f"Could not solve for v at p.\n{root_sol}\n"
            #      f"Returning large v distance.")
            raise RootSolveError

        v_spec = root_sol.root / self.rho_0
        return data_v - v_spec

    def solve_to_find_v_rel_for_p(self, v_rel, p):
        return self(v_rel) - p

    def gruneisen_gamma_mg(self, v_rel):
        return self.gamma * v_rel

    def gruneisen_gamma_constant(self, v_rel):
        return self.gamma

    def sound_speed_squared_at_v_rel(
        self, v_rel: Union[Float, np.ndarray], gamma_kind: Optional[str] = None
    ):
        """uses the form:

        (f1 * f2 * f3) + (f4 * f5)

        f1 = v_spec / 2
        f2 = gg * v_0 - (2 + gg) * v_spec
        f3 = dp/dv along the Hugoniot, where v is specific volume
        f4 = gg * v_spec
        f5 = (p - p0) / 2
        """
        v_spec = v_rel / self.rho_0  # cc/g
        v_0 = 1 / self.rho_0  # cc/g
        p = self(v_rel)  # GPa

        if gamma_kind is None:
            gamma_kind = self.gamma_kind

        if gamma_kind == "mg":
            gg = self.gruneisen_gamma_mg(v_rel)
        elif gamma_kind == "constant":
            gg = self.gruneisen_gamma_constant(v_rel)
        else:
            raise ValueError("gamma_kind must be None 'mg' or 'constant'")

        dp_dv_hugo = self.dp_dv_spec(v_rel)

        f1 = v_spec / 2
        f2 = gg * v_0 - (2 + gg) * v_spec
        f3 = dp_dv_hugo
        f4 = gg * v_spec
        f5 = (p - self.p0) / 2

        return (f1 * f2 * f3) + (f4 * f5)

    def dp_dv_spec(self, v_rel: Union[Float, np.ndarray]):
        v_spec = v_rel / self.rho_0
        v_rels = self.make_test_points()
        pressures = self(v_rels)
        v_specs = v_rels / self.rho_0

        p_v_spec = InterpolatedUnivariateSpline(x=v_specs, y=pressures)
        return p_v_spec(v_spec, nu=1)

    def get_basis(self):
        basis = []
        for i in range(len(self._data[9])):
            coeffs = np.zeros_like(self._data[9])
            coeffs[i] = 1
            basis.append(self.new_spline_from_coeffs(coeffs))
        return basis

    def plot_basis(
        self, x_min=0.5, x_max=1.2, x_left=0.55, x_right=1.05, show_legend=True
    ):
        basis = self.get_basis()
        x = np.linspace(x_min, x_max, 10000)

        fig: plt.Figure
        ax: plt.Axes
        fig, ax = plt.subplots()
        cmap = plt.get_cmap("tab20")

        for i, b in enumerate(basis):
            kwargs = {
                "color": cmap.colors[i % 20],
                "label": f"Spline {i}",
            }
            if i > 20:
                kwargs["ls"] = "-."
            ax.plot(x, b(x), **kwargs)

        ax.set_xlim(left=x_left, right=x_right)
        ax.set_ylim(bottom=-0.25, top=1)
        ax.set_xlabel("Relative Volume (--)")
        ax.set_ylabel("Pressure (GPa)")
        ax.set_title("Basis Splines")
        if show_legend:
            ax.legend(bbox_to_anchor=(1.0, 1), loc="upper left")
        plt.tight_layout()
        return fig, ax

    def _reference_state_missing(self):
        return any((ref is None for ref in (self.rho_0, self.p0, self.e0)))

    def _energy_hugo_at_one_point(self, v, units):
        if self._reference_state_missing():
            raise ReferenceStateMissing

        p_hugo = self(v)
        v_spec = v / self.rho_0
        v0 = 1 / self.rho_0

        # Note: GPa * cc/g = kJ/g
        e_hugo = self.e0 + 0.5 * (p_hugo + self.p0) * (v0 - v_spec)  # kJ/g
        if units == "kJ/g":
            return e_hugo
        elif units == "cal/g":
            return e_hugo * kJ_to_cal
        else:
            raise NotImplementedError

    def energy_hugo(self, v, units):
        if isinstance(v, Iterable):
            if len(v) == 1:
                v = float(v)
            else:
                e = [self._energy_hugo_at_one_point(v_, units) for v_ in v]
                return np.array(e)

        return self._energy_hugo_at_one_point(v, units)

    def get_private_data(self, idx=Union[str, int]):
        """Gets private attribuate _data from `InterpolatedUnivariateSpline`.

        Returns a (shallow) copy to avoid any confusion.

        _data =
                0: x,  # data x
                1: y,  # data y
                2: w,  # weights for spline fitting
                3: xb,  # beginning of x? i.e. lowest x value
                4: xe,  # end of x? i.e. highest value of x
                5: k,  # degree
                6: s,  # smoothing, set to 0 in this class
                7: n,
                8: t,  # knots
                9: c,  # coeffs
                10: fp,
                11: fpint,
                12: nrdata,
                13: ier
        """
        if isinstance(idx, int):
            return copy(self._data[idx])

        data_names = {
            "x": 0,  # data x
            "y": 1,  # data y
            "w": 2,  # weights for spline fitting
            "xb": 3,  # beginning of x? i.e. lowest x value
            "xe": 4,  # end of x? i.e. highest value of x
            "k": 5,  # degree
            "s": 6,  # smoothing, set to 0 in this class
            "n": 7,
            "t": 8,  # knots
            "c": 9,  # coeffs
            "fp": 10,
            "fpint": 11,
            "nrdata": 12,
            "ier": 13,
        }

        return copy(self._data[data_names[idx]])

    # =============
    # Clay approach
    # =============

    def temperature_hugo(self, v):
        """This follows Matthew Clay's reference isentrope approach.

        Takes relative volume.
        """
        if isinstance(v, Iterable):
            if len(v) == 1:
                v = float(v)
            else:
                return np.array([self.temperature_hugo(v_) for v_ in v])
        f1 = self.temperature_isentrope(v)
        f2_num = self.energy_hugo(v, "kJ/g") - self.energy_isentrope(v)
        f2_den = self.cv * J_to_kJ
        return f1 + f2_num / f2_den

    def temperature_isentrope(self, v_rel):
        """This follows Matthew Clay's reference isentrope approach.

        Takes relative volume and converts it to specific volume.
        """
        v_spec = v_rel / self.rho_0
        return self.t0 * self.tau_specific_volume(v_spec)

    def energy_isentrope(self, v_rel):
        """This follows Matthew Clay's reference isentrope approach.

        Takes relative volume and converts it to specific volume.
        """
        v0 = 1 / self.rho_0
        v_spec = v_rel / self.rho_0
        ode_solver = integrate.ode(self._energy_isen_integrand)
        ode_solver.set_integrator(
            "dop853", max_step=0.1 * (v0 - v_spec), nsteps=1000000
        )
        ode_solver.set_initial_value(0.0, t=v0)
        integral = ode_solver.integrate(v_spec)
        if not ode_solver.successful():
            raise RuntimeError("ode solver did not work!")

        return self.tau_specific_volume(v_spec) * (integral + self.e0)

    def _energy_isen_integrand(self, v_spec):
        return self.phi_specific_volume(v_spec) / self.tau_specific_volume(
            v_spec
        )

    def tau_specific_volume(self, v_spec):
        """Clay's tau function."""
        method = "Reid analytical"
        if method == "Reid numerical":
            v0 = 1 / self.rho_0
            ode_solver = integrate.ode(self._tau_integrand)
            ode_solver.set_integrator(
                "dop853", max_step=0.1 * (v0 - v_spec), nsteps=1000000
            )
            ode_solver.set_initial_value(0.0, t=v0)
            integral = ode_solver.integrate(v_spec)
            if not ode_solver.successful():
                raise RuntimeError("ode solver did not work!")
            return np.exp(-integral)
        elif method == "Reid analytical":
            v0 = 1 / self.rho_0
            return np.exp(self.gamma * (1 - v_spec / v0))
        elif method == "Clay":
            v0 = 1 / self.rho_0
            return (v_spec / v0) ** (-self.gamma)
        else:
            raise RuntimeError

    def _tau_integrand(self, v_spec):
        return self.gamma * v_spec * self.rho_0

    def phi_specific_volume(self, v_spec):
        method = "Reid"
        if method == "Clay":
            f1 = self.gamma / v_spec * self.energy_hugo_specific_volume(v_spec)
            f2 = self.pressure_hugo_specific_volume(v_spec)
        elif method == "Reid":
            v_rel = v_spec * self.rho_0
            f1 = self.gamma * v_rel * self.energy_hugo_specific_volume(v_spec)
            f2 = self.pressure_hugo_specific_volume(v_spec)
        else:
            raise RuntimeError(f"Method {method} not found")
        return f1 - f2

    def energy_hugo_specific_volume(self, v_spec):
        v_rel = v_spec * self.rho_0
        return self.energy_hugo(v_rel, "kJ/g")

    def pressure_hugo_specific_volume(self, v_spec):
        v_rel = v_spec * self.rho_0
        return self(v_rel)

    def energy_isentrope_relative_volume(self, v):
        """This follows Matthew Clay's reference isentrope approach.

        Uses relative volume.
        """
        ode_solver = integrate.ode(self.energy_isen_integrand_relative_volume)
        ode_solver.set_integrator(
            "dop853", max_step=0.1 * (1 - v), nsteps=1000000
        )
        ode_solver.set_initial_value(0.0, t=1)
        integral = ode_solver.integrate(v)
        if not ode_solver.successful():
            raise RuntimeError("ode solver did not work!")
        return self.tau_relative_volume(v) * (integral + self.e0)

    def energy_isen_integrand_relative_volume(self, v):
        return self.phi_relative_volume(v) / self.tau_relative_volume(v)

    def tau_relative_volume(self, v):
        """Clay's tau function but with relative volume

        Calculated with relative volume.
        self.gamma is CTH's G0, so the true gamma function is:
            gamma(v) = G0 * v
        """
        return np.exp(-self.gamma * (v - 1))

    def temperature_isentrope_relative_volume(self, v):
        """Isentrope that passes through v."""
        return self.t0 * self.tau_relative_volume(v)

    def phi_relative_volume(self, v):
        """Clay's phi function but with relative volume."""
        return self.gamma * self.rho_0 * self.energy_hugo(v, "kJ/g") - self(v)

    def Us_from_mu_hugoniot(self, mu):
        """Rankine-Hugoniot Equations.

        P_hugo = P_0 + rho_0 U_s^2 mu
        =>
        Us = sqrt((P_hugo - P_0) / rho_0 / mu)

        :param mu: Strain
        :return:
        """
        Us = np.sqrt((self.p_hugo_from_mu(mu) - self.p0) / self.rho_0 / mu)
        return Us

    def Us_from_v_hugoniot(self, v):
        mu = 1 - v
        return self.Us_from_mu_hugoniot(mu)

    def p_hugo_from_mu(self, mu):
        return self(1 - mu)

    def dP_dmu(self, mu):
        mu_ = np.linspace(0.0, 0.5)
        p_mu_spline = InterpolatedUnivariateSpline(
            mu_, self.p_hugo_from_mu(mu_)
        )
        dp_dmu_spline = p_mu_spline.derivative()
        return dp_dmu_spline(mu)

    def dUs_dmu(self, mu):
        r"""Analytical derivative of the shock velocity from Rankine-Hugoniot.

        Using quotient rule and cancelling:
        numerator = p0 + mu * dP_hugo(mu) - P_hugo(mu)
        denominator = 2 * mu^2 * rho_0 * Us(mu)
        """
        if (isinstance(mu, np.ndarray) and (mu < 0.1).any()) or mu < 0.1:
            warn(
                "This method is not stable for mu < 0.1. "
                "Probably rounding errors. Do not trust results."
            )

        return self._dUs_dmu_numerator(mu) / self._dUs_dmu_denominator(mu)

    def _dUs_dmu_numerator(self, mu):
        return self.p0 + mu * self.dP_dmu(mu) - self.p_hugo_from_mu(mu)

    def _dUs_dmu_denominator(self, mu):
        return 2 * mu ** 2 * self.rho_0 * self.Us_from_mu_hugoniot(mu)

    # ===============================================
    # Distended EOS for different reference densities
    # ===============================================
    def pressure_distended(self, v_spec: float, rho_dist: float):
        """Calculates the pressure of one point on a distended Hugoniot.

        If the rho_dist is greater than self.rho_0, then it assumes that
        the current spline Hugoniot EOS is actually the distended
        Hugoniot and solves for the original reference Hugoniot.
        (In the code, this is called "anti-distended", although that is not
        a standard term.)

        Note: this EOS' gamma follows the CTH convention, so
        self.gamma is actually the constant G0.
        The Gruneisen gamma G = G0 * v_rel
        and therefore, G/v_spec = G0 * rho_0

        If distending, uses the following form in the calculations
        P_distended = f1 / f2 * P_hugoniot

        where
        f1 = gamma * rho_0 * (v_spec - v_orig_0) + 2
        f2 = gamma * rho_0 * (v_spec - v_dist_0) + 2

        If "anti-distending", then:
        P_hugoniot = f2 / f1 * P_distended,

        where P_distended is calculated on the currently known spline Hugoniot,
        f1 = gamma * rho_antidist * (v_spec - v_antidist_0) + 2
        f2 = gamma * rho_antidist * (v_spec - v_orig_0) + 2

        We must use rho_dist, which is now the reference density for the
        Hugoniot, which is not the spline.


        v_spec:
          The specific volume for the point on the Hugoniot.
        rho_dist:
          The density of the distended EOS.
        """
        if rho_dist <= self.rho_0:
            logger.debug("distending EOS")
            if rho_dist == self.rho_0:
                warn(
                    "distended density is the same as the current density!\n"
                    f"rho_0: {self.rho_0}  rho_dist: {rho_dist}\n"
                    'returning calculated "distended" pressure anyway, '
                    "which should be the pressure on the Hugoniot "
                    "(within numerical error)."
                )
            v_dist_0 = 1 / rho_dist  # cc/g distended reference specific volume
            v_orig_0 = 1 / self.rho_0  # cc/g reference specific volume

            v_rel = v_spec * self.rho_0  # for original Hugoniot
            p_hugo = self(v_rel)

            f1 = self.gamma * self.rho_0 * (v_spec - v_orig_0) + 2
            f2 = self.gamma * self.rho_0 * (v_spec - v_dist_0) + 2
            return f1 / f2 * p_hugo

        elif rho_dist > self.rho_0:
            logger.debug("anti-distending EOS")
            v_orig_0 = 1 / self.rho_0
            v_anti_dist_0 = 1 / rho_dist

            v_rel = v_spec * self.rho_0  # for original spline Hugoniot
            p_orig = self(v_rel)

            f2 = self.gamma * rho_dist * (v_spec - v_orig_0) + 2
            f1 = self.gamma * rho_dist * (v_spec - v_anti_dist_0) + 2

            return f2 / f1 * p_orig
        else:
            raise RuntimeError(
                "Code should not arrive here. Something wrong "
                f"with rho_dist {rho_dist} and self.rho_0 "
                f"{self.rho_0} comparison."
            )

    # Untested below ==========================================================

    def pressure_to_atm(self, v):
        return self(v) * GPa_to_atm

    def mie_gruneisen_energy(self, v, t, energy_units="kJ/g"):
        """Finds off-Hugoniot energy using the Mie-Gruneisen approximation.

        Only scalar v and t allowed. Not vectorized.
        """
        t_hugo = self.temperature_hugo(v)  # K
        e_hugo = self.energy_hugo(v, "kJ/g")
        e = e_hugo + self.cv * (t - t_hugo) * J_to_kJ  # kJ/g

        if energy_units == "kJ/g":
            return e
        elif energy_units == "cal/g":
            return e / 4.184
        else:
            raise NotImplementedError

    def mie_gruneisen_pressure(self, v, t, p_units="GPa"):
        """Finds off-Hugoniot pressure using the Mie-Gruneisen approximation.

        Only scalar v and t allowed. Not vectorized.
        v is relative volume
        """
        e_hugo = self.energy_hugo(v, "kJ/g")
        e = self.mie_gruneisen_energy(v, t, "kJ/g")

        p_hugo = self(v)  # GPa

        # GPa, kJ/cc are the same
        p = p_hugo + self.gamma * self.rho_0 * (e - e_hugo)

        if p_units == "GPa":
            return p
        elif p_units == "atm":
            return p * GPa_to_atm
        else:
            raise NotImplementedError


"""
Private attribute of the `InterpolatedUnivariateSpline` instance.
_data =
        0: x,  # data x
        1: y,  # data y
        2: w,  # weights for spline fitting
        3: xb,  # beginning of x? i.e. lowest x value
        4: xe,  # end of x? i.e. highest value of x
        5: k,  # degree
        6: s,  # smoothing, set to 0 in this class
        7: n,
        8: t,  # knots
        9: c,  # coeffs
        10: fp,
        11: fpint,
        12: nrdata,
        13: ier
"""
