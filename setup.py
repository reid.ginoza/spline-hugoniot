# Copyright (C) 2021 Reid Ginoza
# All rights reserved.
from setuptools import setup

setup(
    name="seos",
    version="",
    packages=["seos"],
    url="",
    license="",
    author="Reid Ginoza",
    author_email="",
    description="Spline equations of state (EOS).",
)
